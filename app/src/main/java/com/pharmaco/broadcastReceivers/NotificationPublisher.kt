package com.pharmaco.broadcastReceivers

import android.app.Notification
import android.content.BroadcastReceiver
import android.content.Context.NOTIFICATION_SERVICE
import androidx.core.content.ContextCompat.getSystemService
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.util.Log
import com.pharmaco.models.Notificacion
import com.pharmaco.repo.notificaciones.NotificacionesRepo
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*


class NotificationPublisher(): BroadcastReceiver() {

    public var NOTIFICATION_ID = "notification-id"

    public var NOTIFICATION = "notification"


    override fun onReceive(context: Context, intent: Intent) {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notification = intent.getParcelableExtra<Notification>(NOTIFICATION)
        val id = intent.getIntExtra(NOTIFICATION_ID, 0)
        notificationManager.notify(id, notification)
        val titulo = intent.extras!!.getString("titulo")
        val tipo = intent.extras!!.getString("tipo")
        val mensaje = intent.extras!!.getString("mensaje")
        val mensajeLargo = intent.extras!!.getString("mensajeLargo")

        GlobalScope.launch { NotificacionesRepo.addNotificacion(Notificacion(titulo, Date(), tipo, mensaje, mensajeLargo)) }
    }
}