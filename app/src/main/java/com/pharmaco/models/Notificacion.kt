package com.pharmaco.models

import com.google.firebase.Timestamp
import java.util.*

data class Notificacion (var titulo: String? = null, var fecha: Date? = null,
                         var tipo: String? = null, var mensajeCorto:String? = null,
                         var mensaje:String? = null, var id: String? = null) {

    fun toHashMap(): HashMap<String, Any?> {

        val hm = hashMapOf<String, Any?>(
                "titulo" to titulo,
                "fecha" to Timestamp(fecha),
                "tipo" to tipo,
                "mensajeCorto" to mensajeCorto,
                "mensaje" to mensaje
        )

        return hm
    }

}