package com.pharmaco.models

import com.google.firebase.firestore.DocumentReference
import java.io.Serializable

//Medicamento Orden + El id de la orden a la que pertenece el medicamento
data class MedicamentoCreate(var nombre: String = "", var nombreLab: String = "", var dosisNumero: Int = -1,
                        var dosisUnidad: String = "", var ordenReference: DocumentReference): Serializable{

    constructor(mo: MedicamentoOrden, ordenReference: DocumentReference): this(nombre = mo.nombre,nombreLab = mo.nombreLab,
        dosisNumero = mo.dosisNumero, dosisUnidad = mo.dosisUnidad, ordenReference = ordenReference)
}