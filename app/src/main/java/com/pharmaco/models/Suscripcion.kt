package com.pharmaco.models

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import java.io.Serializable
import java.util.*
import kotlin.collections.HashMap

data class Suscripcion(var fechaInicio: Date = Date(), var fechaFin: Date = Date(),
                       var fechaAgendada: List<Date> = mutableListOf(), var qr: String = "",
                       var fechaEntrega: List<Date>? = null, var farmacia: FarmaciaTSD = FarmaciaTSD(),
                       var medicamentos: MutableList<MedicamentoTSD> = mutableListOf(),
                       var ordenes: List<DocumentReference> = mutableListOf(), var cancelado: Boolean = false,
                       var id: String = "", var hora: String = "", var diaSemana: String = "Juernes",
                       var frecuencia: Int = -1): Serializable {

    fun toHashMap(): HashMap<String, Any?>{

        val fechaAgendadaTS = mutableListOf<Timestamp>()
        for(fechaA in fechaAgendada){
            fechaAgendadaTS.add(Timestamp(fechaA))
        }

        val medicamentosHm = arrayListOf<HashMap<String, Any>>()
        for(med in medicamentos){
            medicamentosHm.add(med.toHashMap())
        }


        val hm = hashMapOf(
            "fechaInicio" to Timestamp(fechaInicio),
            "fechaFin" to Timestamp(fechaFin),
            "fechaAgendada" to fechaAgendadaTS,
            "qr" to qr,
            "fechaEntrega" to null,
            "farmacia" to farmacia.toHashMap(),
            "medicamentos" to medicamentosHm,
            "ordenes" to ordenes,
            "cancelado" to cancelado

        )

        return hm

    }

}