package com.pharmaco.models

import java.io.Serializable

 data class MedicamentoOrden(var nombre: String = "", var nombreLab: String = "", var dosisNumero: Int = -1,
                            var dosisUnidad: String = ""): Serializable{

     fun toHashMap(): HashMap<String, Any>{

         return hashMapOf(
             "nombre" to nombre,
             "nombreLab" to nombreLab,
             "dosisNumero" to dosisNumero,
             "dosisUnidad" to dosisUnidad
         )

     }
 }