package com.pharmaco.models

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import java.io.Serializable
import java.util.*
import kotlin.collections.HashMap

data class Orden(var doctor: String = "", var fecha: Date = Date(), var tipo: String = "", var audio: String? = null,
                 var foto: String?  = null, var suscripciones: MutableList<DocumentReference>? = null,
                 var domicilios: MutableList<DocumentReference>? = null,
                 var turnos: MutableList<DocumentReference>? = null, var especialidad: String = "",
                 var frecuencias: MutableList<Frecuencia> = mutableListOf(),
                 var id: String = "", var reference: DocumentReference? = null): Serializable {

    /**
    companion object{
        //Construye esta clase a partir de un hashmap generado por la función toSerializableHashMap
        fun ordenFromHashMap(hm: HashMap<String, Any?>): Orden {

            val db = FirebaseFirestore.getInstance()

            var suscripcionesReference: MutableList<DocumentReference>? = null
            val hmSuscripciones = hm["suscripciones"] as MutableList<String>?
            if(hmSuscripciones != null){
                suscripcionesReference = mutableListOf()
                var i = 0
                while(i < hmSuscripciones.size){
                    suscripcionesReference.add(db.document(hmSuscripciones.get(i)))
                    i++
                }
            }

            var turnosReference: MutableList<DocumentReference>? = null
            val hmTurnos = hm["suscripciones"] as MutableList<String>?
            if(hmTurnos != null){
                turnosReference = mutableListOf()
                var i = 0
                while(i < hmTurnos.size){
                    turnosReference.add(db.document(hmTurnos.get(i)))
                    i++
                }
            }

            var domiciliosReference: MutableList<DocumentReference>? = null
            val hmDomicilios = hm["suscripciones"] as MutableList<String>?
            if(hmDomicilios != null){
                domiciliosReference = mutableListOf()
                var i = 0
                while(i < hmDomicilios.size){
                    domiciliosReference.add(db.document(hmDomicilios.get(i)))
                    i++
                }
            }

            return Orden(doctor = hm["doctor"] as String, fecha = hm["fecha"] as Date,
                tipo = hm["tipo"] as String, audio = hm["audio"] as String?, foto = hm["foto"] as String?,
                suscripciones = suscripcionesReference, domicilios = domiciliosReference, turnos = turnosReference,
                frecuencias = hm["frecuencias"] as MutableList<Frecuencia>, id = hm["id"] as String,
                reference = db.document(hm["reference"] as String), especialidad = hm["especialidad"] as String)
        }

    }
    **/

    //Función usada para guardarlo en LocalStorage, los DocumentReferences se deben convertir en Strings
    fun toSerializableHashMap(): HashMap<String, Any?>{

        var suscripcionesString: MutableList<String>? = null
        if(suscripciones!=null){
            suscripcionesString = mutableListOf()
            var i = 0
            while(i < suscripciones!!.size){
                suscripcionesString.add(suscripciones!!.get(i).path)
                i++
            }
        }

        var turnosString: MutableList<String>? = null
        if(turnos!=null){
            turnosString = mutableListOf()
            var i = 0
            while(i < turnos!!.size){
                turnosString.add(turnos!!.get(i).path)
                i++
            }
        }

        var domiciliosString: MutableList<String>? = null
        if(domicilios!=null){
            domiciliosString = mutableListOf()
            var i = 0
            while(i < domicilios!!.size){
                domiciliosString.add(domicilios!!.get(i).path)
                i++
            }
        }

        return hashMapOf(
            "doctor" to doctor,
            "fecha" to fecha,
            "tipo" to tipo,
            "audio" to audio,
            "foto" to foto,
            "suscripciones" to suscripcionesString,
            "domicilios" to domiciliosString,
            "turnos" to turnosString,
            "especialidad" to especialidad,
            "frecuencias" to frecuencias,
            "id" to id,
            "reference" to reference!!.path
        )

    }

    fun toHashMap(): HashMap<String, Any?>{

        return hashMapOf(
            "doctor" to doctor,
            "fecha" to Timestamp(fecha),
            "tipo" to tipo,
            "audio" to audio,
            "foto" to foto,
            "suscripciones" to suscripciones,
            "domicilios" to domicilios,
            "turnos" to turnos,
            "especialidad" to especialidad,
            "borrada" to false
        )

    }

}