package com.pharmaco.models

import com.google.firebase.firestore.GeoPoint
import java.io.Serializable


data class FarmaciaTSD(var nombre: String = "", var direccion: String = "", var telefono: String = "",val ubicacion: GeoPoint = GeoPoint(0.0,0.0)): Serializable{

    fun toHashMap(): HashMap<String, String>{

        return hashMapOf(
            "nombre" to nombre,
            "direccion" to direccion,
            "telefono" to telefono
        )

    }

}