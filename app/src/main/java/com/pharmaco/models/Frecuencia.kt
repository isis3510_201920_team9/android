package com.pharmaco.models

import com.google.firebase.firestore.DocumentReference
import java.io.Serializable


data class Frecuencia(var numero: Int? = null, var unidadFrecuencia: String? = null, var periodo: Int? = null,
                      var medicamento: MedicamentoOrden = MedicamentoOrden(), var unidades: Int? = null,
                      var unidadPeriodo: String? = null): Serializable
{

    //Retorna un MedicamentoTSDCreate construido a partir de esta frecuencia.
    fun toMedicamentoTSDCreate(ordenReference: DocumentReference): MedicamentoTSDCreate{

        return MedicamentoTSDCreate(MedicamentoCreate(medicamento, ordenReference = ordenReference)
            , unidadesPedidas = 0, dosisCaja = 0)
    }

    fun toHashMap(): HashMap<String, Any?>{

        return hashMapOf(
            "numero" to numero,
            "unidadFrecuencia" to unidadFrecuencia,
            "periodo" to periodo,
            "medicamento" to medicamento.toHashMap(),
            "unidades" to unidades,
            "unidadPeriodo" to unidadPeriodo
        )

    }

}