package com.pharmaco.models
import java.io.Serializable


data class MedicamentoTSD(var unidadesPedidas: Int = -1, var dosisCaja: Int = -1, var nombre: String = "",
                          var nombreLab: String = "", var dosisNumero: Int = -1, var dosisUnidad: String = ""): Serializable {

    //Crear un medicamento TSD a partir de un MedicamentoCreate
    constructor(mc: MedicamentoCreate, unidadesPedidas: Int, dosisCaja: Int): this(unidadesPedidas = unidadesPedidas,
        dosisCaja = dosisCaja, nombre =  mc.nombre, nombreLab =  mc.nombreLab, dosisNumero = mc.dosisNumero,
        dosisUnidad = mc.dosisUnidad)

    fun toHashMap(): HashMap<String, Any>{

        return hashMapOf(
            "unidadesPedidas" to unidadesPedidas,
            "dosisCaja" to dosisCaja,
            "nombre" to nombre,
            "nombreLab" to nombreLab,
            "dosisNumero" to dosisNumero,
            "dosisUnidad" to dosisUnidad
        )

    }

}