package com.pharmaco.models

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import java.io.Serializable
import java.util.*
import kotlin.collections.HashMap

data class Turno(var especial: Boolean = false, var observacionesEspecial: String? = null,
                 var fechaAgendada: Date = Date(), var qr:String = "", var fechaEntrega: Date? = null,
                 var exitoso: Boolean? = null, var novedades: String? = null, var farmacia: FarmaciaTSD = FarmaciaTSD(),
                 var medicamentos: MutableList<MedicamentoTSD> = mutableListOf(),
                 var ordenes: MutableList<DocumentReference>? = null,
                 var id: String = ""): Serializable
{

    fun toHashMap(): HashMap<String, Any?> {


        val medicamentosHm = arrayListOf<HashMap<String, Any>>()
        for(med in medicamentos){
            medicamentosHm.add(med.toHashMap())
        }


        val hm = hashMapOf(
            "especial" to especial,
            "observacionesEspecial" to observacionesEspecial,
            "fechaAgendada" to Timestamp(fechaAgendada.time/1000L, 0),
            "qr" to qr,
            "fechaEntrega" to null,
            "exitoso" to exitoso,
            "novedades" to novedades,
            "farmacia" to farmacia.toHashMap(),
            "medicamentos" to medicamentosHm,
            "ordenes" to ordenes
        )

        return hm

    }

}
