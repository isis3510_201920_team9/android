package com.pharmaco.models

import com.google.firebase.Timestamp
import java.util.*
import kotlin.collections.HashMap

data class Usuario(var email: String? = null, var eps: String? = null, var numDocumento: String? = null,
                   var nombre: String? = null, var uid: String? = null, var genero: String? = null,
                   var edad: Int? = null){

    fun toHashMap(): HashMap<String, Any?>{

        return hashMapOf(
                "email" to email,
                "eps" to eps,
                "numDocumento" to numDocumento,
                "tipoDocumento" to "cc",
                "nombre" to nombre,
                "genero" to genero,
                "edad" to edad,
                "sesionActiva" to true,
                "plataforma" to "Android",
                "fechaCreacionCuenta" to Timestamp(Date()),
                "fechaNacimiento" to Timestamp(Date(907477200000))
        )

    }

}