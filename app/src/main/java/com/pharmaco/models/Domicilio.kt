package com.pharmaco.models

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import java.io.Serializable
import java.util.*

data class Domicilio(var direccion: String = "", var observaciones: String? = null, var fechaAgendada: Date = Date(),
                     var costo: Int = 0, var qr: String = "", var fechaEntrega: Date? = null,
                     var exitoso:Boolean? = null, var novedades: String? = null, var farmacia: FarmaciaTSD = FarmaciaTSD(),
                     var medicamentos: MutableList<MedicamentoTSD> = mutableListOf(),
                     var ordenes: List<DocumentReference> = mutableListOf(),
                     var id: String = ""): Serializable {

    fun toHashMap(): HashMap<String, Any?> {


        val medicamentosHm = arrayListOf<HashMap<String, Any>>()
        for(med in medicamentos){
            medicamentosHm.add(med.toHashMap())
        }


        val hm = hashMapOf(
            "direccion" to direccion,
            "observaciones" to observaciones,
            "fechaAgendada" to Timestamp(fechaAgendada),
            "costo" to costo,
            "qr" to qr,
            "fechaEntrega" to null,
            "exitoso" to exitoso,
            "novedades" to novedades,
            "farmacia" to farmacia.toHashMap(),
            "medicamentos" to medicamentosHm,
            "ordenes" to ordenes
        )

        return hm

    }

}