package com.pharmaco.viewmodelfactories.suscripcion

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pharmaco.models.Suscripcion
import com.pharmaco.viewModels.suscripciones.SuscripcionDetailViewModel

class SuscripcionDetailViewModelFactory(val suscripcion: Suscripcion): ViewModelProvider.Factory {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        val mlp = MutableLiveData<Suscripcion>()
        mlp.value = suscripcion

        return SuscripcionDetailViewModel(mlp) as T

    }
}