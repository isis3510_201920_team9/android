package com.pharmaco.viewmodelfactories.turno

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pharmaco.models.Orden
import com.pharmaco.viewModels.ordenes.OrdenDetailViewModel

class OrdenDetailViewModelFactory(val orden: Orden): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val mlp = MutableLiveData<Orden>()
        mlp.value = orden

        return OrdenDetailViewModel(mlp) as T
    }
}