package com.pharmaco.viewmodelfactories.turno

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pharmaco.models.Turno
import com.pharmaco.viewModels.turnos.TurnoDetailViewModel

class TurnoDetailViewModelFactory(val turno: Turno): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val mlp = MutableLiveData<Turno>()
        mlp.value = turno

        return TurnoDetailViewModel(mlp) as T
    }
}