package com.pharmaco.viewmodelfactories.domicilio

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pharmaco.models.Domicilio
import com.pharmaco.viewModels.domicilios.DomicilioDetailViewModel

class DomicilioDetailViewModelFactory(val domicilio: Domicilio): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val mlp = MutableLiveData<Domicilio>()
        mlp.value = domicilio

        return DomicilioDetailViewModel(mlp) as T
    }
}