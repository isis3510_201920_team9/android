package com.pharmaco

import android.util.Log
import com.pharmaco.repo.domicilios.DomiciliosRepo
import com.pharmaco.repo.farmacias.FarmaciasRepo
import com.pharmaco.repo.ordenes.OrdenesRepo
import com.pharmaco.repo.suscripciones.SuscripcionesRepo
import com.pharmaco.repo.turnos.TurnosRepo
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

object DataCache {

    val TAG = this.javaClass.simpleName

    fun cacheData(){

        GlobalScope.launch {
            FarmaciasRepo.cacheFarmaciasOnInit()
            DomiciliosRepo.cacheDomicilios()
            OrdenesRepo.cacheOrdenes()
            SuscripcionesRepo.cacheSuscripciones()
            TurnosRepo.cacheTurnos()

            Log.d(TAG, "DATA CACHED")
        }
    }

}