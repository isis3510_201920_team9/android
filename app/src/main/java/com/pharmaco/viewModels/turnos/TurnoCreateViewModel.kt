package com.pharmaco.viewModels.turnos

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pharmaco.models.*
import com.pharmaco.repo.farmacias.FarmaciasRepo
import com.pharmaco.repo.medicamentos.MedicamentosRepo
import com.pharmaco.repo.turnos.TurnosRepo
import com.pharmaco.viewModels.interfaces.CreateViewModelInterface
import com.pharmaco.viewModels.interfaces.MapViewModelInterface
import com.pharmaco.viewModels.interfaces.PharmacoViewModelInterface
import kotlinx.coroutines.launch
import java.util.*

class TurnoCreateViewModel: ViewModel(), CreateViewModelInterface, MapViewModelInterface, PharmacoViewModelInterface{

    override val selectedFarmacia: MutableLiveData<FarmaciaTSD> = MutableLiveData()

    override val farmaciasDisponibles: MutableLiveData<MutableList<FarmaciaTSD>> by lazy {
        MutableLiveData<MutableList<FarmaciaTSD>>().also {
            viewModelScope.launch {
                FarmaciasRepo.loadFarmacias(farmaciasDisponibles)
            }
        }

    }

    override val TAG = this.javaClass.toString()

    override val medicamentos: MutableLiveData<MutableList<MedicamentoCreate>> =  MutableLiveData<MutableList<MedicamentoCreate>>()

    override val medicamentosSeleccionados: MutableLiveData<MutableList<MedicamentoTSDCreate>> = MutableLiveData()

    override var orden: Orden? = null

    init{
        loadMedicamentos()
    }

    fun createTurno(fechaAgendada: Date){
        viewModelScope.launch {
            TurnosRepo.createTurno(fechaAgendada, medicamentosSeleccionados, selectedFarmacia)
        }
    }

    override fun loadMedicamentos() {
        viewModelScope.launch {
            MedicamentosRepo.loadMedicamentos(medicamentos)
        }
    }

    /*fun createTurno(fechaAgendada: Date){

        val medicamentos = medicamentosSeleccionados.value


        if(medicamentos != null){
            val ordenes = mutableListOf<DocumentReference>()
            val medicamentosTSD = mutableListOf<MedicamentoTSD>()


            for(med in medicamentos) {
                ordenes.add(med.ordenReference)
                medicamentosTSD.add(med.toMedicamentoTSD())
            }


            //ordenes entra con entradas repetidas, se deben eliminar
            val ordenesDistinct = ordenes.distinct().toMutableList()

            Log.d(TAG, "ORDENES DISTINCT ${ordenesDistinct}")

            val turno = Turno(fechaAgendada = fechaAgendada, farmacia =  selectedFarmacia!!.value!!, medicamentos = medicamentosTSD ,
                ordenes = ordenesDistinct
            )

            val db = FirebaseFirestore.getInstance()

            db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.TURNOS_ROUTE).add(turno.toHashMap())
                .addOnSuccessListener {turnoReference ->
                    Log.d(TAG, "Documento creado: $turno")

                    //Ahora añadir referencia en las ordenes.
                    for (orden in ordenesDistinct) {
                        Log.d(TAG, "ORDEN IN ORDENES DISCTINCT")

                        orden.get().addOnSuccessListener {

                            Log.d(TAG, "ORDEN GET")


                            val ordenObj = it.toObject(Orden::class.java)

                            var turnos= ordenObj!!.turnos

                            if(turnos == null){
                                turnos = mutableListOf()
                            }
                            if(!turnos!!.contains(orden)){
                                turnos.add(turnoReference)
                            }

                            orden.update("turnos", turnos)
                                .addOnSuccessListener {
                                    Log.d(TAG, "ACTUALIZADA REFERENCIA EN ORDENES")
                                }
                                .addOnFailureListener { e ->
                                    Log.w(TAG, "Error creando el documento", e)
                                }
                        }
                    }
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error creando el documento", e)
                }

        }
    }*/

}