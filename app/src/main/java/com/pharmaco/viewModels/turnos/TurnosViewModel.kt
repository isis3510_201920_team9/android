package com.pharmaco.viewModels.turnos

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.pharmaco.models.Turno
import com.pharmaco.repo.turnos.TurnosRepo
import com.pharmaco.viewModels.Routes
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class TurnosViewModel: ViewModel(){
    private val TAG = this.javaClass.toString()

    private val turnosProximos: MutableLiveData<MutableList<Turno>> = MutableLiveData<MutableList<Turno>>()

    init {
        loadTurnosProximos()
        addDBListener()
    }

    private fun addDBListener() {

        val db = FirebaseFirestore.getInstance()
        val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.TURNOS_ROUTE)

        docRef.addSnapshotListener { snapshot, e ->

            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            //Lanzar corrutina que espere y vuelva a cargar órdenes.
            GlobalScope.launch {
                delay(1000L)
                loadTurnosProximos()
            }

        }
    }

    fun deleteTurno(posTurno: Int){
        viewModelScope.launch {
            TurnosRepo.deleteTurno(posTurno, turnosProximos)
        }
    }

    fun getTurnosProximos(): LiveData<MutableList<Turno>> {
        return turnosProximos
    }

    private fun loadTurnosProximos(){
        viewModelScope.launch {
            TurnosRepo.loadTurnosProximos(turnosProximos)
        }
    }



    //posTurno indica la posición del turno en la lista turnosProximos
    /* fun deleteTurno(posTurno: Int){

         val db = FirebaseFirestore.getInstance()

         //Primero, quitar de MutableLiveData
         val turnos = turnosProximos.value

         if(turnos != null) {
             val turno = turnos.get(posTurno)
             turnos.removeAt(posTurno)

             turnosProximos.value = turnos

             db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                 .collection(Routes.TURNOS_ROUTE).document(turno.id)
                 .update("exitoso", false, "novedades", "Turno cancelado por el usuario")
                 .addOnSuccessListener {
                     Log.d(TAG, "Documento marcado como eliminado!")
                 }
                 .addOnFailureListener { e ->
                     Log.w(TAG, "Error eliminando el documento", e)
                 }
         }
         else{
             Log.w(TAG, "Error eliminando el documento, turnosProximos inesperadamente nulo")
         }
     }*/


    /*private fun loadTurnosProximos() {

        val db = FirebaseFirestore.getInstance()

        //Recuperar las ordenes de este usuario de la base de datos

        val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID).collection(Routes.TURNOS_ROUTE)

        val list = mutableListOf<Turno>()

        docRef.whereEqualTo("exitoso", null).orderBy("fechaAgendada", Query.Direction.DESCENDING).get()
            .addOnSuccessListener { result ->

                for (document in result) {

                    Log.d(TAG, "${document.id} => ${document.data}")

                    val turno = document.toObject(Turno::class.java)
                    turno.id = document.id

                    list.add(turno)
                }
                turnosProximos.value = list
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }*/
}