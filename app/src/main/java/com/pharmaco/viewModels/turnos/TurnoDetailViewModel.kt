package com.pharmaco.viewModels.turnos

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pharmaco.models.Turno

class TurnoDetailViewModel(private val turno: MutableLiveData<Turno>): ViewModel(){

    fun getTurno():  MutableLiveData<Turno>{
        return turno
    }

}
