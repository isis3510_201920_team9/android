package com.pharmaco.viewModels

import android.util.Log
import com.google.firebase.auth.FirebaseAuth

object Routes{

    var USER_ID = "uKBOq3cEfJgfv3EUlgzB"
    val CLIENTES_ROUTE = "clientes"
    val ORDENES_ROUTE = "ordenesMedicas"
    val NOTIFICACIONES_ROUTE = "notificaciones"
    val SUSCRIPCIONES_ROUTE = "suscripciones"
    val TURNOS_ROUTE = "turnos"
    val FRECUENCIAS_ROUTE = "frecuencias"
    val DOMICILIOS_ROUTE = "domicilios"
    val FARMACIAS_ROUTE = "farmacias"
    var MEDICAMENTOS_ROUTE = "medicamentos"

    //Rutas del cache local
    val ORDENES_CACHE_ROUTE = "ordenes"


    fun setUserID(){

        val auth = FirebaseAuth.getInstance()

        if(auth.currentUser != null){
            USER_ID = auth.currentUser!!.uid
            Log.d("AYUDA", USER_ID)
        }

    }

    fun clearUserID(){

        USER_ID = ""

    }


}