package com.pharmaco.viewModels.interfaces

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.pharmaco.models.*
import com.pharmaco.viewModels.Routes

interface CreateViewModelInterface {

    val TAG: String

    val medicamentos: MutableLiveData<MutableList<MedicamentoCreate>>

    val medicamentosSeleccionados: MutableLiveData<MutableList<MedicamentoTSDCreate>>

    //Orden a partir de la cual se está creando el turno/suscripcion/domicilio (si aplica).
    val orden: Orden?

    fun setMedicamentosSeleccionados(ml: MutableList<MedicamentoTSDCreate>){
        medicamentosSeleccionados.value = ml
    }

    fun editCantidadMedicamentoSeleccionado(pos: Int, nCantidad: Int) {

        val ms = medicamentosSeleccionados.value

        val medTsd = ms!![pos]
        medTsd.unidadesPedidas = nCantidad

        //medicamentosSeleccionados.value = ms

        Log.d(TAG, medicamentosSeleccionados.value.toString())
    }

    fun deleteMedicamentoSeleccionado(pos: Int) {

        val ms = medicamentosSeleccionados.value
        ms!!.removeAt(pos)
        medicamentosSeleccionados.value = ms

        Log.d(TAG, medicamentosSeleccionados.value.toString())


    }

    //Recibe un medicamento Create y crea un medicamento TSD a partir de este.
    fun addMedicamentoSeleccionado(mc: MedicamentoCreate){

        var ms = medicamentosSeleccionados.value

        if(ms == null){
            ms = mutableListOf()
        }

        //TODO, en el siguiente Sprint se debe cambiar dosisCaja y unidadesPedidas de modo que se saque del a BD de
        // medicamentos y de la orden respetivamente.
        val mtsd = MedicamentoTSDCreate(mc = mc, unidadesPedidas = 0, dosisCaja = 0)

        ms.add(mtsd)

        medicamentosSeleccionados.value = ms

        Log.d(TAG, medicamentosSeleccionados.value.toString())
    }



    fun loadMedicamentos()

    //Ir a BD, consultar órdenes
    // por cada órden consultar frecuencias,
    // por cada frecuencia, añadir su medicamento a la lista

     /*fun loadMedicamentos() {

        @Synchronized fun addMedicamento(med: MedicamentoCreate){
            var  list = medicamentos.value

            if(list == null){
                list = mutableListOf()
            }

            list!!.add(med)
            medicamentos.value = list
        }

        val db = FirebaseFirestore.getInstance()

        //Recuperar las ordenes de este usuario de la base de datos

        val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID).collection(
            Routes.ORDENES_ROUTE
        )

        docRef.whereEqualTo("borrada", false).orderBy("fecha", Query.Direction.DESCENDING).get()
            .addOnSuccessListener { result ->

                for (documentOrden in result) {

                    Log.d(TAG, "${documentOrden.id} => ${documentOrden.data}")

                    val orden = documentOrden.toObject(Orden::class.java)
                    orden.id = documentOrden.id

                    // por cada orden, se deben traer sus frecuencias
                    val freqRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                        .collection(Routes.ORDENES_ROUTE).document(documentOrden.id).collection(
                            Routes.FRECUENCIAS_ROUTE
                        )


                    freqRef.get().addOnSuccessListener { frecuencias ->
                        for(document in frecuencias){

                            Log.d(TAG, "${document.id} => ${document.data}")

                            val freq = document.toObject(Frecuencia::class.java)
                            val med = MedicamentoCreate(freq.medicamento, documentOrden.reference)
                            addMedicamento(med)
                        }

                    }

                }
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }*/

    fun getMedicamentosSeleccionados(): LiveData<MutableList<MedicamentoTSDCreate>> {
        return medicamentosSeleccionados
    }

    fun getMedicamentosSeleccionadosList(): MutableList<MedicamentoTSDCreate>?{
        return medicamentosSeleccionados.value
    }

    fun getMedicamentos(): LiveData<MutableList<MedicamentoCreate>> {
        return medicamentos
    }

}