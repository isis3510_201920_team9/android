package com.pharmaco.viewModels.interfaces

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.pharmaco.models.Frecuencia
import com.pharmaco.models.MedicamentoOrden

//Interfaz para los métodos comúnes de editar y crer una Orden
interface OrdenCreateEditViewModelInterface{

    val TAG: String

    val frecuencias: MutableLiveData<MutableList<Frecuencia>>

    val medicamentosAlternativos: MutableLiveData<List<String>>

    val medicamentoValido: MutableLiveData<Boolean>

    fun buscarMedicamento(nombre: String)

    fun confirmarMedicamento(nombre: String)

    fun deleteFrecuencia(pos: Int) {

        val ms = frecuencias.value
        ms!!.removeAt(pos)
        frecuencias.value = ms

        Log.d(TAG, frecuencias.value.toString())


    }

    fun addFrecuencia(nombreMedicamento: String, dosisPorUnidad: Int, medicamentoDosisUnidad: String, tomarUnidad: Int,
                      tomarFrecuencia: Int, tomarFrecuenciaUnidad: String, tomarPeriodo: Int, tomarPeriodoUnidad: String) {


        //Primero verificar que el medicamento, en efecto, existe


        val medicamento = MedicamentoOrden(
            nombre = nombreMedicamento, nombreLab = "", dosisNumero = dosisPorUnidad,
            dosisUnidad = medicamentoDosisUnidad
        )

        val frecuencia = Frecuencia(
            unidades = tomarUnidad, numero = tomarFrecuencia,
            unidadFrecuencia = tomarFrecuenciaUnidad, periodo = tomarPeriodo, unidadPeriodo = tomarPeriodoUnidad,
            medicamento = medicamento
        )


        var list = frecuencias.value

        if (list == null) {
            list = mutableListOf()
        }
        list.add(frecuencia)
        frecuencias.value = list

    }


}