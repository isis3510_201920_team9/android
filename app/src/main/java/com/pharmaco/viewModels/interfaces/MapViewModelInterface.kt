package com.pharmaco.viewModels.interfaces

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.pharmaco.models.FarmaciaTSD
import com.pharmaco.viewModels.Routes

interface MapViewModelInterface {

    val TAG: String

    val selectedFarmacia: MutableLiveData<FarmaciaTSD>

    val farmaciasDisponibles: MutableLiveData<MutableList<FarmaciaTSD>>

    fun getFarmaciasDisponibles(): LiveData<MutableList<FarmaciaTSD>> {
        return farmaciasDisponibles
    }

    fun setSelected( farmacia: FarmaciaTSD ) {

        selectedFarmacia.value = farmacia

    }
}