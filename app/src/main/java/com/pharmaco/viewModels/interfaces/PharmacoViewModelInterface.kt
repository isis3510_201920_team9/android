package com.pharmaco.viewModels.interfaces

import android.content.Context
import android.net.ConnectivityManager
import androidx.fragment.app.FragmentActivity

interface  PharmacoViewModelInterface{

    fun verifyAvailableNetwork(activity: FragmentActivity):Boolean{

        val connectivityManager=activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo=connectivityManager.activeNetworkInfo
        return  networkInfo!=null && networkInfo.isConnected

    }
}