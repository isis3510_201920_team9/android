package com.pharmaco.viewModels.notificaciones

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestore
import com.pharmaco.models.Notificacion
import com.pharmaco.repo.notificaciones.NotificacionesRepo
import com.pharmaco.viewModels.Routes
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class NotificacionesViewModel: ViewModel(){


    val TAG = this.javaClass.simpleName
    val notificaciones: MutableLiveData<MutableList<Notificacion>> = MutableLiveData()


    init {
        loadNotificaciones()
        addDBListener()
    }

    private fun addDBListener() {

        val db = FirebaseFirestore.getInstance()
        val docRef = db.collection(Routes.CLIENTES_ROUTE)
                .document(Routes.USER_ID).collection(Routes.NOTIFICACIONES_ROUTE)

        docRef.addSnapshotListener { snapshot, e ->

            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            //Lanzar corrutina que espere y vuelva a cargar órdenes.
            GlobalScope.launch {
                delay(1000L)
                loadNotificaciones()
            }

        }
    }

    public fun loadNotificaciones() {
        viewModelScope.launch {
            NotificacionesRepo.getNotificaciones(notificaciones)
        }
    }

}