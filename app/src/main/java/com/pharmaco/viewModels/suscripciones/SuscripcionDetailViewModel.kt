package com.pharmaco.viewModels.suscripciones

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pharmaco.models.Suscripcion
import java.lang.Math.ceil
import java.text.SimpleDateFormat
import java.util.*

class SuscripcionDetailViewModel(private val suscripcion: MutableLiveData<Suscripcion>): ViewModel(){

    private val diaSemana: MutableLiveData<String> by lazy {
        loadDiaSemana()
    }

    private val hora: MutableLiveData<String> by lazy {
        loadHora()
    }

    //frecuencia de citas (en semanas)
    private val frecuencia: MutableLiveData<Int> by lazy {
        loadFrecuencia()
    }

    fun getSuscripcion(): LiveData<Suscripcion> {
        return suscripcion
    }

    fun getDiaSemana(): LiveData<String> {
        return diaSemana
    }

    fun getHora(): LiveData<String> {
        return hora
    }

    fun getFrecuencia(): LiveData<Int>{
        return frecuencia
    }

    private fun loadDiaSemana():MutableLiveData<String> {

        //Contar cuántas entregas se han realizado.

        val mSuscripcion = suscripcion.value


        var entregas = mSuscripcion?.fechaEntrega?.size

        //Si no hay entregas realizadas
        if (entregas == null) {
            entregas = 0
        }

        //La fecha correspondiente será última entregas o entregas-1 si entregas == fechaAgendada.size

        //Si ya se hicieron todas las entregas, el día estará dado por el último día de entrega
        var fecha: Date? = null
        if (entregas == mSuscripcion?.fechaAgendada?.size) {
            if (entregas != null)
                fecha = mSuscripcion?.fechaEntrega?.get(entregas - 1)
        } else {
            if (entregas != null)
                fecha = mSuscripcion?.fechaAgendada?.get(entregas)
        }

        val diasSemana = arrayOf("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado")

        val mlp: MutableLiveData<String> = MutableLiveData()
        if (fecha != null) {

            mlp.value = diasSemana[fecha.day]
            return mlp
        }
        //Nunca debería entrar al else!
        else {
            mlp.value = "Error Fatal!"
            return mlp
        }
    }

    private fun loadHora(): MutableLiveData<String>{
        //Contar cuántas entregas se han realizado.
        val mSuscripcion = suscripcion.value


        var entregas = mSuscripcion?.fechaEntrega?.size

        //Si no hay entregas realizadas
        if(entregas == null){
            entregas = 0
        }

        //La fecha correspondiente será última entregas o entregas-1 si entregas == fechaAgendada.size

        //Si ya se hicieron todas las entregas, el día estará dado por el último día de entrega
        var fecha: Date? = null
        if(entregas == mSuscripcion?.fechaAgendada?.size){
            if(entregas != null)
                fecha = mSuscripcion?.fechaEntrega?.get(entregas-1)
            }
        else{
            if(entregas != null)
                fecha = mSuscripcion?.fechaAgendada?.get(entregas)
        }

        val formatter  = SimpleDateFormat("hh:mm aa")
        val hora = formatter.format(fecha?.time)

        val mlp = MutableLiveData<String>()
        mlp.value = hora

        return mlp
    }

    private fun loadFrecuencia(): MutableLiveData<Int> {

        //Tomar las dos últimas agendadas y revisar tiempo entre ellas diviviso 7 para obtener semanas.


        val mSuscripcion = suscripcion.value
        if (mSuscripcion != null) {

            val size = mSuscripcion.fechaAgendada.size
            val lastFecha = mSuscripcion.fechaAgendada[size - 1]
            val beforeLastFecha = mSuscripcion.fechaAgendada[size - 2]

            lastFecha.time
            val timeMilis = lastFecha.time - beforeLastFecha.time

            //milias a días y después dividido 7 para semanas
            var weeks = ((timeMilis.toDouble() / 86400000.00) / 7.00)

            val weeksInt = ceil(weeks).toInt()

            val mlp = MutableLiveData<Int>()
            mlp.value = weeksInt
            return mlp

        }

        return MutableLiveData()
    }
}