package com.pharmaco.viewModels.suscripciones

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.pharmaco.models.Suscripcion
import com.pharmaco.repo.suscripciones.SuscripcionesRepo
import com.pharmaco.viewModels.Routes
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class SuscripcionesViewModel: ViewModel(){
    private val TAG = this.javaClass.toString()

    private val suscripcionesProximas: MutableLiveData<MutableList<Suscripcion>> =
        MutableLiveData<MutableList<Suscripcion>>()

    init {
        addDBListener()
        loadSuscripcionesProximas()
    }

    private fun addDBListener() {

        val db = FirebaseFirestore.getInstance()
        val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID).collection(Routes.SUSCRIPCIONES_ROUTE)

        docRef.addSnapshotListener { snapshot, e ->

            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }


            Log.d(TAG, "ON SNAPSHOT LISTENER")
            loadSuscripcionesProximas()
        }
    }

    fun getSuscripcionesProximas(): LiveData<MutableList<Suscripcion>> {
        return suscripcionesProximas
    }

    fun deleteSuscripcion(posSuscripcion: Int){
        viewModelScope.launch {
            SuscripcionesRepo.deleteSuscripcion(posSuscripcion, suscripcionesProximas)
        }
    }

    fun loadSuscripcionesProximas(){
        viewModelScope.launch {
            SuscripcionesRepo.loadSuscripcionesProximas(suscripcionesProximas)
        }
    }

    //posTurno indica la posición del turno en la lista turnosProximos
    /*fun deleteSuscripcion(posSuscripcion: Int){

        val db = FirebaseFirestore.getInstance()

        //Primero, quitar de MutableLiveData
        val suscripciones = suscripcionesProximas.value

        if(suscripciones != null) {
            val suscripcion = suscripciones.get(posSuscripcion)
            suscripciones.removeAt(posSuscripcion)

            suscripcionesProximas.value = suscripciones

            db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.SUSCRIPCIONES_ROUTE).document(suscripcion.id)
                .update("cancelado", true)
                .addOnSuccessListener {
                    Log.d(TAG, "Documento marcado como eliminado")
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error eliminando el documento", e)
                }
        }
        else{
            Log.w(TAG, "Error eliminando el documento, turnosProximos inesperadamente nulo")
        }
    }*/

    /*private fun loadHora(mSuscripcion: Suscripcion): String{

        //Contar cuántas entregas se han realizado.
        var entregas = mSuscripcion?.fechaEntrega?.size

        //Si no hay entregas realizadas
        if(entregas == null){
            entregas = 0
        }

        //La fecha correspondiente será última entregas o entregas-1 si entregas == fechaAgendada.size

        //Si ya se hicieron todas las entregas, el día estará dado por el último día de entrega
        var fecha: Date? = null
        if(entregas == mSuscripcion?.fechaAgendada?.size){
            if(entregas != null)
                fecha = mSuscripcion?.fechaEntrega?.get(entregas-1)
        }
        else{
            if(entregas != null)
                fecha = mSuscripcion?.fechaAgendada?.get(entregas)
        }

        val formatter  = SimpleDateFormat("hh:mm aa")
        val hora = formatter.format(fecha?.time)

        return hora
    }

    private fun loadFrecuencia(mSuscripcion: Suscripcion): Int {

        //Tomar las dos últimas agendadas y revisar tiempo entre ellas diviviso 7 para obtener semanas.

        val size = mSuscripcion.fechaAgendada.size
        val lastFecha = mSuscripcion.fechaAgendada[size - 1]
        val beforeLastFecha = mSuscripcion.fechaAgendada[size - 2]

        lastFecha.time
        val timeMilis = lastFecha.time - beforeLastFecha.time

        //milias a días y después dividido 7 para semanas
        var weeks = ((timeMilis.toDouble() / 86400000.00) / 7.00)

        val weeksInt = Math.ceil(weeks).toInt()
        return weeksInt

    }

    private fun loadDiaSemana(mSuscripcion: Suscripcion): String {

        //Contar cuántas entregas se han realizado.

        var entregas = mSuscripcion.fechaEntrega?.size

        //La fecha correspondiente será última entregas o entregas-1 si entregas == fechaAgendada.size

        //Si ya se hicieron todas las entregas, el día estará dado por el último día de entrega
        //Si no hay entregas, el último día estará dado por la primera fecha programada
        var fecha: Date? = null
        if (entregas == mSuscripcion?.fechaAgendada?.size) {
            if (entregas != null)
                fecha = mSuscripcion?.fechaEntrega?.get(entregas - 1)
        }else if(entregas == null){
            fecha = mSuscripcion.fechaAgendada.get(0)
        }
        else {
            if (entregas != null)
                fecha = mSuscripcion?.fechaAgendada?.get(entregas)
        }

        val diasSemana = arrayOf("Domingos", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado")

        val mlp: MutableLiveData<String> = MutableLiveData()
        if (fecha != null) {
            return diasSemana[fecha.day]
        }
        //Nunca debería entrar al else!
        else {
            return "Error Fatal!"
        }
    }

    private fun loadSuscripcionesProximas() {

        val db = FirebaseFirestore.getInstance()

        //Recuperar las ordenes de este usuario de la base de datos

        val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID).collection(Routes.SUSCRIPCIONES_ROUTE)

        val list = mutableListOf<Suscripcion>()

        docRef.whereEqualTo("cancelado", false).orderBy("fechaInicio", Query.Direction.DESCENDING).get()
            .addOnSuccessListener { result ->

                Log.d(TAG, "EN RESULT DE SUSCRIPS")

                for (document in result) {

                    Log.d(TAG, "${document.id} => ${document.data}")

                    val suscripcion = document.toObject(Suscripcion::class.java)
                    suscripcion.id = document.id
                    suscripcion.hora = loadHora(suscripcion)
                    suscripcion.diaSemana = loadDiaSemana(suscripcion)
                    suscripcion.frecuencia = loadFrecuencia(suscripcion)
                    list.add(suscripcion)
                }
                suscripcionesProximas.value = list
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }*/
}