package com.pharmaco.viewModels.suscripciones

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import com.pharmaco.models.*
import com.pharmaco.repo.suscripciones.SuscripcionesRepo
import com.pharmaco.viewModels.interfaces.CreateViewModelInterface
import com.pharmaco.viewModels.Routes
import com.pharmaco.viewModels.interfaces.MapViewModelInterface
import com.pharmaco.viewModels.interfaces.PharmacoViewModelInterface
import kotlinx.coroutines.launch
import java.util.*

class SuscripcionEditViewModel:  ViewModel(), PharmacoViewModelInterface {

    val TAG = this.javaClass.toString()

    var suscripcion: Suscripcion? = null

    val orden: Orden? = null

    fun editSuscripcion(fechaModificacion: Date, frecuencia: Int, fechaFin: Date){
        viewModelScope.launch {
            SuscripcionesRepo.editSuscripcion(fechaModificacion, frecuencia, fechaFin,  suscripcion!!)
        }
    }

    /*fun setMedicamentosSeleccionados(medicamentos: MutableList<MedicamentoTSD>, ordenesReference: List<DocumentReference>){


        var counter = ordenesReference.size
        val ordenes = mutableListOf<Orden>()

        for (ordenRef in ordenesReference) {

            Log.d(TAG, "ASKING FOR REFERENCE ${ordenRef}")
            ordenRef.get().addOnSuccessListener { doc ->

                Log.d(TAG, "GOT RESPONSE ${doc}")
                if(doc != null) {
                    //synchronized(this) {

                        Log.d(TAG, "ORDEN GET ${doc.toObject(Orden::class.java)!!}")

                        counter--
                        ordenes.add(doc.toObject(Orden::class.java)!!)
                    //}
                }
            }.addOnFailureListener { e ->
                Log.w(TAG, "Error", e)
            }
        }

        //Machete que bloquea hasta que no acaben todas las peticiones a la BD
        while(counter != 0) {

        }

        val db = FirebaseFirestore.getInstance()

        val medicamentosTSDCreate = mutableListOf<MedicamentoTSDCreate>()

        //Por cada orden, recorrer frecuencias y recorrer medicamentos.
        //Si el medicamento se encuentra en una orden, se genera un medicamento TSD con la referencia a esa orden.

        for(medicamento in medicamentos){

            val found = false
            for(orden in ordenes) {
                if (!found) {
                    //Ahora buscar el medicamento en las frecuencias de la orden.
                    for (frecuencia in orden.frecuencias) {

                        if (medicamento.nombre == frecuencia.medicamento.nombre) {


                            //Referencia de la orden
                            val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                                .collection(Routes.ORDENES_ROUTE).document(orden.id)


                            //Crear un nuevo medicamentoTSD create con base en este medicamento y esta orden
                            val medicamentoTSD = MedicamentoTSDCreate(
                                unidadesPedidas = medicamento.unidadesPedidas,
                                dosisCaja = medicamento.dosisCaja, nombre = medicamento.nombre,
                                nombreLab = medicamento.nombreLab, dosisNumero = medicamento.dosisNumero,
                                dosisUnidad = medicamento.dosisUnidad, ordenReference = docRef
                            )

                            medicamentosTSDCreate.add(medicamentoTSD)


                            //Se emcontró la orden de este medicamento, no se debe seguir buscando para este medicamento
                            break
                        }

                    }
                }
                else{
                    break
                }
            }
        }
        medicamentosSeleccionados.value = medicamentosTSDCreate
    }*/

    //Esta función fue tomada de https://www.journaldev.com/700/java-date-add-days-subtract-days-calendar
   /* private fun addDays(date: Date, days: Int): Date {
        val cal = GregorianCalendar()
        cal.time = date
        cal.add(Calendar.DATE, days)

        return cal.time
    }

    fun editSuscripcion(fechaModificacion: Date, frecuencia: Int, fechaFin: Date){

        val db = FirebaseFirestore.getInstance()

        val fechasModificacion = mutableListOf<Date>()

        var nuevaFecha = fechaModificacion

        while(nuevaFecha.before(fechaFin)){
            fechasModificacion.add(nuevaFecha)
            nuevaFecha = addDays(nuevaFecha, 7*frecuencia)
        }

        //Obtener actual
        val fechasAgendadas = suscripcion!!.fechaAgendada


        val nuevasFechas = mutableListOf<Date>()

        var entsSize = 0
        if (suscripcion!!.fechaEntrega != null){
            entsSize = suscripcion!!.fechaEntrega!!.size
        }

        for(i in 0 until entsSize){

            nuevasFechas.add(fechasAgendadas.get(i))

        }

        nuevasFechas.addAll(fechasModificacion)

        //Ya se tiene la nueva lista de fechas, modificarla a Timestramp
        val fechasTimestamp = mutableListOf<Timestamp>()
        for(nF in nuevasFechas){
            fechasTimestamp.add(Timestamp(nF))
        }

        //Actualizar

        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
            .collection(Routes.SUSCRIPCIONES_ROUTE)
            .document(suscripcion!!.id)
            .update(
                "fechaFin", Timestamp(fechaFin),
                "fechaAgendada", fechasTimestamp
            )
            .addOnSuccessListener {
                Log.d(TAG, "Documento actualizado: ${suscripcion!!.id}")
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error creando el documento", e)
            }

    }*/

}