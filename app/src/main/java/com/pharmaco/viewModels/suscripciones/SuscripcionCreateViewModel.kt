package com.pharmaco.viewModels.suscripciones

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pharmaco.models.*
import com.pharmaco.repo.farmacias.FarmaciasRepo
import com.pharmaco.repo.medicamentos.MedicamentosRepo
import com.pharmaco.repo.suscripciones.SuscripcionesRepo
import com.pharmaco.viewModels.interfaces.CreateViewModelInterface
import com.pharmaco.viewModels.interfaces.MapViewModelInterface
import com.pharmaco.viewModels.interfaces.PharmacoViewModelInterface
import kotlinx.coroutines.launch
import java.util.*


class SuscripcionCreateViewModel: ViewModel(), CreateViewModelInterface, MapViewModelInterface,
    PharmacoViewModelInterface {

    override val selectedFarmacia: MutableLiveData<FarmaciaTSD>  = MutableLiveData()


    override val farmaciasDisponibles: MutableLiveData<MutableList<FarmaciaTSD>> by lazy {

        MutableLiveData<MutableList<FarmaciaTSD>>().also {
            viewModelScope.launch {
                FarmaciasRepo.loadFarmacias(farmaciasDisponibles)
            }
        }

    }
    override val TAG = this.javaClass.toString()

    override val medicamentos: MutableLiveData<MutableList<MedicamentoCreate>> = MutableLiveData<MutableList<MedicamentoCreate>>()

    override val medicamentosSeleccionados: MutableLiveData<MutableList<MedicamentoTSDCreate>> = MutableLiveData()

    override var orden: Orden? = null

    init{
        loadMedicamentos()
    }

    fun createSuscripcion(fechaInicio: Date , frecuencia: Int, fechaFin: Date){
        viewModelScope.launch {
            SuscripcionesRepo.createSuscripcion(fechaInicio, frecuencia, fechaFin,
                medicamentosSeleccionados, selectedFarmacia)
        }
    }

    override fun loadMedicamentos() {
        viewModelScope.launch {
            MedicamentosRepo.loadMedicamentos(medicamentos)
        }
    }

    //Esta función fue tomada de https://www.journaldev.com/700/java-date-add-days-subtract-days-calendar
   /* private fun addDays(date: Date, days: Int): Date {
        val cal = GregorianCalendar()
        cal.time = date
        cal.add(Calendar.DATE, days)

        return cal.time
    }


    //frecuencia = número de semanas entre cada cita.
    fun createSuscripcion(fechaInicio: Date , frecuencia: Int, fechaFin: Date){

        val medicamentos = medicamentosSeleccionados.value


        if(medicamentos != null){
            val ordenes = mutableListOf<DocumentReference>()
            val medicamentosTSD = mutableListOf<MedicamentoTSD>()


            for(med in medicamentos) {
                ordenes.add(med.ordenReference)
                medicamentosTSD.add(med.toMedicamentoTSD())
            }

            //PRIMERO, con String de farmacia, obtener Farmacia!
            val db = FirebaseFirestore.getInstance()

            //ordenes entra con entradas repetidas, se deben eliminar
            val ordenesDistinct = ordenes.distinct().toMutableList()

            val fechasAgendada = mutableListOf<Date>()

            var nuevaFecha = fechaInicio

            while(nuevaFecha.before(fechaFin)){
                fechasAgendada.add(nuevaFecha)
                nuevaFecha = addDays(nuevaFecha, 7*frecuencia)
            }

            val suscripcion = Suscripcion(fechaInicio = fechaInicio, fechaFin = fechasAgendada.last(),
                fechaAgendada = fechasAgendada, farmacia = selectedFarmacia!!.value!!, medicamentos = medicamentosTSD,
                ordenes = ordenesDistinct)


            db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.SUSCRIPCIONES_ROUTE).add(suscripcion.toHashMap())
                .addOnSuccessListener {suscripcionReference ->
                    Log.d(TAG, "Documento creado: $suscripcion")

                    //Ahora añadir referencia en las ordenes.
                    for (orden in ordenesDistinct) {
                        Log.d(TAG, "ORDEN IN ORDENES DISCTINCT")

                        orden.get().addOnSuccessListener {

                            Log.d(TAG, "ORDEN GET")


                            val ordenObj = it.toObject(Orden::class.java)

                            var suscripciones= ordenObj!!.suscripciones

                            if(suscripciones == null){
                                suscripciones = mutableListOf()
                            }
                            if(!suscripciones!!.contains(orden)){
                                suscripciones.add(suscripcionReference)
                            }

                            orden.update("suscripciones", suscripciones)
                                .addOnSuccessListener {
                                    Log.d(TAG, "ACTUALIZADA REFERENCIA EN ORDENES")
                                }
                                .addOnFailureListener { e ->
                                    Log.w(TAG, "Error creando el documento", e)
                                }
                        }
                    }

                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error creando el documento", e)
                }

            }
        }*/

}