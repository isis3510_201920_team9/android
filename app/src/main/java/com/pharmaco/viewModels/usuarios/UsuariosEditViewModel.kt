package com.pharmaco.viewModels.usuarios

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pharmaco.models.Usuario
import com.pharmaco.repo.usuarios.UsuariosRepo
import com.pharmaco.viewModels.Routes
import com.squareup.okhttp.Route
import kotlinx.coroutines.launch

class UsuariosEditViewModel: ViewModel(){

    val usuario: MutableLiveData<Usuario> = MutableLiveData()

    init{
        usuario.value = Usuario()
        loadUsuario()
    }

    fun editUsuario(nombre: String, eps: String, edad: Int, genero: String){
        viewModelScope.launch {

            if(usuario != null){
                UsuariosRepo.updateUsuario(Routes.USER_ID, nombre, eps, edad, genero)
                usuario.value!!.nombre = nombre
                usuario.value!!.eps = eps
                usuario.value!!.edad = edad
                usuario.value!!. genero = genero
                usuario.value = usuario.value
            }
        }
    }

    fun loadUsuario(){
        viewModelScope.launch {
            UsuariosRepo.loadUsuario(uid = Routes.USER_ID, usuarioLD = usuario)
        }
    }


}