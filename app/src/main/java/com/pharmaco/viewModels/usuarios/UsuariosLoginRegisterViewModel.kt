package com.pharmaco.viewModels.usuarios

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.RuntimeExecutionException
import com.google.firebase.auth.FirebaseAuth
import com.pharmaco.DataCache
import com.pharmaco.models.Usuario
import com.pharmaco.repo.farmacias.FarmaciasRepo
import com.pharmaco.repo.usuarios.UsuariosRepo
import com.pharmaco.viewModels.Routes
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class UsuariosLoginRegisterViewModel: ViewModel(){

    var loggedInSuccess: MutableLiveData<Boolean?> = MutableLiveData()
    var errMsg: MutableLiveData<String> = MutableLiveData()

    val auth = FirebaseAuth.getInstance()


    init{
        loggedInSuccess.value = null
    }

    fun register(email: String, password: String, eps: String, cedula: String, nombre: String,
                 genero: String, edad: Int){

        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { result ->
            if (result.isSuccessful) {

                val usuario = Usuario(email = email, eps = eps, numDocumento = cedula, nombre = nombre,
                        uid = auth.currentUser!!.uid, genero = genero, edad = edad)

                //2, registrar el usuario en la BD general
                viewModelScope.launch {
                    UsuariosRepo.createCollection(usuario)
                    Routes.setUserID()
                    DataCache.cacheData()
                    UsuariosRepo.setActive()
                    loggedInSuccess.value = true
                }

            } else{

                val err = result.exception!!.message

                if(err =="The email address is already in use by another account.") {
                    errMsg.value = "Ya existe un usuario asociado a ese correo."
                }
                else if(err == "An internal error has occurred. [ 7: ]") {
                    errMsg.value = "Lo sentimos, debes estar conectado a internet para registrarte."
                }
                else if(err =="The email address is badly formatted.") {
                    errMsg.value = "El correo electrónico no es válido."
                }
                else if(err == "The given password is invalid. [ Password should be at least 6 characters ]"){
                    errMsg.value = "Su contraseña debe tener por lo menos 6 caracteres."
                }
                else{
                    Log.d(this.javaClass.toString(), err!!)
                    errMsg.value = err
                }
                loggedInSuccess.value = false
            }
        }
    }

    fun signIn(email: String, password: String, prenderBoton: () -> Unit){

        if(email != null && password != null){
            auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { result ->

                if (result.isSuccessful) {
                    Routes.setUserID()
                    DataCache.cacheData()
                    viewModelScope.launch {
                        UsuariosRepo.setActive()
                    }
                    loggedInSuccess.value = true


                } else {

                    prenderBoton()
                    val err = result.exception!!.message

                    Log.d("ERR", err!!)
                    if(err == "An internal error has occurred. [ 7: ]"){
                        errMsg.value = "Lo sentimos, debes estar conectado a internet para poder iniciar sesión."
                    }
                    else if(err == "The email address is badly formatted."){
                        errMsg.value = "El correo electrónico no es válido."
                    }
                    else if(err == "The password is invalid or the user does not have a password." || err == "There is no user record corresponding to this identifier. The user may have been deleted."){
                        errMsg.value = "Creedenciales inválidas. Inténtalo de nuevo."
                    }
                    else {
                        errMsg.value = err
                    }
                    loggedInSuccess.value = false
                }
            }
        }
    }

}