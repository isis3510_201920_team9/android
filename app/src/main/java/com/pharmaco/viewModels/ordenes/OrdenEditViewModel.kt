package com.pharmaco.viewModels.ordenes

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestore
import com.pharmaco.models.Frecuencia
import com.pharmaco.models.MedicamentoOrden
import com.pharmaco.repo.medicamentos.MedicamentosRepo
import com.pharmaco.repo.ordenes.OrdenesRepo
import com.pharmaco.viewModels.interfaces.OrdenCreateEditViewModelInterface
import com.pharmaco.viewModels.Routes
import com.pharmaco.viewModels.interfaces.PharmacoViewModelInterface
import kotlinx.coroutines.launch

class OrdenEditViewModel: ViewModel(), OrdenCreateEditViewModelInterface, PharmacoViewModelInterface {


    override val TAG = this.javaClass.toString()

    override val frecuencias: MutableLiveData<MutableList<Frecuencia>> = MutableLiveData()

    override val medicamentosAlternativos: MutableLiveData<List<String>> = MutableLiveData()

    override val medicamentoValido: MutableLiveData<Boolean> = MutableLiveData()

    lateinit var ordenId: String


    override fun confirmarMedicamento(nombre: String) {

        viewModelScope.launch {
            MedicamentosRepo.confirmarMedicamento(nombre, medicamentoValido)
        }

    }


    /*override fun addFrecuencia(nombreMedicamento: String, dosisPorUnidad: Int, medicamentoDosisUnidad: String, tomarUnidad: Int, tomarFrecuencia: Int, tomarFrecuenciaUnidad: String, tomarPeriodo: Int, tomarPeriodoUnidad: String) {

        viewModelScope.launch {
            //Primero verificar que el medicamento, en efecto, existe
            MedicamentosRepo.buscarMedicamento(nombreMedicamento, medicamentosAlternativos)

            val medicamento = MedicamentoOrden(
                    nombre = nombreMedicamento, nombreLab = "", dosisNumero = dosisPorUnidad,
                    dosisUnidad = medicamentoDosisUnidad
            )

            val frecuencia = Frecuencia(
                    unidades = tomarUnidad, numero = tomarFrecuencia,
                    unidadFrecuencia = tomarFrecuenciaUnidad, periodo = tomarPeriodo, unidadPeriodo = tomarPeriodoUnidad,
                    medicamento = medicamento
            )


            var list = frecuencias.value

            if (list == null) {
                list = mutableListOf()
            }
            list.add(frecuencia)
            frecuencias.value = list
        }

    }*/

    override fun buscarMedicamento(nombre: String){

        viewModelScope.launch {
            MedicamentosRepo.buscarMedicamento(nombre, medicamentosAlternativos)
        }

    }


    fun setFrecuencias(nFrecuencias: MutableList<Frecuencia>) {

        frecuencias.value = nFrecuencias

    }

    fun getFrecuencias(): LiveData<MutableList<Frecuencia>> {
        return frecuencias
    }

    fun updateOrden(nombreDoctor: String, especialidad: String) {
        viewModelScope.launch {
            OrdenesRepo.updateOrden(nombreDoctor, especialidad, ordenId, frecuencias)
        }
    }

    /*fun updateOrden(nombreDoctor: String, especialidad: String) {


        val db = FirebaseFirestore.getInstance()

        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
            .collection(Routes.ORDENES_ROUTE).document(ordenId)
            .update("doctor", nombreDoctor, "especialidad", especialidad)
            .addOnSuccessListener {

                Log.d(TAG, "Document modified: ${ordenId}")

                //Eliminar todas las frecuencias
                db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                    .collection(Routes.ORDENES_ROUTE).document(ordenId).collection("frecuencias")
                    .get()
                    .addOnSuccessListener {freqs ->

                        for(freq in freqs ){

                            db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                                .collection(Routes.ORDENES_ROUTE).document(ordenId).collection("frecuencias")
                                .document(freq.id)
                                .delete()
                        }



                        //Volver a crear las frecuencias
                        val frecuenciasList = frecuencias.value

                        if(frecuenciasList != null) {

                            Log.d(TAG, "En ciclo!")
                            for (frecuencia in frecuenciasList) {

                                db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                                    .collection(Routes.ORDENES_ROUTE).document(ordenId).collection("frecuencias")
                                    .add(frecuencia.toHashMap())
                                    .addOnSuccessListener {

                                        Log.d(TAG, "Frecuencia Persistida ${frecuencia}")

                                    }
                            }
                        }

                    }

            }

    }*/

}