package com.pharmaco.viewModels.ordenes

object OrdenConstants {

    object Tipo {

        val TEXTO = 0
        val IMAGEN = 1
        val AUDIO = 2

    }

}