package com.pharmaco.viewModels.ordenes

import android.util.Log
import androidx.lifecycle.*
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.pharmaco.models.*
import com.pharmaco.repo.ordenes.OrdenesRepo
import com.pharmaco.viewModels.Routes
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class OrdenesViewModel : ViewModel() {


    //para pasar un Mutablelist como referencia en lugar de copia
    class MutableListWrapper<E>(){

        var value: MutableList<E> = mutableListOf()

    }

    private val TAG = this.javaClass.toString()

    /*private val ordenes: MutableLiveData<MutableList<Orden>> by lazy{

        MutableLiveData<MutableList<Orden>>().also{
            addDBListener()
            loadOrdenes()
        }

    }*/

    private val ordenes: MutableLiveData<MutableList<Orden>> = MutableLiveData()

    private var deletesValidos: MutableListWrapper<Boolean> = MutableListWrapper()


    init {
        addDBListener()
        loadOrdenes()
    }

    //Idea, obligar con await a sincronía, lanzarlos en un coroutine cada uno, hacer un join() de cada uno lanzado
    //al final.

    //TODO, se deja este método aquí pues en la próxima iteración se cambiará la forma en la que se verifica la validez de
    //una eliminación.
    private suspend fun checkValid(orden: Orden): Boolean   {


        //@Synchronized fun(updateCounter)

        val turnosTasks = mutableListOf<Task<DocumentSnapshot>>()

        val domiciliosTasks =  mutableListOf<Task<DocumentSnapshot>>()

        val suscripcionesTasks =  mutableListOf<Task<DocumentSnapshot>>()

        //Revisar referencias de turnos, ver si hay al menos una activa (exitoso == null).
        //Si es válido o no.
        var valid: Boolean = true
        if (orden.turnos != null) {
            val turnos = orden.turnos!!

            for (reference in turnos) {

                turnosTasks.add(reference.get())

            }

        }

        //Revisar referencias de domicilios, ver si hay al menos una activa (exitoso == null).
        //Solo revisar si no se ha encontrado ya una condición que invalide la ejecución.

        if (valid && orden.domicilios != null) {
            val domicilios = orden.domicilios!!

            for (reference in domicilios) {

                domiciliosTasks.add(reference.get())

            }
        }

        //Revisar referencias de suscripciones, ver si hay al menos una activa (cancelado == false)

        if (valid && orden.suscripciones != null) {
            val suscripciones = orden.suscripciones!!

            for (reference in suscripciones) {

                val a = reference.get().await()

                Log.d(TAG, a.toString())

                suscripcionesTasks.add(reference.get())

            }
        }

        for (turnoTask in turnosTasks){

            val pTurno = turnoTask.await()
            val turno = pTurno.toObject(Turno::class.java)

            if(turno?.exitoso == null){
                return false
            }
        }

        for(domicilioTask in domiciliosTasks){

            val pDomicilio = domicilioTask.await()
            val domicilio = pDomicilio.toObject(Domicilio::class.java)

            if(domicilio?.exitoso == null){
                return false
            }
        }

        for(suscripcionTask in suscripcionesTasks){

            Log.d(TAG, "Before Await")
            val pSuscripcion = suscripcionTask.await()
            Log.d(TAG, "after Await")
            val suscripcion = pSuscripcion.toObject(Suscripcion::class.java)

            if(!suscripcion!!.cancelado){
                return false
            }

        }

        return true
    }

    //Retorna falso si no se eliminó, verdadero si sí.
    fun deleteOrden(posOrden: Int): Boolean{


        Log.d(TAG, "DELETES VALIDOS: ${deletesValidos.value}")

        //Si no se cumplen requisitos, no eliminar
        if(!deletesValidos.value!!.get(posOrden)){
            ordenes.value = ordenes.value
            return false
        }

        viewModelScope.launch {
            OrdenesRepo.deleteOrden(posOrden, ordenes, deletesValidos)
        }
        return true

        /*val tordenes = ordenes.value

        if(tordenes != null) {

            val orden = tordenes.get(posOrden)

            tordenes.removeAt(posOrden)

            ordenes.value = tordenes

            val db = FirebaseFirestore.getInstance()


            //Marcar documento como eliminado
            db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.ORDENES_ROUTE).document(orden.id)
                .update("borrada", true)
                .addOnSuccessListener {
                    Log.d(TAG, "Documento marcado como eliminado")
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error eliminando el documento", e)
                }

            return true
        }
        else{
            Log.w(TAG, "Error eliminando el documento, turnosProximos inesperadamente nulo")

            return false
        }*/

    }


    fun getOrdenes(): LiveData<MutableList<Orden>> {
        return ordenes
    }

    private fun addDBListener() {

        val db = FirebaseFirestore.getInstance()
        val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID).collection(Routes.ORDENES_ROUTE)

        docRef.addSnapshotListener { snapshot, e ->

            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            //Lanzar corrutina que espere y vuelva a cargar órdenes.
            GlobalScope.launch {
                delay(1000L)
                loadOrdenes()
            }

        }
    }

    fun loadOrdenes(){
        viewModelScope.launch {
            OrdenesRepo.loadOrdenes(ordenes, deletesValidos)

        }
       /* val handlerThread = OrdenesHandlerThread()

        GlobalScope.launch {
            handlerThread.start()

            //Se debe esperar, pues de modo contrario el looper no alcanza a inicializarse y la siguiente linea
            //lanza null pointer.
            while(handlerThread.getHandler() == null) {
                delay(20L)
            }

            handlerThread.getHandler()!!.post(OrdenesGetRunnable(ordenes, deletesValidos, this.javaClass.toString()))
            handlerThread.quitSafely()
        }*/
    }


    /*override fun onCleared() {
        super.onCleared()
        OrdenesRepo.saveOrdenesLocal(this.getApplication(), ordenes.value)

    }*/

    /*private fun loadOrdenes() {


        @Synchronized fun validListReplace(validList: MutableList<Boolean>, i: Int, valido: Boolean){
            validList.add(i, valido)
            validList.removeAt(i+1)
        }

        val db = FirebaseFirestore.getInstance()

        //Recuperar las ordenes de este usuario de la base de datos

        val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID).collection(Routes.ORDENES_ROUTE)

        val list = mutableListOf<Orden>()

        val validList = mutableListOf<Boolean>()

        docRef.whereEqualTo("borrada", false).orderBy("fecha", Query.Direction.DESCENDING).get()
            .addOnSuccessListener { result ->

                var counter = 0
                for (document in result) {

                    Log.d(TAG, "${document.id} => ${document.data}")

                    val orden = document.toObject(Orden::class.java)
                    orden.id = document.id
                    orden.reference = document.reference
                    list.add(orden)

                    // por cada orden, se deben traer sus frecuencias
                    val freqRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                        .collection(Routes.ORDENES_ROUTE).document(document.id).collection(Routes.FRECUENCIAS_ROUTE)

                    val freqList = mutableListOf<Frecuencia>()

                    freqRef.get().addOnSuccessListener { frecuencias ->
                        for(document in frecuencias){
                            Log.d(TAG, "${document.id} => ${document.data}")
                            freqList.add(document.toObject(Frecuencia::class.java))
                        }
                        orden.frecuencias = freqList
                        ordenes.value = list
                    }

                    var valido: Boolean = true

                    validList.add(valido)

                    if(orden.turnos != null){

                        val references = orden.turnos!!

                        val i = counter

                        for(reference in references){

                            reference.get().addOnSuccessListener { pTurno ->

                                val turno = pTurno.toObject(Turno::class.java)

                                if(turno != null) {

                                    valido = valido!! && !(turno?.exitoso == null)
                                }

                                validListReplace(validList, i, valido)
                                //validList.add(i, valido)
                                //validList.removeAt(i+1)

                                deletesValidos = validList
                            }

                        }
                    }

                    if(orden.domicilios != null){

                        val references = orden.domicilios!!

                        val i = counter

                        for(reference in references){

                            reference.get().addOnSuccessListener { pDomicilio ->

                                val domicilio = pDomicilio.toObject(Domicilio::class.java)

                                if(valido != null) {

                                    valido = valido!! && !(domicilio?.exitoso == null)
                                }

                                validListReplace(validList, i, valido)
                                //validList.add(i, valido)
                                //validList.removeAt(i+1)

                                deletesValidos = validList
                            }

                        }
                    }

                    if(orden.suscripciones != null){


                        val references = orden.suscripciones!!

                        val i = counter

                        for(reference in references){

                            reference.get().addOnSuccessListener { pSuscripcion ->

                                val suscripcion = pSuscripcion.toObject(Suscripcion::class.java)

                                if(suscripcion != null) {


                                    Log.d(TAG, "${valido}")
                                    Log.d(TAG, "${suscripcion}")
                                    Log.d(TAG, "${suscripcion?.cancelado!!}")
                                    valido = valido!! && (suscripcion?.cancelado!!)


                                    validListReplace(validList, i, valido)
                                    //validList.add(i, valido)
                                    //validList.removeAt(i+1)

                                    deletesValidos = validList
                                    Log.d(TAG, "VALIDOS ${deletesValidos.toString()}")
                                }
                            }

                        }

                    }

                    counter++
                }
                ordenes.value = list
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }*/
}