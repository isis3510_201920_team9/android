package com.pharmaco.viewModels.ordenes


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pharmaco.models.Orden

class OrdenDetailViewModel(private val orden: MutableLiveData<Orden>): ViewModel(){

    fun getOrden():  MutableLiveData<Orden>{
        return orden
    }

}
