package com.pharmaco.viewModels.ordenes

import android.graphics.BitmapFactory
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.pharmaco.models.Frecuencia
import com.pharmaco.models.MedicamentoOrden
import com.pharmaco.repo.medicamentos.MedicamentosRepo
import com.pharmaco.repo.ordenes.OrdenesRepo
import com.pharmaco.viewModels.interfaces.OrdenCreateEditViewModelInterface
import com.pharmaco.viewModels.interfaces.PharmacoViewModelInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.io.File
import java.lang.Exception

class OrdenCreateViewModel: ViewModel(), OrdenCreateEditViewModelInterface, PharmacoViewModelInterface {

    override val TAG = this.javaClass.toString()

    override val frecuencias: MutableLiveData<MutableList<Frecuencia>> = MutableLiveData()

    override val medicamentosAlternativos: MutableLiveData<List<String>> = MutableLiveData()

    override val medicamentoValido: MutableLiveData<Boolean> = MutableLiveData()

    val successfulImage: MutableLiveData<Boolean> = MutableLiveData()

    var added: Int = 0

    init{
        successfulImage.value = true
    }

    override fun confirmarMedicamento(nombre: String) {

        viewModelScope.launch {
            MedicamentosRepo.confirmarMedicamento(nombre,medicamentoValido)
        }

    }

    override fun buscarMedicamento(nombre: String){

        viewModelScope.launch {
            MedicamentosRepo.buscarMedicamento(nombre, medicamentosAlternativos)
        }

    }

    fun getFrecuencias(): LiveData<MutableList<Frecuencia>> {
        return frecuencias
    }

    fun addOrden(nombreDoctor: String? = null, especialidad: String? = null, tipo: Int, imagen: File? = null) {

        when (tipo) {
            OrdenConstants.Tipo.TEXTO -> viewModelScope.launch {
                OrdenesRepo.addOrdenTexto(nombreDoctor!!, especialidad!!, frecuencias)
            }
            OrdenConstants.Tipo.IMAGEN -> viewModelScope.launch {
                //OrdenesRepo.addOrdenImagen(imagen!!)
                interpretImage(imagen!!)

            }
            OrdenConstants.Tipo.AUDIO -> viewModelScope.launch {
                //TODO, y si es audio
            }
        }
    }

    suspend fun interpretImage(imagen: File) = withContext(Dispatchers.Default){

        added = 0

        val bitmap = BitmapFactory.decodeFile(imagen.path)
        val fireVisionImg = FirebaseVisionImage.fromBitmap(bitmap)

        val detector = FirebaseVision.getInstance().onDeviceTextRecognizer

        detector.processImage(fireVisionImg)
            .addOnSuccessListener { firebaseVisionText ->
                Log.d(TAG, firebaseVisionText.text)

                //Bloque para debug
                /*for(block in firebaseVisionText.textBlocks){

                    Log.d(TAG, "BLOCK TEXT: ${block.text}")

                    for(line in block.lines){

                        Log.d(TAG, "    LINE TEXT: ${line.text}")

                        for(element in line.elements){
                            Log.d(TAG, "        ELEMENT TEXT: ${element.text}")
                        }
                    }

                }*/

                var encontroInicio = false
                var unidadNoEsCero = false
                //Buscar bloque cuyo texto sea "Medicamento y Prescripción"
                for(block in firebaseVisionText.textBlocks){
                    Log.d(TAG, "BLOCK TEXT: ${block.text}")

                    //Una vez se encuentra la línea "Medicamento y Prescripción" se intentan interpretar todas las
                    //líneas siguiente como si fueran un medicameno que sigue el formato:
                    //Tomar (via XYZ) <Número> cada <Número> hora(s) por <Número> día(s).
                    if(encontroInicio){

                        if(block.lines.size >= 2){
                            //Primera línea debe corresponder a el medicamento
                            var nombreMedicamento = block.lines.get(0).text
                            var medicamentoDosisUnidad = "Err"
                            var dosisPorUnidad = 0

                            //Buscar si alguno de los elementos de la primera línea es "mg", "g" o "kg"
                            for(element in block.lines.get(0).elements){
                                if(element.text.contains("mg") || element.text.contains("g")
                                    || element.text.contains("kg")){

                                    medicamentoDosisUnidad = element.text.toLowerCase().replace("\\d", "")
                                    try{
                                        dosisPorUnidad =  element.text.toLowerCase()
                                            .replace("\\D", "").toInt()
                                    }
                                    catch (e: Exception){
                                        dosisPorUnidad = 0
                                    }
                                }
                            }

                            //Segunda línea debe corresponder a la frecuencia en formato:
                            //Tomar (via XYZ) <Número> cada <Número> hora(s) por <Número> día(s).

                            var tomarUnidad = 0
                            var tomarFrecuencia = 0
                            var tomarFrecuenciaUnidad = "Err"
                            var tomarPeriodo = 0
                            var tomarPeriodoUnidad = "Err"
                            var i = 0

                            for(element in block.lines.get(1).elements){
                                if(element.text.toLowerCase() == "tomar"){

                                    //Si se encuentra "Tomar", 3 elementos adelante se espera encontrar la cantidad correspondiente (dosisPorUnidad)
                                    try{
                                        tomarUnidad = block.lines.get(1).elements.get(i+3).text.toInt()
                                    }
                                    catch (e: Exception){
                                        tomarUnidad = 0
                                    }

                                    //Seis elementos más adelante se espera encontrar el cada cuántas horas
                                    try{
                                        tomarFrecuencia = block.lines.get(1).elements.get(i+6).text.toInt()
                                    }
                                    catch(e: Exception){
                                        tomarFrecuencia = 0
                                    }

                                    //Unidad de la frecuencia está 7 más adelante
                                    try{
                                        tomarFrecuenciaUnidad = block.lines.get(1).elements.get(i+7).text.toLowerCase()
                                            .replace("(", "").replace(")", "")
                                    }
                                    catch(e: Exception){
                                        tomarFrecuenciaUnidad = "Err"
                                    }

                                    //el periodo estará 9 más adelante
                                    try{
                                        tomarPeriodo = block.lines.get(1).elements.get(i+9).text.toInt()
                                    }
                                    catch(e: Exception){
                                        tomarPeriodo = 0
                                    }

                                    //Unidad del periodo está 10 más adelante
                                    try{
                                        tomarPeriodoUnidad = block.lines.get(1).elements.get(i+10).text.toLowerCase()
                                            .replace("(", "").replace(")", "")
                                    }
                                    catch(e: Exception){
                                        tomarPeriodoUnidad = "Err"
                                    }

                                    if(tomarUnidad > 0 && tomarFrecuencia > 0 && tomarPeriodo > 0) {
                                        Log.d(TAG, "ADD FRECUENCIA")
                                        addFrecuencia(nombreMedicamento, dosisPorUnidad, medicamentoDosisUnidad, tomarUnidad,
                                                tomarFrecuencia, tomarFrecuenciaUnidad, tomarPeriodo, tomarPeriodoUnidad)
                                        added++
                                        unidadNoEsCero = true
                                    }
                                    break
                                }

                                i++
                            }

                        }

                    }

                    if(block.text.toLowerCase().contains("medicamento y prescripción") || block.text.toLowerCase().contains("medicamento y prescipcion")){
                        Log.d(TAG,"CONTAINS MED Y PRESCRIPCIÓN")
                        encontroInicio = true
                    }
                }


                imagen.delete()

                successfulImage.value = encontroInicio && unidadNoEsCero
                Log.d(TAG, "SUCCIMG VAL ${successfulImage.value}")

            }
            .addOnFailureListener {
                Log.d(TAG, it.toString())
            }

    }.await()

    /*fun addOrden(nombreDoctor: String, especialidad: String){

        //TODO, por ahora sólo se reciben entradas de texto, audio e imágenes no está implementado aún
        val orden = Orden(doctor = nombreDoctor, fecha = Date(), tipo = "texto", especialidad = especialidad)


        val db = FirebaseFirestore.getInstance()

        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
            .collection(Routes.ORDENES_ROUTE).add(orden.toHashMap())
            .addOnSuccessListener {dr ->
                val newId = dr.id
                Log.d(TAG, "Documento creado: $orden")


                val freqList = frecuencias.value
                if(freqList != null) {
                    for (freq in freqList) {

                        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                            .collection(Routes.ORDENES_ROUTE).document(newId)
                            .collection("frecuencias")
                            .add(freq.toHashMap())
                            .addOnSuccessListener {dr ->
                                Log.d(TAG, "Documento creado: $freq")
                            }
                            .addOnFailureListener { e ->
                                Log.w(TAG, "Error creando el documento", e)
                            }
                    }
                }
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error creando el documento", e)
            }

    }*/

}