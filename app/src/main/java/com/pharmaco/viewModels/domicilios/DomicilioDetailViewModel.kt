package com.pharmaco.viewModels.domicilios


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pharmaco.models.Domicilio

class DomicilioDetailViewModel(private val domicilio: MutableLiveData<Domicilio>): ViewModel(){

    fun getDomicilio():  MutableLiveData<Domicilio>{
        return domicilio
    }

}
