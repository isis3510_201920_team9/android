package com.pharmaco.viewModels.domicilios

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.pharmaco.models.Domicilio
import com.pharmaco.models.Orden
import com.pharmaco.models.Turno
import com.pharmaco.repo.domicilios.DomiciliosRepo
import com.pharmaco.viewModels.Routes
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class DomiciliosViewModel: ViewModel(){

    private val TAG = this.javaClass.toString()

    private val domiciliosProximos: MutableLiveData<MutableList<Domicilio>> = MutableLiveData<MutableList<Domicilio>>()

    init{
        loadDomiciliosProximos()
        addDBListener()
    }


    private fun addDBListener() {

        val db = FirebaseFirestore.getInstance()
        val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.DOMICILIOS_ROUTE)

        docRef.addSnapshotListener { snapshot, e ->

            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            //Lanzar corrutina que espere y vuelva a cargar órdenes.
            GlobalScope.launch {
                delay(1000L)
                loadDomiciliosProximos()
            }

        }
    }

    fun deleteDomicilio(posDomicilio: Int){
        viewModelScope.launch {
            DomiciliosRepo.deleteDomicilio(posDomicilio, domiciliosProximos)
        }
    }

    //posTurno indica la posición del turno en la lista turnosProximos
    /*fun deleteDomicilio(posDomicilio: Int){

        val db = FirebaseFirestore.getInstance()

        //Primero, quitar de MutableLiveData
        val domicilios = domiciliosProximos.value

        if(domicilios != null) {
            val domicilio = domicilios.get(posDomicilio)
            domicilios.removeAt(posDomicilio)

            domiciliosProximos.value = domicilios

            val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.DOMICILIOS_ROUTE).document(domicilio.id)

                docRef.update("exitoso", false, "novedades", "Domicilio cancelado por el usuario")
                .addOnSuccessListener {
                    Log.d(TAG, "Documento marcado como eliminado")

                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error eliminando el documento", e)
                }
        }
        else{
            Log.w(TAG, "Error eliminando el documento, turnosProximos inesperadamente nulo")
        }
    }*/

    fun getDomiciliosProximos(): LiveData<MutableList<Domicilio>>{
        return domiciliosProximos
    }

    fun loadDomiciliosProximos() {
        viewModelScope.launch {
            DomiciliosRepo.loadDomiciliosProximos(domiciliosProximos)
        }
    }

    /*private fun loadDomiciliosProximos(){

        val db = FirebaseFirestore.getInstance()

        //Recuperar las ordenes de este usuario de la base de datos

        val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID).collection(Routes.DOMICILIOS_ROUTE)

        val list = mutableListOf<Domicilio>()

        docRef.whereEqualTo("exitoso", null).orderBy("fechaAgendada", Query.Direction.DESCENDING).get()
            .addOnSuccessListener { result ->

                for (document in result) {

                    Log.d(TAG, "${document.id} => ${document.data}")

                    val domicilio = document.toObject(Domicilio::class.java)
                    domicilio.id = document.id
                    list.add(domicilio)
                }

                domiciliosProximos.value = list
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }*/

}