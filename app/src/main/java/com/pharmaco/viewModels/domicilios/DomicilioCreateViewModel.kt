package com.pharmaco.viewModels.domicilios

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pharmaco.models.*
import com.pharmaco.repo.medicamentos.MedicamentosRepo
import com.pharmaco.repo.domicilios.DomiciliosRepo
import com.pharmaco.repo.farmacias.FarmaciasRepo
import com.pharmaco.viewModels.interfaces.CreateViewModelInterface
import com.pharmaco.viewModels.interfaces.MapViewModelInterface
import com.pharmaco.viewModels.interfaces.PharmacoViewModelInterface
import kotlinx.coroutines.launch
import java.util.*

class DomicilioCreateViewModel: ViewModel(), CreateViewModelInterface, MapViewModelInterface,
PharmacoViewModelInterface{

    override val selectedFarmacia: MutableLiveData<FarmaciaTSD> = MutableLiveData()

    override val farmaciasDisponibles: MutableLiveData<MutableList<FarmaciaTSD>> by lazy {
        MutableLiveData<MutableList<FarmaciaTSD>>().also {
            viewModelScope.launch {
                FarmaciasRepo.loadFarmacias(farmaciasDisponibles)
            }
        }
    }

    override val TAG = this.javaClass.toString()

    override val medicamentos: MutableLiveData<MutableList<MedicamentoCreate>> = MutableLiveData<MutableList<MedicamentoCreate>>()

    override val medicamentosSeleccionados: MutableLiveData<MutableList<MedicamentoTSDCreate>> = MutableLiveData()

    override var orden: Orden? = null


    init{
        loadMedicamentos()
    }

    fun createDomicilio(direccion: String, fechaAgendada: Date){
        viewModelScope.launch {
            DomiciliosRepo.createDomicilio(direccion, fechaAgendada, selectedFarmacia, medicamentosSeleccionados)
        }
    }

    override fun loadMedicamentos() {
        viewModelScope.launch {
            MedicamentosRepo.loadMedicamentos(medicamentos)
        }
    }

    /*fun createDomicilio(direccion: String, fechaAgendada: Date) {

        val medicamentos = medicamentosSeleccionados.value


        if (medicamentos != null) {
            val ordenes = mutableListOf<DocumentReference>()
            val medicamentosTSD = mutableListOf<MedicamentoTSD>()


            for (med in medicamentos) {
                ordenes.add(med.ordenReference)
                medicamentosTSD.add(med.toMedicamentoTSD())
            }

            //PRIMERO, con String de farmacia, obtener Farmacia!
            val db = FirebaseFirestore.getInstance()


            //ordenes entra con entradas repetidas, se deben eliminar
            val ordenesDistinct = ordenes.distinct().toMutableList()

            val domicilio = Domicilio( direccion = direccion, fechaAgendada = fechaAgendada,
                farmacia = selectedFarmacia!!.value!!, medicamentos = medicamentosTSD, ordenes = ordenesDistinct)

            db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.DOMICILIOS_ROUTE).add(domicilio.toHashMap())
                .addOnSuccessListener {domicilioReference ->
                    Log.d(TAG, "Documento creado: $domicilio")

                    //Ahora añadir referencia en las ordenes.
                    for (orden in ordenesDistinct) {
                        Log.d(TAG, "ORDEN IN ORDENES DISCTINCT")

                        orden.get().addOnSuccessListener {

                            Log.d(TAG, "ORDEN GET")


                            val ordenObj = it.toObject(Orden::class.java)

                            var domicilios= ordenObj!!.domicilios

                            if(domicilios == null){
                                domicilios = mutableListOf()
                            }
                            if(!domicilios!!.contains(orden)){
                                domicilios.add(domicilioReference)
                            }

                            orden.update("domicilios", domicilios)
                                .addOnSuccessListener {
                                    Log.d(TAG, "ACTUALIZADA REFERENCIA EN ORDENES")
                                }
                                .addOnFailureListener { e ->
                                    Log.w(TAG, "Error creando el documento", e)
                                }
                        }
                    }
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error creando el documento", e)
                }
        }
    }*/

}