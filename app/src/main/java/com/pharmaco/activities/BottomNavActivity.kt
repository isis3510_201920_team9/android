package com.pharmaco.activities

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.getbase.floatingactionbutton.AddFloatingActionButton
import com.getbase.floatingactionbutton.FloatingActionButton
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.pharmaco.R
import com.pharmaco.fragments.domicilios.PedirDomicilioDialogFragment
import com.pharmaco.fragments.ordenes.AgregarOrdenDialogFragment
import com.pharmaco.fragments.suscripciones.PedirSuscripcionDialogFragment
import com.pharmaco.fragments.turnos.PedirTurnoDialogFragment
import kotlinx.android.synthetic.main.activity_bottom_nav_menu.*

@SuppressLint("Registered")
class BottomNavActivity : AppCompatActivity(){

    var fab: AddFloatingActionButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_nav_menu)

        val navController: NavController = findNavController(R.id.nav_host_fragment)
        bottom_navigation.setupWithNavController(navController)

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        fab = findViewById<AddFloatingActionButton>(R.id.fab_expand_menu_button)
        var agregarOrden: FloatingActionButton = findViewById(R.id.fabAgregarOrden)
        var pedirTurno: FloatingActionButton = findViewById(R.id.fabPedirTurno)
        var pedirDomicilio: FloatingActionButton = findViewById(R.id.fabPedirDomicilio)
        var suscribirse: FloatingActionButton = findViewById(R.id.fabSuscribirse)

        agregarOrden.setOnClickListener {
            var agregarOrden = AgregarOrdenDialogFragment()
            val fm = supportFragmentManager
            agregarOrden.show(fm, "")
            fab!!.performClick()
        }
        pedirTurno.setOnClickListener {
            var agregarOrden = PedirTurnoDialogFragment({},null, {})
            val fm = supportFragmentManager
            agregarOrden.show(fm, "")
            fab!!.performClick()
        }
        pedirDomicilio.setOnClickListener {
            var agregarOrden = PedirDomicilioDialogFragment({},null, {})
            val fm = supportFragmentManager
            agregarOrden.show(fm, "")
            fab!!.performClick()
        }
        suscribirse.setOnClickListener {
            var agregarOrden = PedirSuscripcionDialogFragment({},null, {})
            val fm = supportFragmentManager
            agregarOrden.show(fm, "")
            fab!!.performClick()
        }

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("FIREBASE ERROR", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token
                Log.d("FIREBASE TOKEN", token!!)
            })
    }
}
