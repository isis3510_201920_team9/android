package com.pharmaco.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.firebase.auth.FirebaseAuth
import com.pharmaco.R
import com.pharmaco.fragments.GenericAlertDialog
import com.pharmaco.repo.usuarios.UsuariosRepo
import com.pharmaco.viewModels.usuarios.UsuariosLoginRegisterViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject

class IniciarSesionActivity(var callBack: (() -> Unit)?=null): AppCompatActivity()  {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_iniciar_sesion)
        var botonView: Button = findViewById<Button>(R.id.botonIniciarSesion)
        var email = findViewById<EditText>(R.id.correo)
        var password = findViewById<EditText>(R.id.clave)

        val viewModel = ViewModelProviders.of(this)[UsuariosLoginRegisterViewModel::class.java]

        viewModel.loggedInSuccess.observe(this, Observer<Boolean?> {  suc ->

            if(suc != null) {
                if (suc) {
                    val intent = Intent(this, BottomNavActivity::class.java)
                    ActivityCompat.finishAffinity(this)
                    startActivity(intent)
                    finish()
                } else {
                    val alerta = GenericAlertDialog(viewModel.errMsg.value!!, false, {}, {})
                    val fm = supportFragmentManager
                    alerta.show(fm!!, "")
                }
            }
        })

        botonView.setOnClickListener {
            botonView.isEnabled=false

            if(email.text.toString() != "" && password.text.toString() != ""){

                val queue = Volley.newRequestQueue(this)
                val url = "https://us-central1-pharmaco-68bb9.cloudfunctions.net/haySesionActiva"

                val jsonObject = JSONObject()
                jsonObject.put("correo", email.text.toString())

                val jsonObjectRequest = JsonObjectRequest(url, jsonObject,
                    Response.Listener { response ->

                        if(response.getString("rpta")=="false") {
                            var prender = {
                                botonView.isEnabled=true
                            }
                            viewModel.signIn(email.text.toString(), password.text.toString(), prender)
                        }
                        else {
                            botonView.isEnabled=true
                            val alerta = GenericAlertDialog("Ya has iniciado sesión en otro dispositivo. Cierra sesión en el otro dispositivo si quieres acceder desde aca.",false, {}, {})
                            val fm = supportFragmentManager
                            alerta.show(fm!!, "")
                        }

                    },
                    Response.ErrorListener {
                        it.printStackTrace()
                    }
                )
                queue.add(jsonObjectRequest)
            }
            else{
                botonView.isEnabled=true
                val alerta = GenericAlertDialog("Por favor ingrese su usuario y contraseña",false, {}, {})
                val fm = supportFragmentManager
                alerta.show(fm!!, "")
            }

        }
            /*auth = FirebaseAuth.getInstance()
            auth.signInWithEmailAndPassword("mateodevia2@hotmail.com", "aaaaaa").addOnCompleteListener(this, OnCompleteListener { task ->
                try {
                    val result = task.result
                    val intent = Intent(this, BottomNavActivity::class.java)
                    ActivityCompat.finishAffinity(this);
                    startActivity(intent)
                    finish()
                } catch(e: RuntimeExecutionException){

                    var partes =  e.message!!.split(": ")
                    var mensaje = partes[1]
                    Log.d("MENSAJE", mensaje)

                    if(partes[1]=="An internal error has occurred. [ 7") {
                        mensaje = "Lo sentimos, debes estar conectado a internet para poder iniciar sesión"
                    }
                    else if(partes[1]=="The email address is badly formatted.") {
                        mensaje = "El correo electrónico no es válido."
                    }
                    else if(partes[1]=="The password is invalid or the user does not have a password." || partes[1]=="There is no user record corresponding to this identifier. The user may have been deleted.") {
                        mensaje = "Creedenciales inválidas. Intentalo de nuevo."
                    }
                    val alerta = GenericAlertDialog(mensaje,false, {}, {})
                    val fm = supportFragmentManager
                    alerta.show(fm!!, "")
                }
            })*/
    }
}