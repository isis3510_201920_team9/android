package com.pharmaco.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import com.google.firebase.auth.FirebaseAuth
import com.pharmaco.R
import com.pharmaco.fragments.GenericAlertDialog
import com.pharmaco.viewModels.usuarios.UsuariosLoginRegisterViewModel

class RegisterActiviy: AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        var email = findViewById<EditText>(R.id.correo)
        var password = findViewById<EditText>(R.id.clave)
        var nombre = findViewById<EditText>(R.id.nombre)
        var epsView = findViewById<EditText>(R.id.eps)
        var cedulaView = findViewById<EditText>(R.id.cedula)
        var botonView = findViewById<Button>(R.id.botonRegistrarse)
        var edad = findViewById<EditText>(R.id.edad)

        val generos = arrayOf("Masculino", "Femenino", "Otro")
        val genero = findViewById<Spinner>(R.id.spinnerGenero)
        var selectedItem: String? = "Masculino"
        if (genero != null) {
            val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, generos)
            genero.adapter = arrayAdapter

            genero.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    selectedItem = generos[position]
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }

        edad.doAfterTextChanged { text ->

            if (text.toString() == "0"){
                edad.setText("")
                val alerta = GenericAlertDialog("Su edad no puede ser 0", false, {}, {})
                alerta.show(supportFragmentManager!!, "")

            }
        }


        val viewModel = ViewModelProviders.of(this)[UsuariosLoginRegisterViewModel::class.java]

        viewModel.loggedInSuccess.observe(this, Observer<Boolean?> {  suc ->

            if(suc != null) {
                if (suc) {
                    val intent = Intent(this, BottomNavActivity::class.java)
                    ActivityCompat.finishAffinity(this)
                    startActivity(intent)
                    finish()
                } else {
                    val alerta = GenericAlertDialog(viewModel.errMsg.value!!, false, {}, {})
                    val fm = supportFragmentManager
                    alerta.show(fm!!, "")
                }
            }
        })

        botonView.setOnClickListener {

            if(email.text.toString() != "" && password.text.toString() != ""
                    && epsView.text.toString() != "" && cedulaView.text.toString() != ""
                    && nombre.text.toString() != "" && edad.text.toString() != "") {

                if(edad.text.toString().toInt() < 18){
                    val alerta = GenericAlertDialog("Debe ser mayor de edad para registrarse",false, {}, {})
                    val fm = supportFragmentManager
                    alerta.show(fm!!, "")

                    return@setOnClickListener
                }

                viewModel.register(email = email.text.toString(), password =password.text.toString(),
                        eps = epsView.text.toString(), cedula =  cedulaView.text.toString(),
                        nombre = nombre.text.toString(), edad = edad.text.toString().toInt(), genero = selectedItem!!)

            }
            else{

                val alerta = GenericAlertDialog("Por favor complete todos los campos",false, {}, {})
                val fm = supportFragmentManager
                alerta.show(fm!!, "")
            }



           /* auth = FirebaseAuth.getInstance()
            auth.createUserWithEmailAndPassword("mateodevia2@hotmail.com", "aaaaaa").addOnCompleteListener(this, OnCompleteListener { task ->
                 try {
                     val result = task.result
                     val intent = Intent(this, BottomNavActivity::class.java)
                     ActivityCompat.finishAffinity(this);
                     startActivity(intent)
                     finish()
                 } catch(e: RuntimeExecutionException){

                     var partes =  e.message!!.split(": ")
                     var mensaje = partes[1]

                     if(partes[1]=="The email address is already in use by another account.") {
                         mensaje = "Ya existe un usuario asociado a ese correo."
                     }
                     else if(partes[1]=="An internal error has occurred. [ 7") {
                         mensaje = "Lo sentimos, debes estar conectado a internet para poder iniciar sesión"
                     }
                     else if(partes[1]=="The email address is badly formatted.") {
                         mensaje = "El correo electrónico no es válido."
                     }
                     val alerta = GenericAlertDialog(mensaje,false, {}, {})
                     val fm = supportFragmentManager
                     alerta.show(fm!!, "")
                }
            })*/
        }
    }
}