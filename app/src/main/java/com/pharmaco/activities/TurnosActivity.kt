package com.pharmaco.activities

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.R
import com.pharmaco.fragments.SwipeLeftToDeleteCallback
import com.pharmaco.fragments.turnos.adapters.TurnosListAdapter
import com.pharmaco.models.Turno
import com.pharmaco.viewModels.turnos.TurnosViewModel
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.pharmaco.broadcastReceivers.ConnectivityReceiver


class TurnosActivity: AppCompatActivity(), ConnectivityReceiver.ConnectivityReceiverListener{
    private var mSnackBar: Snackbar? = null
    private var rootlayout: CoordinatorLayout? = null
    private var abierto = false

    private lateinit var viewModel: TurnosViewModel
    private var con_rec: ConnectivityReceiver? = null


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_eventos_list)

        val tituloView = findViewById<TextView>(R.id.tituloEventosList)
        var listaVacia = findViewById<TextView>(R.id.listaVacia)
        val cerrar = findViewById<ImageView>(R.id.iconoCerrarEventosList)
        val listView = findViewById<RecyclerView>(R.id.listaEventos2)
        rootlayout = findViewById<CoordinatorLayout>(R.id.coordinatorView) as CoordinatorLayout

        tituloView.text = "Próximos Turnos"


        cerrar.setOnClickListener {
            this.finish()
        }

        val swipeLeft = object : SwipeLeftToDeleteCallback(this) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = listView.adapter as TurnosListAdapter
                val index = viewHolder.adapterPosition

                viewModel.deleteTurno(index)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeLeft)
        itemTouchHelper.attachToRecyclerView(listView)

        //ViewModel
        viewModel = ViewModelProviders.of(this)[TurnosViewModel::class.java]

        viewModel.getTurnosProximos().observe(this, Observer<MutableList<Turno>>{ turnos ->

            listView.adapter = TurnosListAdapter(turnos, {pAbierto -> abierto = pAbierto}, {abierto})
            listView.layoutManager = LinearLayoutManager(this)
            if(turnos.isEmpty()){
                listView.visibility = View.GONE
                listaVacia.visibility = View.VISIBLE
            }

        })

        con_rec = ConnectivityReceiver()

        registerReceiver(con_rec,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }
    private fun showMessage(isConnected: Boolean) {
        if (!isConnected) {

            mSnackBar = Snackbar.make(rootlayout as View, R.string.no_internet, Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            mSnackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            mSnackBar?.show()
        } else {
            mSnackBar?.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()

        ConnectivityReceiver.connectivityReceiverListener = this
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(con_rec)
    }

    /**
     * Callback will be called when there is change
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showMessage(isConnected)
    }
}