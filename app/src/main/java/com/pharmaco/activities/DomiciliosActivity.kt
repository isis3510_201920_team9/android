package com.pharmaco.activities

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.R
import com.pharmaco.fragments.SwipeLeftToDeleteCallback
import com.pharmaco.fragments.domicilios.adapters.DomiciliosListAdapter
import com.pharmaco.models.Domicilio
import com.pharmaco.models.MedicamentoOrden
import com.pharmaco.models.MedicamentoTSD
import java.util.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.pharmaco.broadcastReceivers.ConnectivityReceiver
import com.pharmaco.fragments.turnos.adapters.TurnosListAdapter
import com.pharmaco.models.Turno
import com.pharmaco.viewModels.domicilios.DomiciliosViewModel
import com.pharmaco.viewModels.turnos.TurnosViewModel


class DomiciliosActivity: AppCompatActivity(), ConnectivityReceiver.ConnectivityReceiverListener{
    private var mSnackBar: Snackbar? = null
    private var rootlayout: CoordinatorLayout? = null
    private var abierto = false


    private lateinit var viewModel: DomiciliosViewModel
    private var con_rec: ConnectivityReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_eventos_list)

        val tituloView = findViewById<TextView>(R.id.tituloEventosList)
        var listaVacia = findViewById<TextView>(R.id.listaVacia)
        val cerrar = findViewById<ImageView>(R.id.iconoCerrarEventosList)
        val listView = findViewById<RecyclerView>(R.id.listaEventos2)
        rootlayout = findViewById<CoordinatorLayout>(R.id.coordinatorView) as CoordinatorLayout

        tituloView.text = "Próximos Domicilios"

        cerrar.setOnClickListener {
            this.finish()
        }

        val swipeLeft = object : SwipeLeftToDeleteCallback(this) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = listView.adapter as DomiciliosListAdapter
                viewModel.deleteDomicilio(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeLeft)
        itemTouchHelper.attachToRecyclerView(listView)

        //ViewModel
        viewModel= ViewModelProviders.of(this)[DomiciliosViewModel::class.java]

        viewModel.getDomiciliosProximos().observe(this, Observer<MutableList<Domicilio>>{ domicilios ->

            listView.adapter = DomiciliosListAdapter(domicilios, {pAbierto -> abierto = pAbierto}, {abierto})
            listView.layoutManager = LinearLayoutManager(this)
            if(domicilios.isEmpty()){
                listView.visibility = View.GONE
                listaVacia.visibility = View.VISIBLE
            }

        })

        con_rec = ConnectivityReceiver()

        registerReceiver(con_rec,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
    }
    private fun showMessage(isConnected: Boolean) {
        if (!isConnected) {

            mSnackBar = Snackbar.make(rootlayout as View, R.string.no_internet, Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            mSnackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            mSnackBar?.show()
        } else {
            mSnackBar?.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()

        ConnectivityReceiver.connectivityReceiverListener = this
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(con_rec)
    }

    /**
     * Callback will be called when there is change
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showMessage(isConnected)
    }
}