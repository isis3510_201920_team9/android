package com.pharmaco.activities

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.room.Database
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.pharmaco.DataCache
import com.pharmaco.R
import com.pharmaco.repo.farmacias.FarmaciasRepo
import com.pharmaco.rooms.medicamentos.DatabaseCopier
import com.pharmaco.rooms.medicamentos.MedicamentosDBSingleton
import com.pharmaco.viewModels.Routes
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlin.system.exitProcess


class MainActivity: AppCompatActivity()  {
    private lateinit var auth: FirebaseAuth

    var h: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



        FirebaseApp.initializeApp(this)


       /* FirebaseFirestore.getInstance().clearPersistence()
        moveTaskToBack(true);
        exitProcess(-1)
*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel("ForegroundService Kotlin", "Canal1", importance)
            mChannel.description = "El noticiero independiente"
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(FirebaseMessagingService.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
        }


        //Instanciar Base de datos de medicamentos
        //MedicamentosDBSingleton.instantiateDB(applicationContext)
        val db = DatabaseCopier.getInstance(applicationContext).roomDatabase
        MedicamentosDBSingleton.setDB(db)

       /* GlobalScope.launch {
            Log.d("LAUNCHING DB QUERY", "LAUNCH")
            val result = db.medicamentoDao().getAll()
            Log.d("BASE DE DATOS MIGRADA", result.toString())
        }*/


        auth = FirebaseAuth.getInstance()

        if(auth.currentUser == null) {

            setContentView(R.layout.activity_main)

            var botonView = findViewById<Button>(R.id.botonIniciarSesion)
            var textoView = findViewById<TextView>(R.id.textoRegistrarse)
            botonView.setOnClickListener {
                val intent = Intent(this, IniciarSesionActivity({ this.finish() })::class.java)
                startActivity(intent)
            }

            textoView.setOnClickListener {
                val intent = Intent(this, RegisterActiviy::class.java)
                startActivity(intent)
            }
        }
        else {

            Routes.setUserID()
            DataCache.cacheData()


            val intent = Intent(this, BottomNavActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            startActivity(intent)
            finish()
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        Log.d("MAIN ACTIVITY", "FINISH")
    }


}