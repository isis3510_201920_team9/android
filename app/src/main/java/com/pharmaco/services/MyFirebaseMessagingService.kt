package com.pharmaco.services

import android.app.*
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.pharmaco.R
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import com.pharmaco.activities.BottomNavActivity
import com.pharmaco.activities.MainActivity
import com.pharmaco.models.Notificacion
import com.pharmaco.repo.notificaciones.NotificacionesRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*
import kotlin.coroutines.coroutineContext


class MyFirebaseMessagingService: FirebaseMessagingService() {

    private val CHANNEL_ID = "ForegroundService Kotlin"

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(p0.data["title"])
            .setContentText(p0.data["body"])
            .setSmallIcon(R.drawable.ic_logo_blanco)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .build()

        if(p0.data["tipo"]=="turno"){
            notificacionTurno(p0)
        }
        else if(p0.data["tipo"]=="domicilio"){
            notificacionDomicilio(p0)
        }
        else if(p0.data["tipo"]=="suscripcion"){
            notificacionSuscripcion(p0)
        }

        if (!appInForeground()){
            val intent = Intent(this, MainActivity::class.java)
            val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
            notification!!.contentIntent = pendingIntent
        }
        val manager = NotificationManagerCompat.from(applicationContext)
        manager.notify(123, notification!!)
    }

    fun notificacionTurno(p0: RemoteMessage){
        val mensajeLargo = "Acabas de recoger los medicamentos del turno que habías agendado en la farmacia "+p0.data["farmacia"]+".\n"+
                "Debiste recibir "+p0.data["medicamentos"]+" medicamentos."
        GlobalScope.launch { NotificacionesRepo.addNotificacion(Notificacion(p0.data["title"], Date(), "turno", p0.data["body"], mensajeLargo)) }
    }

    fun notificacionDomicilio(p0: RemoteMessage){
        val mensajeLargo = "Te acaban de entregar el domicilio que habias pedido a la farmacia "+p0.data["farmacia"]+".\n"+
                "Debiste recibir "+p0.data["medicamentos"]+" medicamentos."
        GlobalScope.launch { NotificacionesRepo.addNotificacion(Notificacion(p0.data["title"], Date(), "domicilio", p0.data["body"], mensajeLargo)) }

    }

    fun notificacionSuscripcion(p0: RemoteMessage){
        val mensajeLargo = "Acabas de recoger los medicamentos que recoges en la farmacia "+p0.data["farmacia"]+".\n"+
                "Debiste recibir "+p0.data["medicamentos"]+" medicamentos."
        GlobalScope.launch { NotificacionesRepo.addNotificacion(Notificacion(p0.data["title"], Date(), "suscripcion", p0.data["body"], mensajeLargo)) }

    }

    fun appInForeground(): Boolean {
        val activityManager = this.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val runningAppProcesses = activityManager.runningAppProcesses ?: return false
        return runningAppProcesses.any { it.processName == this.packageName && it.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND }
    }

    private fun isAppRunning(): Boolean {
        val m = this.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val runningTaskInfoList = m.getRunningTasks(10)
        val itr = runningTaskInfoList.iterator()
        var n = 0
        while (itr.hasNext()) {
            n++
            itr.next()
        }
        return if (n == 1) {
            false
        } else true
    }

    override fun onNewToken(token: String) {
        Log.d("FIREBASE EVENT", "Refreshed token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token)
    }

    private fun sendRegistrationToServer(token: String?) {

        // TODO: Implement this method to send token to your app server.
        Log.d("FIREBASE EVENT", "sendRegistrationTokenToServer($token)")

    }
}