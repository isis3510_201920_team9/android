package com.pharmaco.fragments.suscripciones

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import android.app.TimePickerDialog
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.util.Log
import android.widget.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.pharmaco.R
import com.pharmaco.broadcastReceivers.ConnectivityReceiver
import com.pharmaco.fragments.GenericAlertDialog
import com.pharmaco.fragments.medicamentos.adapters.MedicamentosPedidosListAdapter
import com.pharmaco.models.*
import com.pharmaco.viewModels.suscripciones.SuscripcionEditViewModel
import java.text.SimpleDateFormat
import com.pharmaco.models.Suscripcion
import java.util.*

class EditSuscripcionDialogFragment(var suscripcion: Suscripcion): DialogFragment(), ConnectivityReceiver.ConnectivityReceiverListener {

    private lateinit var medicamentosList: MutableList<MedicamentoCreate>
    private lateinit var viewModel: SuscripcionEditViewModel
    private var mSnackBar: Snackbar? = null
    private var rootlayout: CoordinatorLayout? = null
    private lateinit var con_rec: ConnectivityReceiver
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
    }
    var diaSemanaEscogido = ""


    val diasSemanaMap = mutableMapOf<String, Int>(
        "D" to 0,
        "L" to 1,
        "M" to 2,
        "X" to 3,
        "J" to 4,
        "V" to 5,
        "S" to 6
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.dialog_fragment_edit_suscripcion, container, false)

        val titulo: TextView = view.findViewById<TextView>(R.id.descripcionPedirSuscripcion)
        val ubicacion = view.findViewById<EditText>(R.id.ubicacionPedirSuscripcion)
        val horaText = view.findViewById<EditText>(R.id.horaPedirSuscripcion)
        val cerrar = view.findViewById<ImageView>(R.id.iconoCerrarPedirSuscripcion)
        val agregar = view.findViewById<Button>(R.id.agregar)
        val spinner: Spinner = view.findViewById<Spinner>(R.id.spinner)
        val lunes: RadioButton= view.findViewById<RadioButton>(R.id.lunesCheck)
        val martes: RadioButton= view.findViewById<RadioButton>(R.id.martesCheck)
        val miercoles: RadioButton= view.findViewById<RadioButton>(R.id.miercolesCheck)
        val jueves: RadioButton= view.findViewById<RadioButton>(R.id.juevesCheck)
        val viernes: RadioButton= view.findViewById<RadioButton>(R.id.viernesCheck)
        val sabado: RadioButton= view.findViewById<RadioButton>(R.id.sabadoCheck)
        val domingo: RadioButton= view.findViewById<RadioButton>(R.id.domingoCheck)
        val radioGroup: RadioGroup = view.findViewById<RadioGroup>(R.id.radioGroup)
        val fechaFin = view.findViewById<EditText>(R.id.fechaFin)
        rootlayout = view.findViewById<CoordinatorLayout>(R.id.coordinatorView) as CoordinatorLayout
        val enviar = view.findViewById<Button>(R.id.enviar)

        titulo.text = "Editar Suscripcion"
        var fechaEscogida = fechaFin.text.toString()


        //val args: Bundle? = getArguments()
        //val suscripcion = args?.getSerializable("suscripcion")!! as Suscripcion



        var fech = suscripcion.fechaFin
        fechaEscogida = fech.date.toString()+"/"+(fech.month+1).toString()+"/"+(fech.year+1900).toString()
        fechaFin.setText(fechaEscogida)

        var listView: RecyclerView = view.findViewById<RecyclerView>(R.id.listPedirSuscripcion)

        //Log.d(this::class.java.toString(), "SUSCRIPCION CARGADA: ${suscripcion}")

        ubicacion.setText(suscripcion.farmacia.nombre)

        horaText.setText(suscripcion.hora)
        var horaEscogida = horaText.text.toString()


        diaSemanaEscogido= suscripcion.diaSemana

        if(diaSemanaEscogido.toLowerCase() =="lunes"){
            lunes.isChecked = true
        }
        else if(diaSemanaEscogido.toLowerCase() =="martes"){
            martes.isChecked = true
        }
        else if(diaSemanaEscogido.toLowerCase() =="miércoles"){
            miercoles.isChecked = true
        }
        else if(diaSemanaEscogido.toLowerCase() =="jueves"){
            jueves.isChecked = true
        }
        else if(diaSemanaEscogido.toLowerCase() =="viernes"){
            viernes.isChecked = true
        }
        else if(diaSemanaEscogido.toLowerCase() =="sábados"){
            sabado.isChecked = true
        }
        else if(diaSemanaEscogido.toLowerCase() =="domingos"){
            domingo.isChecked = true
        }

        diaSemanaEscogido = view.findViewById<RadioButton>(radioGroup.checkedRadioButtonId).text.toString()

        radioGroup.setOnCheckedChangeListener { radioGroup, i ->

            val radioButton = view.findViewById<RadioButton>(i)
            diaSemanaEscogido = radioButton.text.toString()
        }

        val pesos = arrayOf("1", "2", "3", "4", "5", "6", "7", "8")
        var selectedItem: String? = suscripcion.frecuencia.toString()
        var frecueciaEscogida = suscripcion.frecuencia.toString()

        if (spinner != null) {
            val arrayAdapter = ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_item, pesos)
            spinner.adapter = arrayAdapter

            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    selectedItem = pesos[position]
                    frecueciaEscogida = pesos[position]
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }

            spinner.setEnabled(false);
            spinner.setClickable(false);
        }


        if (spinner != null) {
            val arrayAdapter = ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_item, pesos)
            spinner.adapter = arrayAdapter
            if(selectedItem=="1"){
                spinner.setSelection(0)
            }
            else if(selectedItem=="2"){
                spinner.setSelection(1)
            }
            else if(selectedItem=="3"){
                spinner.setSelection(2)
            }
            else if(selectedItem=="4"){
                spinner.setSelection(3)
            }
            else if(selectedItem=="5"){
                spinner.setSelection(4)
            }
            else if(selectedItem=="6"){
                spinner.setSelection(5)
            }
            else if(selectedItem=="7"){
                spinner.setSelection(6)
            }
            else if(selectedItem=="8"){
                spinner.setSelection(7)
            }


            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    selectedItem = pesos[position]
                    frecueciaEscogida = pesos[position]
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }


        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)

        val dpd = DatePickerDialog(this.requireContext(), DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // Display Selected date in Toast
            fechaFin.setText(dayOfMonth.toString()+"/"+(monthOfYear+1).toString()+"/"+year.toString())
            fechaEscogida = dayOfMonth.toString()+"/"+(monthOfYear+1).toString()+"/"+year.toString()

        }, year, month, day)

        dpd.datePicker.minDate = System.currentTimeMillis();

        val tpd = TimePickerDialog(this.requireContext(), TimePickerDialog.OnTimeSetListener { view, hour, minute ->
            var date = Date()
            date.hours = hour
            date.minutes = minute
            var formatter = SimpleDateFormat("h:mm aa")
            horaText.setText(formatter.format(date.time))
            horaEscogida = formatter.format(date.time)

        }, hour, minute, false)

        /*ubicacion.setOnClickListener {
            openMap()
        }*/

        horaText.setOnClickListener {
            tpd.show()
        }

        /**
        ejemplo.setOnClickListener {
            val navController: NavController = NavHostFragment.findNavController(this)
            navController.navigate(R.id.action_pedirSuscripcionDialogFragment_to_datePickerFragment)
        }*/

        cerrar.setOnClickListener {
            //como accedo al dialogo anterior desde aca para cerrarlo
            dismiss()
        }

        fechaFin.setOnClickListener {
            dpd.datePicker.minDate = calcularFechaMinFin(frecueciaEscogida.toInt()).time
            dpd.show()
        }

        con_rec = ConnectivityReceiver()

        this.activity!!.registerReceiver(con_rec,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )


        fun editSuscripcion(){

            val fechaInicio = getNextDay(diasSemanaMap[diaSemanaEscogido]!!)

            val horaEscogida = horaText.text.toString()
            val horaArr = horaEscogida.split(":")

            fechaInicio.hours = horaArr[0].toInt()
            fechaInicio.minutes = horaArr[1].split(" ")[0].toInt()

            if(horaArr[1].split(" ")[1] == "PM"){
                fechaInicio.hours = fechaInicio.hours + 12
            }

            val frecuencia = frecueciaEscogida.toInt()

            //TODO, sacarse la fecha fin de ese TDP
            val dateArr = fechaEscogida.split("/")
            Log.d("fechaEscogida", fechaEscogida)
            val fechaFin = Date()

            fechaFin.date = dateArr[0].toInt()
            fechaFin.month = dateArr[1].toInt()-1
            fechaFin.year = dateArr[2].toInt() - 1900

            viewModel.editSuscripcion(fechaModificacion = fechaInicio, frecuencia = frecuencia,
                fechaFin = fechaFin)

            dismiss()
        }

        enviar.setOnClickListener{
            //ACA PEZ

            if(ubicacion.text.toString() == "" || horaEscogida == ""){
                val alerta = GenericAlertDialog("Debe completar todos los campos para actualizar su suscripcion", false, {}, {})
                val fm = fragmentManager
                alerta.show(fm!!, "")
                return@setOnClickListener
            }

            //Revisar si hay o no internet
            if(!viewModel.verifyAvailableNetwork(this.activity!!)){

                //si no hay internet, generar alerta.
                val alerta = GenericAlertDialog("No hay internet en este momento. " +
                        "Intentaremos sincronizar tus datos cuando recuperemos la conexión. ¿Deseas continuar?"
                    , true, {}){
                    editSuscripcion()
                }
                val fm = fragmentManager
                alerta.show(fm!!, "")
                return@setOnClickListener
            }
            else{
                editSuscripcion()
            }


        }

        //ViewModel
        viewModel = ViewModelProviders.of(this)[SuscripcionEditViewModel::class.java]

        if(viewModel.suscripcion == null) {
            viewModel.suscripcion = suscripcion
        }

        //Se pierde info en conversión a medicamentoTSD create, en este caso no importa porque los medicamentos
        // no son editables.
        listView.adapter = MedicamentosPedidosListAdapter(viewModel.suscripcion!!.medicamentos)
        listView.layoutManager = LinearLayoutManager(this.requireContext())

        return view
    }


    //0 domingo, 1 lunes, 2 martes, 3 miércoles, 4 jueves, 5 viernes, 6 sábado.
    //Función adaptada de https://coderanch.com/t/385117/java/date-Monday
    fun getNextDay(day: Int): Date{

        val now: Calendar = Calendar.getInstance()
        val weekday: Int = Date().day

        val days: Int  = ((day - weekday) % 7)
        now.add(Calendar.DATE, days)

        return now.time

    }

    //TODO, pez esta función devuelve fecha mínima de fin suponiendo que se tiene frecuencia y día de la semana escogido
    fun calcularFechaMinFin(frecuencia: Int): Date{

        val fechaInicio = getNextDay(diasSemanaMap[diaSemanaEscogido]!!)

        return addDays(fechaInicio, 7*frecuencia)

    }

    //Esta función fue tomada de https://www.journaldev.com/700/java-date-add-days-subtract-days-calendar
    fun addDays(date: Date, days: Int): Date {
        val cal = GregorianCalendar()
        cal.time = date
        cal.add(Calendar.DATE, days)

        return cal.time
    }

    override fun onStart() {
        super.onStart()

        var dialog:  Dialog = requireDialog()

        if(dialog != null){

            var width = ViewGroup.LayoutParams.MATCH_PARENT
            var height = ViewGroup.LayoutParams.MATCH_PARENT

            dialog.window!!.setLayout(width, height)
            dialog.window!!.setWindowAnimations(R.style.AppTheme_Slide)
        }
    }


    /*fun openMap() {
        //El Dialogo del detalle
        val fm = fragmentManager
        val medicamentosList = MapFragment(viewModel)
        medicamentosList.show(fm!!, "")
    }*/

    private fun showMessage(isConnected: Boolean) {
        if (!isConnected) {

            mSnackBar = Snackbar.make(rootlayout as View, R.string.no_internet, Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            mSnackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            mSnackBar?.show()
        } else {
            mSnackBar?.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()

        ConnectivityReceiver.connectivityReceiverListener = this
    }

    /**
     * Callback will be called when there is change
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showMessage(isConnected)
    }

    override fun onDestroy() {
        super.onDestroy()
        this.activity!!.unregisterReceiver(con_rec)
    }
}