package com.pharmaco.fragments.suscripciones.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.pharmaco.R
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.fragments.suscripciones.SuscripcionDetailDialogFragment
import com.pharmaco.models.Suscripcion
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.pharmaco.fragments.ordenes.OrdenesFragmentDirections
import com.pharmaco.fragments.suscripciones.EditSuscripcionDialogFragment
import com.pharmaco.viewModels.domicilios.DomicilioDetailViewModel
import com.pharmaco.viewModels.suscripciones.SuscripcionDetailViewModel
import com.pharmaco.viewModels.suscripciones.SuscripcionesViewModel
import com.pharmaco.viewmodelfactories.domicilio.DomicilioDetailViewModelFactory
import com.pharmaco.viewmodelfactories.suscripcion.SuscripcionDetailViewModelFactory

class SuscripcionesListAdapter (private val items: MutableList<Suscripcion>, var setAbierto: (Boolean) -> Unit, val getAbierto: () -> Boolean): RecyclerView.Adapter<SuscripcionesListAdapter.SuscripcionesListVH>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SuscripcionesListVH {

        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.fragment_list_description_row, parent, false)

        return SuscripcionesListVH(inflatedView, parent.context)
    }

    override fun onBindViewHolder(holder: SuscripcionesListVH, position: Int) {

        holder.bind(items[position], setAbierto, getAbierto)
    }

    override fun getItemCount(): Int = items.size

    fun addItem(suscripcion: Suscripcion) {

        items.add(suscripcion)
        notifyItemInserted(items.size)
    }

    class SuscripcionesListVH(v: View, ctx: Context) : RecyclerView.ViewHolder(v), View.OnClickListener {
        var view: View = v
        var context: Context = ctx
        var suscripcion: Suscripcion? = null

        init {
            v.setOnClickListener {
            }
        }

        override fun onClick(v: View) {
        }

        companion object {
            //5
            private val ORDEN_KEY = "ORDEN"
        }
        fun bind(suscripcion: Suscripcion, setAbierto: (Boolean) -> Unit, getAbierto: () -> Boolean) {

            var titulo: TextView = view.findViewById(R.id.titulo)
            var descripcion: TextView = view.findViewById(R.id.descripcion)
            var hora: TextView = view.findViewById(R.id.fecha)
            var icono: ImageView = view.findViewById(R.id.icono)

            titulo.text = suscripcion.farmacia.nombre

            var semana = "semana"
            var frecuencia = ""
            if(suscripcion.frecuencia > 1){
                semana = "semanas"
                frecuencia = suscripcion.frecuencia.toString() + " "
            }

            descripcion.text = "Los " + suscripcion.diaSemana.toLowerCase() + " cada " + frecuencia  + semana
            hora.text = suscripcion.hora

            var drawable:Drawable? = null;
            drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_suscripcion_24dp)!!
            val wrappedDrawable = DrawableCompat.wrap(drawable)
            icono.setImageDrawable(drawable);

            view.setOnClickListener {
                if(!getAbierto()) {
                    setAbierto(true)
                    val fm = (view.context as AppCompatActivity).supportFragmentManager
                    val suscripcionesDetail = SuscripcionDetailDialogFragment(setAbierto, suscripcion)
                    suscripcionesDetail.show(fm, "")
                }
            }

            view.setOnLongClickListener {
                val fm = (view.context as AppCompatActivity).supportFragmentManager
                val editSuscripcion = EditSuscripcionDialogFragment(suscripcion)
                editSuscripcion.show(fm, "")
                true
            }
        }
    }
}

