package com.pharmaco.fragments.suscripciones

import android.app.*
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.media.RingtoneManager
import android.net.ConnectivityManager
import android.util.Log
import android.widget.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.pharmaco.R
import com.pharmaco.broadcastReceivers.ConnectivityReceiver
import com.pharmaco.broadcastReceivers.NotificationPublisher
import com.pharmaco.fragments.GenericAlertDialog
import com.pharmaco.fragments.SwipeLeftToDeleteCallback
import com.pharmaco.fragments.medicamentos.adapters.MedicamentosAPedirListAdapter
import com.pharmaco.fragments.MapFragment
import com.pharmaco.fragments.ordenes.OrdenesModalFragment
import com.pharmaco.models.*
import com.pharmaco.viewModels.suscripciones.SuscripcionCreateViewModel
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar

@Suppress("DEPRECATION")
class PedirSuscripcionDialogFragment(var setAbierto: () -> Unit, var orden: Orden?, var closeParent: () -> Unit): DialogFragment(), ConnectivityReceiver.ConnectivityReceiverListener{
    private  val LOCATION_PERMISSION_REQUEST_CODE = 1
    private var mSnackBar: Snackbar? = null
    private var rootlayout: CoordinatorLayout? = null
    private lateinit var farmaciasList: MutableList<FarmaciaTSD>
    private lateinit var medicamentosList: MutableList<MedicamentoCreate>
    private lateinit var viewModel: SuscripcionCreateViewModel
    private var mapOpened: Boolean = false
    private lateinit var con_rec: ConnectivityReceiver

    var diaSemanaEscogido = "L"

    val diasSemanaMap = mutableMapOf<String, Int>(
        "D" to 0,
        "L" to 1,
        "M" to 2,
        "X" to 3,
        "J" to 4,
        "V" to 5,
        "S" to 6
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.dialog_fragment_pedir_suscripcion, container, false)

        val titulo = view.findViewById<TextView>(R.id.descripcionPedirSuscripcion)
        val ubicacion = view.findViewById<EditText>(R.id.ubicacionPedirSuscripcion)
        val horaText = view.findViewById<EditText>(R.id.horaPedirSuscripcion)
        val cerrar = view.findViewById<ImageView>(R.id.iconoCerrarPedirSuscripcion)
        val agregar = view.findViewById<Button>(R.id.agregar)
        val spinner: Spinner = view.findViewById<Spinner>(R.id.spinner)
        val radioGroup: RadioGroup = view.findViewById<RadioGroup>(R.id.radioGroup)
        val enviar = view.findViewById<Button>(R.id.enviar)
        val fechaFin = view.findViewById<EditText>(R.id.fechaFin)
        rootlayout = view.findViewById<CoordinatorLayout>(R.id.coordinatorView) as CoordinatorLayout
        //val ejemplo = view.findViewById<EditText>(R.id.ejemplo)

        var fechaEscogida = fechaFin.text.toString()
        var horaEscogida = horaText.text.toString()
        var frecueciaEscogida = ""

        titulo.text = "Suscribirse"
        ubicacion.setText("Escoger Farmacia")
        //ejemplo.setText("Ejemplo Jetpack")

        var listView: RecyclerView = view.findViewById<RecyclerView>(R.id.listPedirSuscripcion)

        //val args: Bundle? = getArguments()
        //val orden = args?.getSerializable("orden") as Orden?

        //listView.adapter = MedicamentosAPedirListAdapter(list);



        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)


        val pesos = arrayOf("1", "2", "3", "4", "5", "6", "7", "8")
        var selectedItem: String? = ""
        if (spinner != null) {
            val arrayAdapter = ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_item, pesos)
            spinner.adapter = arrayAdapter

            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    selectedItem = pesos[position]
                    frecueciaEscogida = pesos[position]
                    fechaFin.setText("Ingresar Fecha de Finalización")
                    fechaEscogida = fechaFin.text.toString()
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }

        radioGroup.setOnCheckedChangeListener { radioGroup, i ->

            val radioButton = view.findViewById<RadioButton>(i)
            diaSemanaEscogido = radioButton.text.toString()
            fechaFin.setText("Ingresar Fecha de Finalización")
            fechaEscogida = fechaFin.text.toString()
        }

        val tpd = TimePickerDialog(this.requireContext(), TimePickerDialog.OnTimeSetListener { view, hour, minute ->
            var date = Date()
            date.hours = hour
            date.minutes = minute
            var formatter = SimpleDateFormat("h:mm aa")
            horaText.setText(formatter.format(date.time))
            horaEscogida = formatter.format(date.time)


        }, hour, minute, false)

        ubicacion.setOnClickListener {
            openMap()
        }

        fechaFin.setOnClickListener {

            val dpd = DatePickerDialog(this.requireContext(), DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in Toast
                fechaFin.setText(dayOfMonth.toString()+"/"+(monthOfYear+1).toString()+"/"+year.toString())
                fechaEscogida = dayOfMonth.toString()+"/"+(monthOfYear+1).toString()+"/"+year.toString()

            }, year, month, day)

            val fechaMin =  calcularFechaMinFin(frecueciaEscogida.toInt())
            dpd.datePicker.minDate = fechaMin.time
            dpd.show()
        }

        horaText.setOnClickListener {
            tpd.show()
        }

        agregar.setOnClickListener {
            openMedicamentosList()
        }

        val swipeLeft = object : SwipeLeftToDeleteCallback(this.requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                viewModel.deleteMedicamentoSeleccionado(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeLeft)
        itemTouchHelper.attachToRecyclerView(listView)

        cerrar.setOnClickListener {
            var controller = findNavController()
            controller.popBackStack(R.id.ordenDetailDialogFragment, true)
            dismiss()
        }

        fun createSuscripcion(){

            val fechaInicio = getNextDay(diasSemanaMap[diaSemanaEscogido]!!)

            val horaArr = horaEscogida.split(":")

            fechaInicio.hours = horaArr[0].toInt()
            fechaInicio.minutes = horaArr[1].split(" ")[0].toInt()

            if(horaArr[1].split(" ")[1] == "PM"){
                fechaInicio.hours = fechaInicio.hours + 12
            }

            val frecuencia = frecueciaEscogida.toInt()

            //TODO, sacarse la fecha fin de ese TDP
            val dateArr = fechaEscogida.split("/")

            val fechaFin = Date()

            fechaFin.date = dateArr[0].toInt()
            fechaFin.month = dateArr[1].toInt()-1
            fechaFin.year = dateArr[2].toInt() - 1900

            Log.d("FECHA FIN", fechaFin.toString())

            viewModel.createSuscripcion(fechaInicio = fechaInicio ,  frecuencia = frecuencia,
                fechaFin = fechaFin)

            var formatter  = SimpleDateFormat("hh:mm aa")
            var mensajeLargo = "Recuerda que hoy tienes una suscripción agendado en la farmacia " + ubicacion.text + " a las " + formatter.format(fechaInicio) + " para recoger " + viewModel.getMedicamentosSeleccionadosList()!!.size + " medicamentos."
            scheduleNotification(c, mensajeLargo)

            closeParent()
            dismiss()
            createNotificationNow(ubicacion.text.toString(), "Creaste una suscripción en la farmacia " + ubicacion.text + " a las " + formatter.format(fechaInicio) + " para recoger " + viewModel.getMedicamentosSeleccionadosList()!!.size + " medicamentos.")

        }

        enviar.setOnClickListener{
            //ACA PEZ

            if(ubicacion.text.toString() == "Escoger Farmacia" || horaEscogida == ""
                    || frecueciaEscogida == "" || fechaEscogida == ""
                    || fechaEscogida == "Ingresar Fecha de Finalización"
                    || viewModel.getMedicamentosSeleccionadosList() == null
                    || viewModel.getMedicamentosSeleccionadosList()!!.size == 0  ){
                val alerta = GenericAlertDialog("Debe completar todos los campos para poder suscribirse", false, {}, {})
                val fm = fragmentManager
                alerta.show(fm!!, "")
                return@setOnClickListener
            }

            val list = viewModel.getMedicamentosSeleccionadosList()
            if(list != null) {
                for (medSelect in list) {

                    if(medSelect.unidadesPedidas == 0){
                        val alerta = GenericAlertDialog("Por favor especifique la cantidad de cada medicamento", false, {}, {})
                        alerta.show(fragmentManager!!, "")
                        return@setOnClickListener
                    }

                }
            }

            //Revisar si hay o no internet
            if(!viewModel.verifyAvailableNetwork(this.activity!!)){

                //si no hay internet, generar alerta.
                val alerta = GenericAlertDialog("No hay internet en este momento. " +
                        "Intentaremos sincronizar tus datos cuando recuperemos la conexión. ¿Deseas continuar?"
                    , true, {}){
                    createSuscripcion()
                }
                val fm = fragmentManager
                alerta.show(fm!!, "")
                return@setOnClickListener
            }
            else{
                createSuscripcion()
            }

        }

        con_rec = ConnectivityReceiver()

        this.activity!!.registerReceiver(con_rec,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)

        )

        //ViewModel
        viewModel = ViewModelProviders.of(this)[SuscripcionCreateViewModel::class.java]

        if(viewModel.orden == null){

            if(orden != null){

                val auxList = mutableListOf<MedicamentoTSDCreate>()

                for(frec in orden!!.frecuencias){
                    auxList.add(frec.toMedicamentoTSDCreate(orden!!.reference!!))
                }

                viewModel.setMedicamentosSeleccionados(auxList)
                viewModel.orden = orden

                listView.adapter = MedicamentosAPedirListAdapter(viewModel, auxList, fragmentManager!!)
                listView.layoutManager = LinearLayoutManager(this.requireContext())

            }
        }

        viewModel.getMedicamentos().observe(this, Observer<MutableList<MedicamentoCreate>>{ medicamentos ->

            medicamentosList = medicamentos

            if(!agregar.isEnabled && !medicamentosList.isEmpty()){
                agregar.isEnabled = true
            }

        })

        viewModel.getMedicamentosSeleccionados().observe(this, Observer<MutableList<MedicamentoTSDCreate>> { medicamentos ->


            Log.d("PEDIR TURNO FRAG", medicamentos.toString())
            listView.adapter = MedicamentosAPedirListAdapter(viewModel, medicamentos, fragmentManager!!)
            listView.layoutManager = LinearLayoutManager(this.requireContext())

        })

        viewModel.getFarmaciasDisponibles().observe(this, Observer<MutableList<FarmaciaTSD>> { farmacias ->

            farmaciasList = farmacias

            if(!ubicacion.isEnabled && !farmaciasList.isEmpty()){
                ubicacion.isEnabled = true
            }
        })

        viewModel.selectedFarmacia.observe(this, Observer<FarmaciaTSD>{ pfarmacia ->
            ubicacion.setText(pfarmacia.nombre)

        })

        return view
    }

    //Esta función fue tomada de https://www.journaldev.com/700/java-date-add-days-subtract-days-calendar
    fun addDays(date: Date, days: Int): Date {
        val cal = GregorianCalendar()
        cal.time = date
        cal.add(Calendar.DATE, days)

        return cal.time
    }

    //TODO, pez esta función devuelve fecha mínima de fin suponien
    // do que se tiene frecuencia y día de la semana escogido
    fun calcularFechaMinFin(frecuencia: Int): Date{

        val fechaInicio = getNextDay(diasSemanaMap[diaSemanaEscogido]!!)

        val fechaMin = addDays(fechaInicio, 7*frecuencia)

        return fechaMin

    }

    //0 domingo, 1 lunes, 2 martes, 3 miércoles, 4 jueves, 5 viernes, 6 sábado.
    //Función adaptada de https://coderanch.com/t/385117/java/date-Monday
    fun getNextDay(day: Int): Date{

        val now: Calendar = Calendar.getInstance()
        val weekday: Int = Date().day


        var days: Int  = ((day - weekday) % 7)// + 7

        if(day <= weekday){
            days += 7
        }

        now.add(Calendar.DATE, days)

        return now.time

    }


    override fun onStart() {
        super.onStart()

        var dialog:  Dialog = requireDialog()

        if(dialog != null){

            var width = ViewGroup.LayoutParams.MATCH_PARENT
            var height = ViewGroup.LayoutParams.MATCH_PARENT

            dialog.window!!.setLayout(width, height)
            dialog.window!!.setWindowAnimations(R.style.AppTheme_Slide)
        }
    }
    fun openMedicamentosList() {
        //El Dialogo del detalle
        val fm = fragmentManager

        val medicamentosList = OrdenesModalFragment(medicamentosList, viewModel)
        medicamentosList.show(fm!!, "")
    }
    fun openMap() {

        if(!mapOpened) {

            if (ActivityCompat.checkSelfPermission(
                    this.requireContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    LOCATION_PERMISSION_REQUEST_CODE
                )
                return
            }

            val fm = fragmentManager

            if (::farmaciasList.isInitialized) {
                val fm = fragmentManager
                val medicamentosList = MapFragment(farmaciasList, { farmacia ->
                    viewModel.setSelected(farmacia)
                }, {
                    mapOpened = false
                })
                medicamentosList.show(fm!!, "")
                mapOpened = true
            }
            else {
                val alerta = GenericAlertDialog("Porfavor espera un momento mientras obtenemos las farmacias",false, {}, {})
                val fm = fragmentManager
                alerta.show(fm!!, "")
            }
        }
    }

    private fun showMessage(isConnected: Boolean) {
        if (!isConnected) {

            mSnackBar = Snackbar.make(rootlayout as View, R.string.no_internet, Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            mSnackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            mSnackBar?.show()
        } else {
            mSnackBar?.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()

        ConnectivityReceiver.connectivityReceiverListener = this
    }

    /**
     * Callback will be called when there is change
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showMessage(isConnected)
    }

    override fun onDestroy() {
        super.onDestroy()
        this.activity!!.unregisterReceiver(con_rec)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        setAbierto()
    }

    private fun scheduleNotification(fechaEvento: Calendar, mensajeLargo: String) {

        val notificationIntent = Intent(requireContext(), NotificationPublisher::class.java)

        val notification = NotificationCompat.Builder(requireContext(), "ForegroundService Kotlin")
            .setContentTitle("Recordatorio")
            .setContentText("Recuerda que hoy tienes una suscripción agendada")
            .setSmallIcon(R.drawable.ic_logo_blanco)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .build()
        notificationIntent.putExtra("titulo", "Recordatorio")
        notificationIntent.putExtra("tipo", "suscripcion")
        notificationIntent.putExtra("mensajeLargo", mensajeLargo)
        notificationIntent.putExtra("mensaje", "Recuerda que hoy tienes una suscripción agendada.")
        notificationIntent.putExtra("notification-id", 1)
        notificationIntent.putExtra("notification", notification)

        val pendingIntent = PendingIntent.getBroadcast(requireContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val alarmManager = context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
        alarmManager!!.set(AlarmManager.RTC_WAKEUP, fechaEvento.timeInMillis-3600000, pendingIntent)
    }

    private fun createNotificationNow(farmacia: String, mensajeLargo: String) {

        val notificationIntent = Intent(requireContext(), NotificationPublisher::class.java)

        val notification = NotificationCompat.Builder(requireContext(), "ForegroundService Kotlin")
            .setContentTitle("Suscripción Creada")
            .setContentText("Creaste una suscripción en la farmacia "+farmacia)
            .setSmallIcon(R.drawable.ic_logo_blanco)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .build()
        notificationIntent.putExtra("titulo", "Suscripción Creada")
        notificationIntent.putExtra("tipo", "suscripcion")
        notificationIntent.putExtra("mensajeLargo", mensajeLargo)
        notificationIntent.putExtra("mensaje", "Creaste una suscripción en la farmacia "+farmacia)
        notificationIntent.putExtra("notification-id", 1)
        notificationIntent.putExtra("notification", notification)

        val pendingIntent = PendingIntent.getBroadcast(requireContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val alarmManager = context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
        alarmManager!!.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+1000, pendingIntent)

    }
}