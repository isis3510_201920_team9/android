package com.pharmaco.fragments.suscripciones

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.R
import com.pharmaco.fragments.medicamentos.adapters.MedicamentosPedidosListAdapter
import com.pharmaco.models.Suscripcion
import androidx.lifecycle.Observer
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.iid.FirebaseInstanceId
import com.pharmaco.viewModels.suscripciones.SuscripcionDetailViewModel
import com.pharmaco.viewmodelfactories.suscripcion.SuscripcionDetailViewModelFactory
import net.glxn.qrgen.android.QRCode


class SuscripcionDetailDialogFragment(var setAbierto: (Boolean) -> Unit, var suscripcion: Suscripcion): AppCompatDialogFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val inflater: LayoutInflater = LayoutInflater.from(this.requireContext())
        var builder: AlertDialog.Builder = AlertDialog.Builder(this.requireContext())
        val view: View = inflater.inflate(R.layout.dialog_fragment_suscripcion_detail, null)
        builder.setView(view).setTitle("Suscripción").setNegativeButton("Aceptar", {dialog, which->

        })

        val farmacia: TextView = view.findViewById<TextView>(R.id.farmacia)
        val dia: TextView = view.findViewById<TextView>(R.id.dia)
        val hora: TextView = view.findViewById<TextView>(R.id.hora)
        val frecuencia: TextView = view.findViewById<TextView>(R.id.frecuencia)
        val recordatorio: TextView = view.findViewById<TextView>(R.id.recordatorio)
        var listView = view.findViewById<RecyclerView>(R.id.listaDetailTurno)
        val QR = view.findViewById<ImageView>(R.id.QR)

        //val args: Bundle? = getArguments()
        //val suscripcion = args?.getSerializable("suscripcion")!! as Suscripcion

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("FIREBASE ERROR", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                val token = task.result?.token

                var url = "https://us-central1-pharmaco-68bb9.cloudfunctions.net/qrSuscripcion?suscripcion="+suscripcion.id+"&email="+FirebaseAuth.getInstance().currentUser!!.email+"&token="+token
                val bitmap = QRCode.from(url).withSize(1000, 1000).bitmap()
                QR.setImageBitmap(bitmap)
            })

        //listView.layoutManager = LinearLayoutManager(this.requireContext())

        recordatorio.text = "No olvides que para que te entreguen tus medicamentos debes presentar el siguiente QR en la farmacia"

        //ViewModel
        val vm = ViewModelProviders.of(this,
            SuscripcionDetailViewModelFactory(suscripcion))[SuscripcionDetailViewModel::class.java]

        vm.getDiaSemana().observe(this, Observer<String>{ diaSemana ->

            dia.text = diaSemana

            listView.layoutManager = LinearLayoutManager(this.requireContext())

        })

        vm.getSuscripcion().observe(this, Observer<Suscripcion>{ suscripcion ->


            farmacia.text = suscripcion.farmacia.nombre
            var list = suscripcion.medicamentos

            listView.adapter = MedicamentosPedidosListAdapter(list)
            listView.layoutManager = LinearLayoutManager(this.requireContext())

        })

        vm.getHora().observe(this, Observer<String>{ nHora ->
            hora.text = nHora

        })

        vm.getFrecuencia().observe(this, Observer<Int> { nFreq ->

            if(nFreq == 1){
                frecuencia.text = "Cada " + nFreq.toString() + " semana"
            }
            else{
                frecuencia.text = "Cada " + nFreq.toString() + " semanas"
            }

        })

        return builder.create()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        setAbierto(false)
    }
}