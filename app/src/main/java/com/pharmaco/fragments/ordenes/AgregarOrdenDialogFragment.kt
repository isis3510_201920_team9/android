package com.pharmaco.fragments.ordenes

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.opengl.Visibility
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import android.widget.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.pharmaco.R
import com.pharmaco.broadcastReceivers.ConnectivityReceiver
import com.pharmaco.fragments.GenericAlertDialog
import com.pharmaco.fragments.SwipeLeftToDeleteCallback
import com.pharmaco.fragments.medicamentos.AgregarMedicamentoDialogFragment
import com.pharmaco.fragments.medicamentos.adapters.FrecuenciaListAdapter
import com.pharmaco.models.Frecuencia
import com.pharmaco.viewModels.ordenes.OrdenConstants
import com.pharmaco.viewModels.ordenes.OrdenCreateViewModel
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class AgregarOrdenDialogFragment: DialogFragment(), ConnectivityReceiver.ConnectivityReceiverListener{

    private val TAG = this.javaClass.simpleName

    private lateinit var viewModel: OrdenCreateViewModel
    private var mSnackBar: Snackbar? = null
    private var rootlayout: CoordinatorLayout? = null
    private lateinit var frecuenciasList: List<Frecuencia>
    private lateinit var con_rec: ConnectivityReceiver
    private lateinit var loadingCircle: ProgressBar


    val REQUEST_IMAGE_CAPTURE: Int = 1;
    val PERMISSION_REQUEST_CODE: Int = 2;
    private var mCurrentPhotoPath: String? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.dialog_fragment_agregar_orden, container, false)

        val titulo = view.findViewById<TextView>(R.id.tituloAgregarOrden)
        val nombreDoctor: EditText = view.findViewById<EditText>(R.id.nombreMedicamentoAgregarOrden)
        val especialidad: EditText = view.findViewById<EditText>(R.id.especialidad)
        val cerrar = view.findViewById<ImageView>(R.id.iconoCerrarAgregarOrden)
        val foto = view.findViewById<ImageButton>(R.id.tomarFotoOrden)
        loadingCircle = view.findViewById(R.id.progressBar)
        val agregar = view.findViewById<Button>(R.id.agregarMedicamentosEnAgregarOrden)
        val enviar = view.findViewById<Button>(R.id.enviar)
        rootlayout = view.findViewById<CoordinatorLayout>(R.id.coordinatorView) as CoordinatorLayout

        titulo.text = "Agregar Orden"

        var listView: RecyclerView = view.findViewById<RecyclerView>(R.id.listaAgregarOrden)

        agregar.setOnClickListener {
            var fm = fragmentManager
            //AgregarMedicamentoDialogFragment(viewModel).show(fm!!, "")

            fm!!.beginTransaction().add(this.id, AgregarMedicamentoDialogFragment(viewModel)).commit()



        }

        foto.setOnClickListener{
            if (verificarPermisos()) tomarFoto() else pedirPermisos()
        }

        /**
        dateText.setOnClickListener {
            Log.d("AAAAAAAAAAAA", "va a pasar al datepicker")
            val navController: NavController = NavHostFragment.findNavController(this)
            navController.navigate(R.id.action_pedirDomicilioDialogFragment_to_datePickerFragment)
        }**/

        cerrar.setOnClickListener {
            //como accedo al dialogo anterior desde aca para cerrarlo
            dismiss()
        }

        fun createOrden(){
            var doctorEscogido = nombreDoctor.text.toString()
            var especialidad = especialidad.text.toString()

            viewModel.addOrden(doctorEscogido, especialidad, tipo = OrdenConstants.Tipo.TEXTO)
            var controller = findNavController()
            controller.popBackStack(R.id.ordenDetailDialogFragment, true)
            dismiss()
        }

        enviar.setOnClickListener{

            if(nombreDoctor.text.toString() == "" || especialidad.text.toString() == ""
                || !::frecuenciasList.isInitialized || frecuenciasList.size == 0){
                val alerta = GenericAlertDialog("Debe completar todos los campos para poder agregar la orden", false, {}, {})
                val fm = fragmentManager
                alerta.show(fm!!, "")
                return@setOnClickListener
            }

            //Revisar si hay o no internet
            if(!viewModel.verifyAvailableNetwork(this.activity!!)){

                //si no hay internet, generar alerta.
                val alerta = GenericAlertDialog("No hay internet en este momento. " +
                        "Intentaremos sincronizar tus datos cuando recuperemos la conexión. ¿Deseas continuar?"
                    , true, {
                    createOrden()
                }, {})
                val fm = fragmentManager
                alerta.show(fm!!, "")
                return@setOnClickListener
            }
            else{
                createOrden()
            }

        }

        val swipeLeft = object : SwipeLeftToDeleteCallback(this.requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                viewModel.deleteFrecuencia(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeLeft)
        itemTouchHelper.attachToRecyclerView(listView)

        con_rec = ConnectivityReceiver()

        this.activity!!.registerReceiver(con_rec,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )

        viewModel = ViewModelProviders.of(this)[OrdenCreateViewModel::class.java]

        viewModel.getFrecuencias().observe(this, Observer<MutableList<Frecuencia>>{ frecuencias ->

            frecuenciasList = frecuencias

            listView.adapter = FrecuenciaListAdapter(frecuencias)
            listView.layoutManager = LinearLayoutManager(this.requireContext())

        })


        viewModel.successfulImage.observe(this, Observer<Boolean> { succ ->

            Log.d("ON LD OBSERVE", "SUCCIMG VAL ${succ}")
            if(!succ){

                val alerta = GenericAlertDialog("Lo sentimos, no pudimos leer la imagen, por favor intente volver a tomar la " +
                        "foto asegurándose de tener buena luz y de que la hoja no esté doblada."
                    , false, {}, {})
                alerta.show(fragmentManager!!, "")
                viewModel.successfulImage.value = true
            }
            loadingCircle.visibility = View.GONE

            if(viewModel.added > 0 ) {

                var plural = if (viewModel.added > 1) "medicamentos" else "medicamento"
                var plural_med = if(viewModel.added > 1 ) "detectaron" else "detectó"
                val alerta = GenericAlertDialog("Se $plural_med ${viewModel.added} $plural en la orden."
                        , false, {}, {})
                alerta.show(fragmentManager!!, "")
            }

        })

        return view
    }

    override fun onStart() {
        super.onStart()

        var dialog:  Dialog = requireDialog()

        if(dialog != null){

            var width = ViewGroup.LayoutParams.MATCH_PARENT
            var height = ViewGroup.LayoutParams.MATCH_PARENT

            dialog.window!!.setLayout(width, height)
            dialog.window!!.setWindowAnimations(R.style.AppTheme_Slide)
        }
    }

    private fun verificarPermisos(): Boolean {
        return (ContextCompat.checkSelfPermission(this.requireContext(), android.Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this.requireContext(),
            android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
    }

    private fun pedirPermisos() {
        requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA), PERMISSION_REQUEST_CODE)
    }

    private fun tomarFoto() {

        val intent: Intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val file: File = createFile()

        val uri: Uri = FileProvider.getUriForFile(this.requireContext(),"com.example.android.fileprovider", file)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,uri)
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
    }

    @Throws(IOException::class)
    private fun createFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = this.requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile("JPEG_${timeStamp}_", ".jpg", storageDir).apply {
            // Save a file: path for use with ACTION_VIEW intents
            Log.d("RUTA FOTOS", absolutePath)
            mCurrentPhotoPath = absolutePath
        }
    }

    //TODO: no se esta ejecutando por alguna razon (pendiente para sprint 2)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode==PERMISSION_REQUEST_CODE){
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) && (grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    tomarFoto()

                } else {
                    mSnackBar = Snackbar.make(rootlayout as View, R.string.no_permitions, Snackbar.LENGTH_INDEFINITE).setAction("OK") {
                        // Call action functions here
                        mSnackBar!!.dismiss()
                    }
                    mSnackBar?.show()
                }
                return
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            loadingCircle.visibility = View.VISIBLE
            //Aca se dice que hacer con la foto tomada
            val auxFile = File(this!!.mCurrentPhotoPath!!)

            viewModel.addOrden(tipo = OrdenConstants.Tipo.IMAGEN, imagen = auxFile)

        }
    }
    private fun showMessage(isConnected: Boolean) {
        if (!isConnected) {

            mSnackBar = Snackbar.make(rootlayout as View, R.string.no_internet, Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            mSnackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            mSnackBar?.show()
        } else {
            mSnackBar?.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()

        ConnectivityReceiver.connectivityReceiverListener = this
    }

    /**
     * Callback will be called when there is change
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showMessage(isConnected)
    }

    override fun onDestroy() {
        super.onDestroy()
        this.activity!!.unregisterReceiver(con_rec)
    }
}