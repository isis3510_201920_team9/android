package com.pharmaco.fragments.ordenes

import android.app.Dialog
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import android.widget.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.pharmaco.R
import com.pharmaco.broadcastReceivers.ConnectivityReceiver
import com.pharmaco.fragments.GenericAlertDialog
import com.pharmaco.fragments.SwipeLeftToDeleteCallback
import com.pharmaco.fragments.medicamentos.AgregarMedicamentoDialogFragment
import com.pharmaco.fragments.medicamentos.adapters.FrecuenciaListAdapter
import com.pharmaco.models.Frecuencia
import com.pharmaco.models.Orden
import com.pharmaco.viewModels.ordenes.OrdenEditViewModel

class EditOrdenDialogFragment: DialogFragment(), ConnectivityReceiver.ConnectivityReceiverListener{

    private lateinit var viewModel: OrdenEditViewModel
    private var mSnackBar: Snackbar? = null
    private var rootlayout: CoordinatorLayout? = null
    private lateinit var con_rec: ConnectivityReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.dialog_fragment_agregar_orden, container, false)

        val titulo = view.findViewById<TextView>(R.id.tituloAgregarOrden)
        val cerrar = view.findViewById<ImageView>(R.id.iconoCerrarAgregarOrden)
        val foto = view.findViewById<ImageButton>(R.id.tomarFotoOrden)
        val agregar = view.findViewById<Button>(R.id.agregarMedicamentosEnAgregarOrden)
        var nombreDoctor = view.findViewById<EditText>(R.id.nombreMedicamentoAgregarOrden)
        var especialidad = view.findViewById<EditText>(R.id.especialidad)
        val enviar = view.findViewById<Button>(R.id.enviar)
        rootlayout = view.findViewById<CoordinatorLayout>(R.id.coordinatorView) as CoordinatorLayout

        val args: Bundle? = getArguments()
        val orden = args?.getSerializable("orden")!! as Orden

        titulo.text = "Editar Orden"
        nombreDoctor.setText(orden.doctor)
        especialidad.setText(orden.especialidad)

        var listView = view.findViewById<RecyclerView>(R.id.listaAgregarOrden)

        listView.adapter = FrecuenciaListAdapter(orden.frecuencias)
        var nombreDoctorEscogido = nombreDoctor.text
        var especialidadEscogido = especialidad.text
        val mutableList = orden.frecuencias
        listView.layoutManager = LinearLayoutManager(this.requireContext())

        agregar.setOnClickListener {
            var fm = fragmentManager
            AgregarMedicamentoDialogFragment(viewModel).show(fm!!, "")
        }

        val swipeLeft = object : SwipeLeftToDeleteCallback(this.requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                viewModel.deleteFrecuencia(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeLeft)
        itemTouchHelper.attachToRecyclerView(listView)

        foto.setOnClickListener{

        }
        cerrar.setOnClickListener {
            //como accedo al dialogo anterior desde aca para cerrarlo
            dismiss()
        }

        fun editOrden(){

            viewModel.updateOrden(nombreDoctor = nombreDoctor.text.toString(),
                especialidad = especialidad.text.toString())

            dismiss()
        }

        enviar.setOnClickListener {

            if(nombreDoctor.text.toString() == "" || especialidad.text.toString() == ""){
                val alerta = GenericAlertDialog("Debe completar todos los campos para poder actualizar una orden", false, {}, {})
                val fm = fragmentManager
                alerta.show(fm!!, "")
                return@setOnClickListener
            }

            //Revisar si hay o no internet
            if(!viewModel.verifyAvailableNetwork(this.activity!!)){

                //si no hay internet, generar alerta.
                val alerta = GenericAlertDialog("No hay internet en este momento. " +
                        "Intentaremos sincronizar tus datos cuando recuperemos la conexión. ¿Deseas continuar?"
                    , true, {
                    editOrden()
                }, {})
                val fm = fragmentManager
                alerta.show(fm!!, "")
                return@setOnClickListener
            }
            else{
                editOrden()
            }
        }

        con_rec = ConnectivityReceiver()

        this.activity!!.registerReceiver(con_rec,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )

        viewModel = ViewModelProviders.of(this)[OrdenEditViewModel::class.java]
        viewModel.setFrecuencias(orden.frecuencias)
        viewModel.ordenId = orden.id

        viewModel.getFrecuencias().observe(this, Observer<MutableList<Frecuencia>>{ frecuencias ->

            listView.adapter = FrecuenciaListAdapter(frecuencias)
            listView.layoutManager = LinearLayoutManager(this.requireContext())

        })

        return view
    }

    override fun onStart() {
        super.onStart()

        var dialog:  Dialog = requireDialog()

        if(dialog != null){

            var width = ViewGroup.LayoutParams.MATCH_PARENT
            var height = ViewGroup.LayoutParams.MATCH_PARENT

            dialog.window!!.setLayout(width, height)
            dialog.window!!.setWindowAnimations(R.style.AppTheme_Slide)
        }
    }

    private fun showMessage(isConnected: Boolean) {
        if (!isConnected) {

            mSnackBar = Snackbar.make(rootlayout as View, R.string.no_internet, Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            mSnackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            mSnackBar?.show()
        } else {
            mSnackBar?.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()

        ConnectivityReceiver.connectivityReceiverListener = this
    }

    /**
     * Callback will be called when there is change
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showMessage(isConnected)
    }

    override fun onDestroy() {
        super.onDestroy()
        this.activity!!.unregisterReceiver(con_rec)
    }
}