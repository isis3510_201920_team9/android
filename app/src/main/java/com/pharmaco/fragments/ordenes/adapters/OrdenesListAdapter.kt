package com.pharmaco.fragments.ordenes.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.pharmaco.R
import androidx.core.graphics.drawable.DrawableCompat
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.auxiliary.CallbackWrapper
import com.pharmaco.fragments.ordenes.AgregarOrdenDialogFragment
import com.pharmaco.fragments.ordenes.OrdenDetailDialogFragment
import com.pharmaco.fragments.ordenes.OrdenesFragmentDirections
import com.pharmaco.fragments.turnos.PedirTurnoDialogFragment
import com.pharmaco.models.Orden
import java.sql.Wrapper
import java.text.SimpleDateFormat


class OrdenesListAdapter (private val items: MutableList<Orden>, val getAbierto: () -> Boolean, val setAbierto: (Boolean) -> Unit): RecyclerView.Adapter<OrdenesListAdapter.OrdenesListVH>(){
    /**override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater:LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(resources, null)

        val titulo: TextView = view.findViewById(R.id.titulo)
        val descripcion: TextView = view.findViewById(R.id.descripcion)
        val fecha: TextView = view.findViewById(R.id.fecha)
        val tipo: ImageView = view.findViewById(R.id.icono)

        var orden: Orden = items[position]

        titulo.text = orden.titulo
        descripcion.text = orden.descripcion
        fecha.text = orden.fecha.hours.toString()+":"+orden.fecha.hours.toString()

        var drawable:Drawable? = null;
        drawable = ContextCompat.getDrawable(mCtx, R.drawable.ic_ordenes_24dp)!!
        val wrappedDrawable = DrawableCompat.wrap(drawable)
        DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(mCtx, R.color.colorVerde))

        tipo.setImageDrawable(drawable);

        view.setOnClickListener(object: View.OnClickListener{

            override fun onClick(p0: View?) {
                val navController: NavController = view.findNavController()
                val action = OrdenesFragmentDirections.actionOrdenesFragmentToOrdenDetailDialogFragment(orden)
                navController.navigate(action)
            }
        })

        return view
    }*/
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrdenesListVH {

        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.fragment_list_description_row, parent, false)

        return OrdenesListVH(inflatedView)

    }

    override fun onBindViewHolder(holder: OrdenesListVH, position: Int) {

        holder.bind(items[position], getAbierto, setAbierto)

    }

    override fun getItemCount(): Int = items.size

    fun addItem(orden: Orden) {

        items.add(orden)
        notifyItemInserted(items.size)
    }

    fun removeAt(position: Int) {

        items.removeAt(position)
        notifyItemRemoved(position)
    }


    class OrdenesListVH(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        var view: View = v
        var orden: Orden? = null

        init {
            v.setOnClickListener {
            }
        }

        override fun onClick(v: View) {
        }

        companion object {
            //5
            private val ORDEN_KEY = "ORDEN"
        }

        fun bind(orden: Orden, getAbierto: () -> Boolean, setAbierto: (Boolean) -> Unit) {

            val titulo: TextView = view.findViewById(R.id.titulo)
            val descripcion: TextView = view.findViewById(R.id.descripcion)
            val fecha: TextView = view.findViewById(R.id.fecha)
            val tipo: ImageView = view.findViewById(R.id.icono)

            titulo.text = "Orden de " + orden.especialidad
            descripcion.text = "Doctor " + orden.doctor
            val formatter  = SimpleDateFormat("dd/MM/yyyy")
            fecha.text = formatter.format(orden.fecha.time)

            var drawable:Drawable? = null;
            drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_ordenes_24dp)!!
            val wrappedDrawable = DrawableCompat.wrap(drawable)
            DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(view.context, R.color.colorVerde))

            tipo.setImageDrawable(drawable);

            view.setOnClickListener(object: View.OnClickListener{

                override fun onClick(p0: View?) {
                    Log.d("AFUERA", getAbierto().toString())
                    if(!getAbierto()) {
                        Log.d("ADENTRO", getAbierto().toString())
                        setAbierto(true)
                        Log.d("ADENTRO 2", getAbierto().toString())
                        val fm = (view.context as AppCompatActivity).supportFragmentManager
                        val ordenDetail = OrdenDetailDialogFragment(orden, CallbackWrapper(setAbierto))
                        ordenDetail.show(fm, "")
                    }

                }
            })

            view.setOnLongClickListener {
                val navController: NavController = view.findNavController()
                val action = OrdenesFragmentDirections.actionOrdenesFragmentToEditOrdenDialogFragment(orden)
                navController.navigate(action)
                true
            }
        }
    }
}

