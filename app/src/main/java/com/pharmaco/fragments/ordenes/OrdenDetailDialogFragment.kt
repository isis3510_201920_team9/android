package com.pharmaco.fragments.ordenes

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.telecom.Call
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.R
import com.pharmaco.fragments.medicamentos.adapters.FrecuenciaListAdapter
import com.pharmaco.fragments.medicamentos.adapters.MedicamentosPedidosListAdapter
import com.pharmaco.models.Domicilio
import com.pharmaco.models.Frecuencia
import com.pharmaco.models.Orden
import com.pharmaco.viewmodelfactories.turno.OrdenDetailViewModelFactory
import kotlinx.android.synthetic.main.dialog_fragment_suscripcion_detail.*
import java.text.SimpleDateFormat
import androidx.lifecycle.Observer
import com.pharmaco.auxiliary.CallbackWrapper
import com.pharmaco.fragments.domicilios.PedirDomicilioDialogFragment
import com.pharmaco.fragments.suscripciones.PedirSuscripcionDialogFragment
import com.pharmaco.fragments.turnos.PedirTurnoDialogFragment
import com.pharmaco.viewModels.ordenes.OrdenDetailViewModel


class OrdenDetailDialogFragment(var orden: Orden, val cBack: CallbackWrapper): AppCompatDialogFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val inflater: LayoutInflater = LayoutInflater.from(this.requireContext())
        var builder: AlertDialog.Builder = AlertDialog.Builder(this.requireContext())
        val view: View = inflater.inflate(R.layout.dialog_fragment_orden_detail, null)

        val descripcion = view.findViewById<TextView>(R.id.descripcionDetailOrden)
        val pedirTurnoBoton = view.findViewById<Button>(R.id.pedirTurno)
        val pedirSuscripcionBoton = view.findViewById<Button>(R.id.suscribirse)
        val pedirDomicilioBoton = view.findViewById<Button>(R.id.pedirDomicilio)
        var listView = view.findViewById<RecyclerView>(R.id.listaDetailOrden)

        //val args: Bundle? = getArguments()
        //val orden = args?.getSerializable("orden")!! as Orden
        //val callBack = args?.getSerializable("callBack")!! as CallbackWrapper

        //cBack = callBack.callBack

        val cb = {
            pedirTurnoBoton.isEnabled = true
            pedirSuscripcionBoton.isEnabled = true
            pedirDomicilioBoton.isEnabled = true
        }

        pedirTurnoBoton.setOnClickListener{
            pedirTurnoBoton.isEnabled = false
            pedirSuscripcionBoton.isEnabled = false
            pedirDomicilioBoton.isEnabled = false

            val fm = (view.context as AppCompatActivity).supportFragmentManager
            val pedirTurno = PedirTurnoDialogFragment(cb, orden, {dismiss()})
            pedirTurno.show(fm, "")
        }

        pedirSuscripcionBoton.setOnClickListener{
            pedirTurnoBoton.isEnabled = false
            pedirSuscripcionBoton.isEnabled = false
            pedirDomicilioBoton.isEnabled = false

            val fm = (view.context as AppCompatActivity).supportFragmentManager
            val pedirTurno = PedirSuscripcionDialogFragment(cb, orden, {dismiss()})
            pedirTurno.show(fm, "")
        }

        pedirDomicilioBoton.setOnClickListener{
            pedirTurnoBoton.isEnabled = false
            pedirSuscripcionBoton.isEnabled = false
            pedirDomicilioBoton.isEnabled = false

            val fm = (view.context as AppCompatActivity).supportFragmentManager
            val pedirTurno = PedirDomicilioDialogFragment(cb, orden, {dismiss()})
            pedirTurno.show(fm, "")
        }


        builder.setView(view).setTitle("Orden de " + orden.especialidad).setNegativeButton("Cancelar", {dialog, which->

        })

        //ViewModel
        val vm = ViewModelProviders.of(this,
            OrdenDetailViewModelFactory(orden)
        )[OrdenDetailViewModel::class.java]

        vm.getOrden().observe(this, Observer<Orden>{ orden ->

            builder.setView(view).setTitle("Orden de " + orden.especialidad).setNegativeButton("Cancelar", {dialog, which->

            })

            val frecuencias = orden.frecuencias

            Log.d("FRECUENCIAS", frecuencias.toString())


            descripcion.text = "Doctor " + orden.doctor

            listView.adapter = FrecuenciaListAdapter(frecuencias);
            listView.layoutManager = LinearLayoutManager(this.requireContext())

        })

        return builder.create()
    }


    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        cBack.callBack!!(false)
    }
}