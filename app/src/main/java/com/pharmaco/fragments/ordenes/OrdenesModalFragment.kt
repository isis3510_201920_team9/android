package com.pharmaco.fragments.ordenes

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.R
import com.pharmaco.fragments.medicamentos.adapters.MedicamentosSimpleListAdapter
import com.pharmaco.fragments.ordenes.adapters.OrdenesListAdapter
import com.pharmaco.models.MedicamentoCreate
import com.pharmaco.viewModels.interfaces.CreateViewModelInterface

class OrdenesModalFragment(val list: MutableList<MedicamentoCreate>, val viewModel: CreateViewModelInterface): AppCompatDialogFragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val inflater: LayoutInflater = LayoutInflater.from(this.requireContext())
        var builder: AlertDialog.Builder = AlertDialog.Builder(this.requireContext())
        val view: View = inflater.inflate(R.layout.generic_list, null)

        var listView= view.findViewById<RecyclerView>(R.id.listaGenerica)
        var listaVacia = view.findViewById<TextView>(R.id.listaVacia)
        var titulo = view.findViewById<TextView>(R.id.tituloListaGenerica)

        titulo.text = "Medicamentos de tus órdenes"

        //var list = mutableListOf<MedicamentoOrden>()
        //list.add(MedicamentoOrden("Dolex"))
        //list.add(MedicamentoOrden("Tiroxin 100"))
        //list.add(MedicamentoOrden("Dolex"))
        //list.add(MedicamentoOrden("Tiroxin 100"))
        //list.add(MedicamentoOrden("Dolex"))
        //list.add(MedicamentoOrden("Tiroxin 100"))

        listView.layoutManager = LinearLayoutManager(this.requireContext())

        viewModel.getMedicamentos().observe(this, Observer<MutableList<MedicamentoCreate>>{medicamentos ->
            Log.d("AAAA", "")
            listView.adapter = MedicamentosSimpleListAdapter(list, viewModel, this);
            if(medicamentos.isEmpty()){
                listView.visibility = View.GONE
                listaVacia.visibility = View.VISIBLE
            }

        })

        builder.setView(view)

        return builder.create()
    }

}