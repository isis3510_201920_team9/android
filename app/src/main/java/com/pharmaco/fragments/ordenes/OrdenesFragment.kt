package com.pharmaco.fragments.ordenes

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.pharmaco.R
import com.pharmaco.broadcastReceivers.ConnectivityReceiver
import com.pharmaco.fragments.SwipeLeftToDeleteCallback
import com.pharmaco.fragments.ordenes.adapters.OrdenesListAdapter
import com.pharmaco.models.Orden
import com.pharmaco.viewModels.ordenes.OrdenesViewModel
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.pharmaco.fragments.GenericAlertDialog


class OrdenesFragment : Fragment(), ConnectivityReceiver.ConnectivityReceiverListener{

    private lateinit var viewModel: OrdenesViewModel
    private var mSnackBar: Snackbar? = null
    private var rootlayout: CoordinatorLayout? = null
    public  var detalleAbierto = false
    private lateinit var con_rec: ConnectivityReceiver

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Mostrar la lista
        val view: View = inflater.inflate(R.layout.fragment_banner, container, false)

        var listView = view.findViewById<RecyclerView>(R.id.listaBanner)
        var swipeRefresh = view.findViewById<SwipeRefreshLayout>(R.id.swipeContainer)
        var listaVacia = view.findViewById<TextView>(R.id.listaVacia)
        rootlayout = view.findViewById<CoordinatorLayout>(R.id.coordinator)

        listView.layoutManager = LinearLayoutManager(this.requireContext())


        val swipeLeft = object : SwipeLeftToDeleteCallback(this.requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                val fm = fragmentManager
                val alerta = GenericAlertDialog("¿Seguro que desea borrar esta orden?", true, { ->
                    var borrar = viewModel.deleteOrden(viewHolder.adapterPosition)
                    Log.d("PUDO BORRAR", borrar.toString())
                    if(!borrar){
                        val alerta2 = GenericAlertDialog("No puede borrar esta orden porque tiene un turno, domicilio, o suscripcion asociado", false, {viewModel.loadOrdenes()}, {})
                        alerta2.show(fm!!, "")
                    }
                }, {viewModel.loadOrdenes()})
                alerta.show(fm!!, "")
            }
        }

        swipeRefresh.setOnRefreshListener {
            viewModel.loadOrdenes()
            swipeRefresh.isRefreshing = false
        }
        val itemTouchHelper = ItemTouchHelper(swipeLeft)
        itemTouchHelper.attachToRecyclerView(listView)

        //ViewModel
        viewModel = ViewModelProviders.of(this)[OrdenesViewModel::class.java]

        viewModel.getOrdenes().observe(this, Observer<MutableList<Orden>>{ordenes ->

            listView.adapter = OrdenesListAdapter(ordenes, {detalleAbierto}, {pAbierto -> detalleAbierto = pAbierto})
            if(ordenes.isEmpty()){
                listView.visibility = View.GONE
                listaVacia.visibility = View.VISIBLE
            }

        })

        con_rec = ConnectivityReceiver()
        this.activity!!.registerReceiver(con_rec,
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
            )
        return view
    }

    private fun showMessage(isConnected: Boolean) {



        if (!isConnected) {

            mSnackBar = Snackbar.make(this.view!!, R.string.no_internet, Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            mSnackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            mSnackBar?.show()
        } else {
            mSnackBar?.dismiss()
        }


    }

    override fun onResume() {
        super.onResume()
        detalleAbierto = false
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    /**
     * Callback will be called when there is change
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showMessage(isConnected)
    }

    override fun onDestroy() {
        super.onDestroy()
        this.activity!!.unregisterReceiver(con_rec)
    }



}
