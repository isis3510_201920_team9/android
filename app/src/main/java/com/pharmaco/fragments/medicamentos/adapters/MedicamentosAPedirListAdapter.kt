package com.pharmaco.fragments.medicamentos.adapters

import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.content.ContextCompat
import com.pharmaco.R
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.fragments.GenericAlertDialog
import com.pharmaco.models.MedicamentoOrden
import com.pharmaco.models.MedicamentoTSDCreate
import com.pharmaco.viewModels.interfaces.CreateViewModelInterface
import android.app.Activity
import androidx.fragment.app.FragmentManager




class MedicamentosAPedirListAdapter (private val viewModel: CreateViewModelInterface, private val items: MutableList<MedicamentoTSDCreate>, val fragmentManager: FragmentManager): RecyclerView.Adapter<MedicamentosAPedirListAdapter.MedicamentosListVH>(){

    /**override fun getView(position: Int, convertView: View?, parent: ViewGroup): View
        val layoutInflater:LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(resources, null)

    val titulo: TextView = view.findViewById(R.id.tituloMedicamento)
    val imagen: ImageView = view.findViewById(R.id.iconoMedicamento)

    var medicamentoOrden: MedicamentoOrden = items[position]

    titulo.text = medicamentoOrden.nombre

    var drawable:Drawable? = null;
    drawable = ContextCompat.getDrawable(mCtx, R.drawable.ic_ordenes_24dp)!!
    val wrappedDrawable = DrawableCompat.wrap(drawable)
    DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(mCtx, R.color.colorVerde))

    imagen.setImageDrawable(drawable);

    return view
    }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedicamentosListVH {

        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.fragment_medicamentos_pedir_algo_row, parent, false)
        return MedicamentosListVH(inflatedView)
    }

    override fun onBindViewHolder(holder: MedicamentosListVH, position: Int) {
        holder.bind(position, items[position], viewModel, fragmentManager)
    }

    override fun getItemCount(): Int = items.size

    /*fun addItem(medicamentoOrden: MedicamentoTSD) {
        items.add(medicamentoOrden)
        notifyItemInserted(items.size)
    }*/

    fun removeAt(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    class MedicamentosListVH(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        var view: View = v
        var medicamentoOrden: MedicamentoOrden? = null

        init {
            v.setOnClickListener(this)
        }

        override fun onClick(v: View) {
        }

        companion object {
            //5
            private val MEDICAMENTO_KEY = "MEDICAMENTO"
        }

        fun bind(position: Int, medicamentoTSD: MedicamentoTSDCreate, viewModel: CreateViewModelInterface, fragmentManager: FragmentManager?) {

            val titulo: TextView = view.findViewById(R.id.tituloMedicamento)
            val imagen: ImageView = view.findViewById(R.id.iconoMedicamento)
            val cantidad: EditText = view.findViewById(R.id.cantidadMedicamento)

            var medicamentoTSD: MedicamentoTSDCreate = medicamentoTSD

            if(medicamentoTSD.unidadesPedidas != 0){
                cantidad.append(medicamentoTSD.unidadesPedidas.toString())
            }
            else{
                cantidad.append("")
            }

            titulo.text = medicamentoTSD.nombre

            /**
            cantidad.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                }

                override fun afterTextChanged(s: Editable) {
                    if(cantidad.text.toString() == "0") {
                        cantidad.append("")
                        val alerta = GenericAlertDialog("Debe completar todos los campos para poder agendar un turno", false, {})
                        alerta.show(fragmentManager!!, "")
                    }
                }
            })
            */

           /*cantidad.setOnFocusChangeListener { view, b ->

                if(!b) {
                    val text = (view as EditText).text
                    Log.d("ONFOCUSCHANGE", text.toString())


                    if (text.toString() == "") {
                        viewModel.editCantidadMedicamentoSeleccionado(pos = position, nCantidad = 0)

                    } else {

                        //si el usuario ingrrsa un número más grande que Int se totea,
                        //necesario implementar ,medida que evite el ingreso de este número.
                        if (text.toString().length <= 3)
                            viewModel.editCantidadMedicamentoSeleccionado(
                                pos = position,
                                nCantidad = text.toString().toInt()
                            )
                    }
                }
                
            }*/

            cantidad.doAfterTextChanged { text ->

                if(text.toString() == ""){
                    viewModel.editCantidadMedicamentoSeleccionado(pos = position, nCantidad = 0)
                }
                else if (text.toString() == "0"){
                    cantidad.setText("")
                    val alerta = GenericAlertDialog("La cantidad de medicamentos no puede ser 0", false, {}, {})
                    alerta.show(fragmentManager!!, "")

                }
                else{

                    //si el usuario ingrrsa un número más grande que Int se totea,
                    //necesario implementar ,medida que evite el ingreso de este número.
                    //if(text.toString().length <= 3)
                    viewModel.editCantidadMedicamentoSeleccionado(pos = position, nCantidad = text.toString().toInt())
                }

            }


            var drawable:Drawable? = null;
            drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_logo_blanco)!!
            val wrappedDrawable = DrawableCompat.wrap(drawable)
            DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(view.context, R.color.colorPrimary))

            imagen.setImageDrawable(drawable);
        }

    }

}


