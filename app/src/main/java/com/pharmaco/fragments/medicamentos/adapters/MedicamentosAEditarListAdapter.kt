package com.pharmaco.fragments.medicamentos.adapters

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import com.pharmaco.R
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.models.MedicamentoTSD


class MedicamentosAEditarListAdapter (private val items: MutableList<MedicamentoTSD>): RecyclerView.Adapter<MedicamentosAEditarListAdapter.MedicamentosListVH>(){

    /**override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater:LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(resources, null)

    val titulo: TextView = view.findViewById(R.id.tituloMedicamento)
    val imagen: ImageView = view.findViewById(R.id.iconoMedicamento)

    var medicamentoOrden: MedicamentoTSD = items[position]

    titulo.text = medicamentoOrden.nombre

    var drawable:Drawable? = null;
    drawable = ContextCompat.getDrawable(mCtx, R.drawable.ic_ordenes_24dp)!!
    val wrappedDrawable = DrawableCompat.wrap(drawable)
    DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(mCtx, R.color.colorVerde))

    imagen.setImageDrawable(drawable);

    return view
    }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedicamentosListVH {

        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.fragment_medicamentos_padidos_list_row, parent, false)
        return MedicamentosListVH(inflatedView)
    }

    override fun onBindViewHolder(holder: MedicamentosListVH, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    fun addItem(medicamentoOrden: MedicamentoTSD) {
        items.add(medicamentoOrden)
        notifyItemInserted(items.size)
    }

    fun removeAt(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    class MedicamentosListVH(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        var view: View = v
        var medicamentoOrden: MedicamentoTSD? = null

        init {
            v.setOnClickListener(this)
        }

        override fun onClick(v: View) {
        }

        companion object {
            //5
            private val MEDICAMENTO_KEY = "MEDICAMENTO"
        }

        fun bind(medicamentoOrden: MedicamentoTSD) {

            val titulo: TextView = view.findViewById(R.id.tituloMedicamento)
            val imagen: ImageView = view.findViewById(R.id.iconoMedicamento)

            var medicamentoTSD: MedicamentoTSD = medicamentoOrden

            titulo.text = medicamentoTSD.nombre

            var drawable:Drawable? = null;
            drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_logo_blanco)!!
            val wrappedDrawable = DrawableCompat.wrap(drawable)
            DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(view.context, R.color.colorPrimary))

            imagen.setImageDrawable(drawable);
        }

    }

}


