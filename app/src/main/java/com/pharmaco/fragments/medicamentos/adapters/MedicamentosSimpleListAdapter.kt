package com.pharmaco.fragments.medicamentos.adapters

import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.content.ContextCompat
import com.pharmaco.R
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.models.MedicamentoCreate
import com.pharmaco.models.MedicamentoOrden
import com.pharmaco.viewModels.interfaces.CreateViewModelInterface


class MedicamentosSimpleListAdapter (private val items: MutableList<MedicamentoCreate>, val viewModel: CreateViewModelInterface, val fragment: AppCompatDialogFragment): RecyclerView.Adapter<MedicamentosSimpleListAdapter.MedicamentosListVH>(){

    /**override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater:LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(resources, null)

    val titulo: TextView = view.findViewById(R.id.tituloMedicamento)
    val imagen: ImageView = view.findViewById(R.id.iconoMedicamento)

    var medicamentoOrden: MedicamentoOrden = items[position]

    titulo.text = medicamentoOrden.nombre

    var drawable:Drawable? = null;
    drawable = ContextCompat.getDrawable(mCtx, R.drawable.ic_ordenes_24dp)!!
    val wrappedDrawable = DrawableCompat.wrap(drawable)
    DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(mCtx, R.color.colorVerde))

    imagen.setImageDrawable(drawable);

    return view
    }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedicamentosListVH {

        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.fragment_simple_list_row, parent, false)
        return MedicamentosListVH(inflatedView, viewModel)
    }

    override fun onBindViewHolder(holder: MedicamentosListVH, position: Int) {
        holder.bind(items[position], fragment)
    }

    override fun getItemCount(): Int = items.size

    /*fun addItem(medicamentoOrden: MedicamentoOrden) {
        items.add(medicamentoOrden)
        notifyItemInserted(items.size)
    }

    fun removeAt(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }*/

    class MedicamentosListVH(v: View, val viewModel: CreateViewModelInterface) : RecyclerView.ViewHolder(v), View.OnClickListener {
        var view: View = v
        var medicamentoOrden: MedicamentoOrden? = null

        init {
            v.setOnClickListener(this)
        }

        override fun onClick(v: View) {
        }

        companion object {
            //5
            private val MEDICAMENTO_KEY = "MEDICAMENTO"
        }

        fun bind(medicamentoOrden: MedicamentoCreate, fragment: AppCompatDialogFragment) {

            val titulo: TextView = view.findViewById(R.id.titulo)
            val imagen: ImageView = view.findViewById(R.id.icono)

            var medicamentoOrden: MedicamentoCreate = medicamentoOrden

            titulo.text = medicamentoOrden.nombre

            var drawable:Drawable? = null;
            drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_logo_blanco)!!
            val wrappedDrawable = DrawableCompat.wrap(drawable)
            DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(view.context, R.color.colorPrimary))

            imagen.setImageDrawable(drawable);

            view.setOnClickListener{
                //este es el listener de la lista de todos los medicamentos

                Log.d("ON CLICK LISTENER", "ENTRÓ")
                //TODO, esto no está sirviendo.
                viewModel.addMedicamentoSeleccionado(medicamentoOrden)
                fragment.dismiss()

            }
        }

    }

}


