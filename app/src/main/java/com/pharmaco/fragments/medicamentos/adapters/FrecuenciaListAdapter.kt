package com.pharmaco.fragments.medicamentos.adapters

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import com.pharmaco.R
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.models.Frecuencia


class FrecuenciaListAdapter (private val items: MutableList<Frecuencia>): RecyclerView.Adapter<FrecuenciaListAdapter.FrecuenciaListVH>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FrecuenciaListVH {

        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.fragment_frecuencia_list_row, parent, false)
        return FrecuenciaListVH(inflatedView)
    }

    override fun onBindViewHolder(holder: FrecuenciaListVH, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    fun addItem(frecuencia: Frecuencia) {
        items.add(frecuencia)
        notifyItemInserted(items.size)
    }

    fun removeAt(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    class FrecuenciaListVH(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        var view: View = v
        var frecuencia: Frecuencia? = null

        init {
            v.setOnClickListener(this)
        }

        override fun onClick(v: View) {
        }

        companion object {
            //5
            private val MEDICAMENTO_KEY = "MEDICAMENTO"
        }

        fun bind(frecuencia: Frecuencia) {

            val nombre: TextView = view.findViewById(R.id.nombreMedicamento)
            val imagen: ImageView = view.findViewById(R.id.iconoMedicamento)
            val unidades: TextView = view.findViewById(R.id.unidades)
            val numero: TextView = view.findViewById(R.id.edad)
            val unidadFrecuencia: TextView = view.findViewById(R.id.unidadFrecuencia)
            val periodo: TextView = view.findViewById(R.id.periodo)
            val unidadPeriodo: TextView = view.findViewById(R.id.unidadPeriodo)

            var frecuencia: Frecuencia = frecuencia

            nombre.text = frecuencia.medicamento.nombre
            unidades.text = frecuencia.unidades.toString()
            numero.text = frecuencia.numero.toString()
            unidadFrecuencia.text = frecuencia.unidadFrecuencia
            periodo.text = frecuencia.periodo.toString()
            unidadPeriodo.text = frecuencia.unidadPeriodo


            var drawable:Drawable? = null;
            drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_logo_blanco)!!
            val wrappedDrawable = DrawableCompat.wrap(drawable)
            DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(view.context, R.color.colorPrimary))

            imagen.setImageDrawable(drawable);
        }
    }
}


