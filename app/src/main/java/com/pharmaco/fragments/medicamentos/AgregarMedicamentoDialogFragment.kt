package com.pharmaco.fragments.medicamentos

import android.app.Dialog
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import android.widget.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.pharmaco.R
import com.pharmaco.broadcastReceivers.ConnectivityReceiver
import com.pharmaco.fragments.GenericAlertDialog
import com.pharmaco.viewModels.interfaces.OrdenCreateEditViewModelInterface

class AgregarMedicamentoDialogFragment(val viewModel: OrdenCreateEditViewModelInterface): DialogFragment(), ConnectivityReceiver.ConnectivityReceiverListener{

    private val TAG = this.javaClass.simpleName
    private var mSnackBar: Snackbar? = null
    private var rootlayout: CoordinatorLayout? = null

    private lateinit var con_rec: ConnectivityReceiver

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.dialog_fragment_agregar_medicamento, container, false)

        val cerrar: ImageView = view.findViewById<ImageView>(R.id.iconoCerrarAgregarMedicamento)
        val enviar = view.findViewById<Button>(R.id.enviar)

        rootlayout = view.findViewById<CoordinatorLayout>(R.id.coordinatorView) as CoordinatorLayout
        cerrar.setOnClickListener {
            //como accedo al dialogo anterior desde aca para cerrarlo
            dismiss()
        }

        val pesos = arrayOf("mg", "g", "kg")
        val unidadMedicamento = view.findViewById<Spinner>(R.id.spinnerUnidadPeso1)
        var selectedItem: String? = ""
        if (unidadMedicamento != null) {
            val arrayAdapter = ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_item, pesos)
            unidadMedicamento.adapter = arrayAdapter

            unidadMedicamento.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    selectedItem = pesos[position]
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }

        val unidadFrecuencia = view.findViewById<Spinner>(R.id.spinnerUnidadPeso2)
        val tiempos = arrayOf("horas", "dias", "semanas", "meses")
        var tiempos2 = arrayOf<String>()
        var selectedItem2: String? = ""
        val unidadPeriodo = view.findViewById<Spinner>(R.id.spinnerUnidadTiempo)
        if (unidadFrecuencia != null) {
            val arrayAdapter2 = ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_item, tiempos)
            unidadFrecuencia.adapter = arrayAdapter2

            unidadFrecuencia.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    selectedItem2 = tiempos[position]
                    tiempos2 = arrayOf()
                    Log.d("TIEMPOS VACIO", tiempos2.size.toString())
                    for(i in position..3){
                        tiempos2+=tiempos[i]
                    }
                    Log.d("TIEMPOS FINAL", tiempos2.size.toString())
                    val newAdapter = ArrayAdapter(view.context, android.R.layout.simple_spinner_item, tiempos2)
                    unidadPeriodo.adapter = newAdapter
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }

        var selectedItem3: String? = ""
        if (unidadPeriodo != null) {
            val arrayAdapter3 = ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_item, tiempos2)
            unidadPeriodo.adapter = arrayAdapter3

            unidadPeriodo.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    selectedItem3 = tiempos2[position]
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }

        val nombreMedicamento = view.findViewById<AutoCompleteTextView>(R.id.nombreMedicamentoAgregarOrden)
        val dosisPorUnidad = view.findViewById<EditText>(R.id.dosisPorUnidadAgregarMedicamento)
        val cantidadTomar = view.findViewById<EditText>(R.id.cantidadTomarAgregarMedicamento)
        val frecuenciaTomar = view.findViewById<EditText>(R.id.frecuenciaTomarAgregarMedicamento)
        val periodoTomar = view.findViewById<EditText>(R.id.periodoTomarAgregarMedicamento)

        nombreMedicamento.doAfterTextChanged { text ->

            Log.d(TAG, "doAfterTextChanged: $text")

            if(text!= null && text.toString().length >= 3){
                viewModel.buscarMedicamento(text.toString())
            }
            viewModel.confirmarMedicamento(nombreMedicamento.text.toString())

        }

        nombreMedicamento.setOnFocusChangeListener{ view, b ->
            Log.d(TAG, "ON FOCUS CHANGE $b")

            //Si el usuario ha terminado de ingresar el medicamento, se consulta en la BD que, en efecto, exista un medicamento con este nombre
            viewModel.confirmarMedicamento(nombreMedicamento.text.toString())

        }

        dosisPorUnidad.doAfterTextChanged { text ->

            if (text.toString() == "0"){
                dosisPorUnidad.setText("")
                val alerta = GenericAlertDialog("La cantidad de medicamentos no puede ser 0", false, {}, {})
                alerta.show(fragmentManager!!, "")

            }
        }

        cantidadTomar.doAfterTextChanged { text ->

            if (text.toString() == "0"){
                cantidadTomar.setText("")
                val alerta = GenericAlertDialog("La cantidad de medicamentos no puede ser 0", false, {}, {})
                alerta.show(fragmentManager!!, "")

            }
        }

        frecuenciaTomar.doAfterTextChanged { text ->

            if (text.toString() == "0"){
                frecuenciaTomar.setText("")
                val alerta = GenericAlertDialog("La cantidad de medicamentos no puede ser 0", false, {}, {})
                alerta.show(fragmentManager!!, "")

            }
        }

        periodoTomar.doAfterTextChanged { text ->

            if (text.toString() == "0"){
                periodoTomar.setText("")
                val alerta = GenericAlertDialog("La cantidad de medicamentos no puede ser 0", false, {}, {})
                alerta.show(fragmentManager!!, "")

            }
        }




        fun addMedicamento() {

            viewModel.addFrecuencia(nombreMedicamento = nombreMedicamento.text.toString(),
                    dosisPorUnidad =  dosisPorUnidad.text.toString().toInt(),
                    medicamentoDosisUnidad = unidadMedicamento.selectedItem.toString(),
                    tomarUnidad = cantidadTomar.text.toString().toInt(),
                    tomarFrecuencia = frecuenciaTomar.text.toString().toInt(),
                    tomarFrecuenciaUnidad = unidadFrecuencia.selectedItem.toString(),
                    tomarPeriodo = periodoTomar.text.toString().toInt(),
                    tomarPeriodoUnidad = unidadPeriodo.selectedItem.toString()
            )
            dismiss()
        }

        enviar.setOnClickListener{

            if(nombreMedicamento.text.toString() == "" || dosisPorUnidad.text.toString() == "" || cantidadTomar.text.toString() == ""
                    || frecuenciaTomar.text.toString() == ""
                    || periodoTomar.text.toString() == "" ){
                val fm = fragmentManager
                val alerta = GenericAlertDialog("Debe completar todos los campos para poder agendar un turno", false, {}, {})
                alerta.show(fm!!, "")
                return@setOnClickListener
            }

            if(frecuenciaTomar.text.toString().toInt() > periodoTomar.text.toString().toInt() && unidadFrecuencia.selectedItem == unidadPeriodo.selectedItem) {
                val alerta = GenericAlertDialog("La frecuencia y el periodo no son validas.", false, {}, {})
                alerta.show(fragmentManager!!, "")
                return@setOnClickListener
            }

            //Ver si el medicamento sí es válido
            //Si medicamento no es válido, pedir confirmación al usuario
            if(viewModel.medicamentoValido != null && viewModel.medicamentoValido.value == false){

                val alerta = GenericAlertDialog("El medicamento seleccionado parece no existir, ¿desea continuar?", true, { addMedicamento() }, {})
                alerta.show(fragmentManager!!, "")

            }
            else{
                addMedicamento()
            }

        }

        con_rec = ConnectivityReceiver()

        this.activity!!.registerReceiver(con_rec,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )


        viewModel.medicamentosAlternativos.observe(this, Observer<List<String>?>{ list ->

            if(list != null){
                Log.d(TAG, "Medicamentos encontrados: ${list.toString()}")

                if(!list.isEmpty()){
                    //TODO, ahora el usuario debe escoger el medicamento correcto.
                    //Mostrar lista al usuario

                    ArrayAdapter<String>(this.context!!, android.R.layout.simple_list_item_1, list).also { adapter ->
                        nombreMedicamento.setAdapter(adapter)
                    }
                }
            }



        })

        return view
    }

    override fun onStart() {
        super.onStart()

        var dialog:  Dialog = requireDialog()

        if(dialog != null){

            var width = ViewGroup.LayoutParams.MATCH_PARENT
            var height = ViewGroup.LayoutParams.MATCH_PARENT

            dialog.window!!.setLayout(width, height)
            dialog.window!!.setWindowAnimations(R.style.AppTheme_Slide)
        }
    }

    private fun showMessage(isConnected: Boolean) {
        if (!isConnected) {

            mSnackBar = Snackbar.make(rootlayout as View, R.string.no_internet, Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            mSnackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            mSnackBar?.show()
        } else {
            mSnackBar?.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()

        ConnectivityReceiver.connectivityReceiverListener = this
    }

    /**
     * Callback will be called when there is change
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showMessage(isConnected)
    }

    override fun onDestroy() {
        super.onDestroy()
        this.activity!!.unregisterReceiver(con_rec)
    }
}