package com.pharmaco.fragments

import android.app.Dialog
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.DialogFragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.pharmaco.R
import com.pharmaco.models.FarmaciaTSD


class MapFragment(val listFarmacias: List<FarmaciaTSD>, val onSelect: (farmacia: FarmaciaTSD) -> Unit, val onClose: () -> Unit) : DialogFragment(), OnMapReadyCallback {

    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private  val LOCATION_PERMISSION_REQUEST_CODE = 1
    private lateinit var lastLocation: Location


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view: View  = inflater.inflate(R.layout.dialog_fragment_map, container, false)


        val mapFragment = activity!!.supportFragmentManager!!.findFragmentById(R.id.mapView) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val cerrar = view.findViewById<ImageView>(R.id.cerrarMapa)

        cerrar.setOnClickListener {
            dismiss()
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.activity!!)

        return view
    }

    override fun onStart() {
        super.onStart()

        var dialog: Dialog = requireDialog()

        if(dialog != null){

            var width = ViewGroup.LayoutParams.MATCH_PARENT
            var height = ViewGroup.LayoutParams.MATCH_PARENT

            dialog.window!!.setLayout(width, height)
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        val f = activity!!.supportFragmentManager!!.findFragmentById(R.id.mapView) as SupportMapFragment?
        if (f != null) {
            activity!!.supportFragmentManager!!.beginTransaction().remove(f).commit()
        }
        onClose()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap!!

        for(farmacia in listFarmacias) {
            map.addMarker(MarkerOptions().position(LatLng(farmacia.ubicacion.latitude, farmacia.ubicacion.longitude)).title(farmacia.nombre))
        }

        //map.moveCamera(CameraUpdateFactory.newLatLngZoom(colombia, 12.0f))
        map.getUiSettings().setZoomControlsEnabled(true)
        map.getUiSettings().isMyLocationButtonEnabled()
        map.setOnMarkerClickListener { marker: Marker? ->
            Log.d("", marker!!.title)
            dismiss()
            onSelect(listFarmacias.find{ it.nombre == marker!!.title }!!)
            true
        }

        if(!isLocationEnabled()){
            val alerta = GenericAlertDialog("Tu GPS no está activado. ¿Deseas prenderlo para ver tu ubicación en el mapa?", true, {

                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }, {

            })
            alerta.show(fragmentManager!!, "")
        }

        setUpMap()

    }
    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(this.requireContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this.requireActivity(),
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        if(!isLocationEnabled()){
            val currentLatLng = LatLng(4.6028028, -74.064689)
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
        }

        map.isMyLocationEnabled = true

        fusedLocationClient.lastLocation.addOnSuccessListener(this.requireActivity()) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
            }
        }
    }

    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

}
