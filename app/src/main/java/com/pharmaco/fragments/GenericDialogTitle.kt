package com.pharmaco.fragments

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import com.pharmaco.R

class GenericDialogTitle(val titulo: String, val cancelar: Boolean, val okCallBack: () -> Unit, val cancelCallBack: () -> Unit): AppCompatDialogFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val inflater: LayoutInflater = LayoutInflater.from(this.requireContext())
        var builder: AlertDialog.Builder = AlertDialog.Builder(this.requireContext())
        builder.setTitle(titulo).setPositiveButton("OK", {dialog, which->
            okCallBack()
        })

        if(cancelar){
            builder.setNegativeButton("Cancelar", {dialog, which->
                cancelCallBack()
            })
        }
        return builder.create()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        cancelCallBack()
    }
}