package com.pharmaco.fragments.turnos.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.pharmaco.R
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.fragments.turnos.TurnoDetailDialogFragment
import com.pharmaco.models.Turno
import android.os.Bundle
import java.text.SimpleDateFormat


class TurnosListAdapter (private val items: MutableList<Turno>, var setAbierto: (Boolean) -> Unit, val getAbierto: () -> Boolean): RecyclerView.Adapter<TurnosListAdapter.TurnosListVH>(){

    private lateinit var view: View

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TurnosListVH {

        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.fragment_eventos_list_description_row, parent, false)

        view = inflatedView

        return TurnosListVH(inflatedView, parent.context)

    }

    override fun onBindViewHolder(holder: TurnosListVH, position: Int) {
        holder.bind(items[position], setAbierto, getAbierto)
    }

    override fun getItemCount(): Int = items.size
    //fun addItem(turno: Turno) {
    //
    //    items.add(turno)
    //    notifyItemInserted(items.size)
    //}

    fun removeAt(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }


    class TurnosListVH(v: View, ctx: Context) : RecyclerView.ViewHolder(v), View.OnClickListener {
        var view: View = v
        var context: Context = ctx
        var turno: Turno? = null

        init {
            v.setOnClickListener {
            }
        }

        override fun onClick(v: View) {
        }

        companion object {
            //5
            private val ORDEN_KEY = "ORDEN"
        }

        fun bind(turno: Turno, setAbierto: (Boolean) -> Unit, getAbierto: () -> Boolean) {

            var titulo: TextView = view.findViewById(R.id.titulo)
            var descripcion: TextView = view.findViewById(R.id.descripcion)
            var fecha: TextView = view.findViewById(R.id.fecha)
            var hora: TextView = view.findViewById(R.id.hora)
            var icono: ImageView = view.findViewById(R.id.icono)

            if (turno.medicamentos.size == 1) {
                titulo.text = turno.medicamentos.get(0).nombre
            }
            else {

                if(!turno.medicamentos.isEmpty())
                    titulo.text = turno. medicamentos.get(0).nombre+"+"+(turno.medicamentos.size-1)
                else
                    titulo.text = "test!"
            }

            descripcion.text = turno.farmacia.nombre

            var formatter  = SimpleDateFormat("dd/MM/yyyy")
            fecha.text = formatter.format(turno.fechaAgendada.time)
            formatter  = SimpleDateFormat("hh:mm aa")
            hora.text = formatter.format(turno.fechaAgendada.time)

            var drawable:Drawable? = null;
            drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_turno_24dp)!!
            val wrappedDrawable = DrawableCompat.wrap(drawable)
            icono.setImageDrawable(drawable);

            view.setOnClickListener {
                if(!getAbierto()) {
                    setAbierto(true)
                    val fm = (view.context as AppCompatActivity).supportFragmentManager
                    val turnosDetail = TurnoDetailDialogFragment(setAbierto, turno)
                    turnosDetail.show(fm, "")
                }
            }
        }
    }
}

