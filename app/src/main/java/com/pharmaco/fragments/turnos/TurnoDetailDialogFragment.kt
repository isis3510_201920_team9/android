package com.pharmaco.fragments.turnos

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.R
import com.pharmaco.fragments.medicamentos.adapters.MedicamentosPedidosListAdapter
import com.pharmaco.models.Turno
import com.pharmaco.viewmodelfactories.turno.TurnoDetailViewModelFactory
import java.text.SimpleDateFormat
import androidx.lifecycle.Observer
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.storage.FirebaseStorage
import com.pharmaco.viewModels.turnos.TurnoDetailViewModel
import net.glxn.qrgen.android.QRCode


class TurnoDetailDialogFragment(var setAbierto: (Boolean) -> Unit, val turno: Turno): AppCompatDialogFragment() {


    private  lateinit var storage: FirebaseStorage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val inflater: LayoutInflater = LayoutInflater.from(this.requireContext())
        var builder: AlertDialog.Builder = AlertDialog.Builder(this.requireContext())
        val view: View = inflater.inflate(R.layout.dialog_fragment_turno_domicilio_detail, null)

        val fecha: TextView = view.findViewById<TextView>(R.id.fecha)
        val farmacia: TextView = view.findViewById<TextView>(R.id.farmacia)
        val recordatorio: TextView = view.findViewById<TextView>(R.id.recordatorio)
        var listView: RecyclerView = view.findViewById<RecyclerView>(R.id.listaDetailTurno)
        val QR = view.findViewById<ImageView>(R.id.QR)

        //val args: Bundle? = getArguments()
        //val turno = args?.getSerializable("turno")!! as Turno


        /**
        GlideApp.with(this!!)
        .load(gsReference.downloadUrl)
        .into(QR)*/

        /**
        var bitmap = loadBitmap(turno.id+".jpg")
        Log.d("RUTA", activity!!.getFileStreamPath(turno.id+".jpg").toString())

        if(bitmap != null) {
            QR.setImageBitmap(bitmap)
        }
        else{
            var storageRef: StorageReference  = storage.getReferenceFromUrl("gs://pharmaco-68bb9.appspot.com/QRs/turnos")
            var fileRef: StorageReference = storageRef.child(turno.id+".jpg")
            val ONE_MEGABYTE: Long = 1024 * 1024
            fileRef.getBytes(ONE_MEGABYTE).addOnSuccessListener {
                val bitmap = BitmapFactory.decodeByteArray(it, 0, it.size)
                QR.setImageBitmap(bitmap)

            }.addOnFailureListener {
                // Handle any errors
            }
        }*/


        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("FIREBASE ERROR", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                val token = task.result?.token
                var url = "https://us-central1-pharmaco-68bb9.cloudfunctions.net/qrTurno?turno="+turno.id+"&email="+ FirebaseAuth.getInstance().currentUser!!.email+"&token="+token
                val bitmap = QRCode.from(url).withSize(1000, 1000).bitmap()
                QR.setImageBitmap(bitmap)
            })

        var list = turno.medicamentos
        listView.adapter = MedicamentosPedidosListAdapter(list);
        listView.layoutManager = LinearLayoutManager(this.requireContext())


        builder.setView(view).setTitle("Turno").setNegativeButton("Aceptar", {dialog, which->

        })


        //ViewModel
        val vm = ViewModelProviders.of(this,
            TurnoDetailViewModelFactory(turno)
        )[TurnoDetailViewModel::class.java]

        vm.getTurno().observe(this, Observer<Turno>{ turno ->

            val formatter  = SimpleDateFormat("h:mm aa - dd/MM/yyyy")
            fecha.text = formatter.format(turno.fechaAgendada.time)
            farmacia.text = turno.farmacia.nombre
            recordatorio.text = "No olvides que para que te entreguen tus medicamentos debes presentar el siguiente QR en la farmacia"

            listView.layoutManager = LinearLayoutManager(this.requireContext())

        })

        return builder.create()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        setAbierto(false)
    }
}