package com.pharmaco.fragments.eventos

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.R
import com.pharmaco.fragments.turnos.adapters.TurnosListAdapter
import com.pharmaco.models.*
import java.util.*

class EventosListDialogFragment: DialogFragment(){

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.activity_eventos_list, container, false)

        val tituloView = view.findViewById<TextView>(R.id.tituloEventosList)
        val cerrar = view.findViewById<ImageView>(R.id.iconoCerrarEventosList)
        val listView: RecyclerView = view.findViewById<RecyclerView>(R.id.listaEventos2)

        val args: Bundle? = getArguments()
        val tipo = args?.getString("tipo")!!

        if(tipo == "Turnos"){

            var medicamentos = mutableListOf<MedicamentoTSD>()
            medicamentos.add(MedicamentoTSD(nombre = "Dolex"))
            medicamentos.add(MedicamentoTSD(nombre ="Tiroxin 100"))

            var list = mutableListOf<Turno>()

            list.add(Turno(farmacia = FarmaciaTSD("Cruz Verde 106"), medicamentos = medicamentos,fechaAgendada =  Date("2/10/2019")))


            //TODO, descomentar esta línea y arreglar.
            //listView.adapter = TurnosListAdapter(list);
            listView.layoutManager = LinearLayoutManager(this.requireContext())
        }/**
        else if(tipo == "Domicilios"){

            var medicamentoOrdens = mutableListOf<MedicamentoOrden>()
            medicamentoOrdens.add(MedicamentoOrden("Dolex", 5 ))
            medicamentoOrdens.add(MedicamentoOrden("Tiroxin 100", 5 ))

            var list = mutableListOf<Domicilio>()

            list.add(Domicilio("Cra 11B No 199-36", Date("2/10/2019"), medicamentoOrdens))

            listView.adapter = DomiciliosListAdapter(this.requireContext(), R.layout.fragment_list_description_row, list);
        }
        else if(tipo == "Suscripciones"){

            var list = mutableListOf<Suscripcion>()
            list.add(Suscripcion("Cruz Verde de la 106", "Martes", Date("2/10/2019")))
            list.add(Suscripcion("Cruz Verde de la 119", "Miercoles", Date("2/10/2019")))
            listView.adapter = SuscripcionesListAdapter(this.requireContext(), R.layout.fragment_list_description_row, list);
        }*/

        tituloView.text = tipo

        cerrar.setOnClickListener {
            //como accedo al dialogo anterior desde aca para cerrarlo
            dismiss()
        }

        return view
    }

    override fun onStart() {
        super.onStart()

        var dialog:  Dialog = requireDialog()

        if(dialog != null){

            var width = ViewGroup.LayoutParams.MATCH_PARENT
            var height = ViewGroup.LayoutParams.MATCH_PARENT

            dialog.window!!.setLayout(width, height)
            dialog.window!!.setWindowAnimations(R.style.enterRight)
        }
    }
}