package com.pharmaco.fragments.eventos

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.pharmaco.R
import com.pharmaco.broadcastReceivers.ConnectivityReceiver
import com.pharmaco.fragments.eventos.adapters.EventosListAdapter
import com.pharmaco.models.Evento
import com.pharmaco.models.Notificacion
import com.pharmaco.viewModels.notificaciones.NotificacionesViewModel
import com.pharmaco.viewModels.ordenes.OrdenesViewModel
import java.util.*


class EventosFragment : Fragment(), ConnectivityReceiver.ConnectivityReceiverListener {

    private var mSnackBar: Snackbar? = null
    private var rootlayout: CoordinatorLayout? = null

    private lateinit var con_rec: ConnectivityReceiver

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout. fragment_banner_eventos, container, false)

        var listView = view.findViewById<RecyclerView>(R.id.listaBanner)
        rootlayout = view.findViewById<CoordinatorLayout>(R.id.coordinator)

        var list = mutableListOf<Evento>()
        list.add(Evento("Turnos"))
        list.add(Evento("Domicilios"))
        list.add(Evento("Suscripciones"))

        listView.adapter = EventosListAdapter(list);
        listView.layoutManager = LinearLayoutManager(this.requireContext())

        con_rec = ConnectivityReceiver()

        this.activity!!.registerReceiver(
            con_rec,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )

        return view
    }

    

    private fun showMessage(isConnected: Boolean) {



        if (!isConnected) {

            mSnackBar = Snackbar.make(this.view!!, R.string.no_internet, Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            mSnackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            mSnackBar?.show()
        } else {
            mSnackBar?.dismiss()
        }


    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    /**
     * Callback will be called when there is change
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showMessage(isConnected)
    }

    override fun onDestroy() {
        super.onDestroy()
        this.activity!!.unregisterReceiver(con_rec)
    }
}
