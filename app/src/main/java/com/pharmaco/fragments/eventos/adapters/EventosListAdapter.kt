package com.pharmaco.fragments.eventos.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.pharmaco.models.Evento
import com.pharmaco.R
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.marginTop
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.fragments.eventos.EventosFragmentDirections
import com.pharmaco.fragments.notificaciones.adapters.NotificacionesListAdapter
import com.pharmaco.fragments.ordenes.OrdenesFragmentDirections


class EventosListAdapter (private val items: MutableList<Evento>): RecyclerView.Adapter<EventosListAdapter.EventosListVH>(){
    /**override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater:LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(resources, null)

        var titulo: TextView = view.findViewById(R.id.titulo)
        var icono: ImageView = view.findViewById(R.id.icono)

        var notificacion: Notificacion = items[position]

        var drawable:Drawable? = null;

        if(notificacion.tipo == "Turnos") {

            titulo.text = "Proximos Turnos"
            drawable = ContextCompat.getDrawable(mCtx, R.drawable.ic_turno_24dp)!!
            val wrappedDrawable = DrawableCompat.wrap(drawable)
            DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(mCtx, R.color.colorPrimary))
        }
        else if(notificacion.tipo == "Domicilios") {

            titulo.text = "Proximos Domicilios"
            drawable = ContextCompat.getDrawable(mCtx, R.drawable.ic_domicilio_24dp)!!
            val wrappedDrawable = DrawableCompat.wrap(drawable)
            DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(mCtx, R.color.colorPrimaryDark))
        }
        else if(notificacion.tipo == "Suscripciones") {

            titulo.text = "Suscripciones Vigentes"
            drawable = ContextCompat.getDrawable(mCtx, R.drawable.ic_suscripcion_24dp)!!
            val wrappedDrawable = DrawableCompat.wrap(drawable)
            DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(mCtx, R.color.colorMorado))
        }

        icono.setImageDrawable(drawable);

        view.setOnClickListener(object: View.OnClickListener{

            override fun onClick(p0: View?) {
                val navController: NavController = view.findNavController()
                val action = EventosFragmentDirections.actionEventosFragmentToEventosListDialogFragment(notificacion.tipo)
                navController.navigate(action)
            }
        })

        return view

    }*/
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventosListVH {

        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.fragment_list_eventos_row, parent, false)

        return EventosListVH(inflatedView)

    }

    override fun onBindViewHolder(holder: EventosListVH, position: Int) {

        holder.bind(items[position])

    }

    override fun getItemCount(): Int = items.size

    fun addItem(evento: Evento) {

        items.add(evento)
        notifyItemInserted(items.size)
    }

    fun removeAt(position: Int) {

        items.removeAt(position)
        notifyItemRemoved(position)
    }


    class EventosListVH(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        var view: View = v
        var evento: Evento? = null

        init {
            v.setOnClickListener {
            }
        }

        override fun onClick(v: View) {
        }

        companion object {
            //5
            private val ORDEN_KEY = "ORDEN"
        }

        fun bind(evento: Evento) {

            var titulo: TextView = view.findViewById(R.id.titulo)
            var icono: ImageView = view.findViewById(R.id.icono)

            var drawable:Drawable? = null;

            if(evento.tipo == "Turnos") {

                titulo.text = "Próximos Turnos"
                drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_turno_24dp)!!
                val wrappedDrawable = DrawableCompat.wrap(drawable)
                DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(view.context, R.color.colorVerde))
            }
            else if(evento.tipo == "Domicilios") {

                titulo.text = "Próximos Domicilios"
                drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_domicilio_24dp)!!
                val wrappedDrawable = DrawableCompat.wrap(drawable)
                DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(view.context, R.color.colorPrimaryDark))
            }
            else if(evento.tipo == "Suscripciones") {

                titulo.text = "Suscripciones Vigentes"
                drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_suscripcion_24dp)!!
                val wrappedDrawable = DrawableCompat.wrap(drawable)
                DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(view.context, R.color.colorMorado))
            }

            icono.setImageDrawable(drawable);

            view.setOnClickListener(object: View.OnClickListener{

                override fun onClick(p0: View?) {

                    if(evento.tipo=="Turnos") {

                        val navController: NavController = view.findNavController()
                        val action = EventosFragmentDirections.actionEventosFragmentToEventosActivity(evento.tipo!!)
                        navController.navigate(action)
                    }
                    else if(evento.tipo=="Domicilios") {

                        val navController: NavController = view.findNavController()
                        val action = EventosFragmentDirections.actionEventosFragmentToDomiciliosActivity(evento.tipo!!)
                        navController.navigate(action)
                    }
                    else if(evento.tipo=="Suscripciones") {

                        val navController: NavController = view.findNavController()
                        val action = EventosFragmentDirections.actionEventosFragmentToSuscripcionesActivity(evento.tipo!!)
                        navController.navigate(action)
                    }
                }
            })
        }

    }
}

