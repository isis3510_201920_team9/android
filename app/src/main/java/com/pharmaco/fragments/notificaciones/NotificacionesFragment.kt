package com.pharmaco.fragments.notificaciones

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.pharmaco.R
import com.pharmaco.broadcastReceivers.ConnectivityReceiver
import com.pharmaco.fragments.notificaciones.adapters.NotificacionesListAdapter
import com.pharmaco.models.Notificacion
import com.pharmaco.viewModels.notificaciones.NotificacionesViewModel
import java.util.*


class NotificacionesFragment : Fragment(), ConnectivityReceiver.ConnectivityReceiverListener {

    private lateinit var viewModel: NotificacionesViewModel
    private var mSnackBar: Snackbar? = null
    private var rootlayout: CoordinatorLayout? = null

    private lateinit var con_rec: ConnectivityReceiver

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_banner, container, false)

        var listView = view.findViewById<RecyclerView>(R.id.listaBanner)
        var swipeRefresh = view.findViewById<SwipeRefreshLayout>(R.id.swipeContainer)
        var listaVacia = view.findViewById<TextView>(R.id.listaVacia)
        rootlayout = view.findViewById<CoordinatorLayout>(R.id.coordinator)
        listView.layoutManager = LinearLayoutManager(this.requireContext())

        con_rec = ConnectivityReceiver()

        this.activity!!.registerReceiver(
            con_rec,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )

        viewModel = ViewModelProviders.of(this)[NotificacionesViewModel::class.java]

        viewModel.notificaciones.observe(this, Observer<MutableList<Notificacion>>{ notificaciones ->

            listView.adapter = NotificacionesListAdapter(notificaciones)
            if(notificaciones.isEmpty()){
                listView.visibility = View.GONE
                listaVacia.visibility = View.VISIBLE
            }
        })

        swipeRefresh.setOnRefreshListener {
            viewModel.loadNotificaciones()
            swipeRefresh.isRefreshing = false
        }

        return view
    }

    private fun showMessage(isConnected: Boolean) {

        if (!isConnected) {

            mSnackBar = Snackbar.make(this.view!!, R.string.no_internet, Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            mSnackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            mSnackBar?.show()
        } else {
            mSnackBar?.dismiss()
        }


    }

    override fun onResume() {
        super.onResume()

        ConnectivityReceiver.connectivityReceiverListener = this
    }

    /**
     * Callback will be called when there is change
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showMessage(isConnected)
    }

    override fun onDestroy() {
        super.onDestroy()
        if(::con_rec.isInitialized) {
            this.activity!!.unregisterReceiver(con_rec)
        }
    }

}
