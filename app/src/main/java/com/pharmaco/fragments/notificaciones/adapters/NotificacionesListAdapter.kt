package com.pharmaco.fragments.notificaciones.adapters

import android.graphics.drawable.Drawable
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.pharmaco.models.Notificacion
import com.pharmaco.R
import androidx.core.graphics.drawable.DrawableCompat
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.fragments.notificaciones.NotificationDetailDialog
import java.text.SimpleDateFormat
import java.util.*


class NotificacionesListAdapter (private val items: MutableList<Notificacion>): RecyclerView.Adapter<NotificacionesListAdapter.NotificacionesListVH>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificacionesListVH {

        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.fragment_list_description_row, parent, false)

        return NotificacionesListVH(inflatedView)

    }

    override fun onBindViewHolder(holder: NotificacionesListVH, position: Int) {

        holder.bind(items[position])

    }

    override fun getItemCount(): Int = items.size

    fun addItem(notificacion: Notificacion) {

        items.add(notificacion)
        notifyItemInserted(items.size)
    }

    fun removeAt(position: Int) {

        items.removeAt(position)
        notifyItemRemoved(position)
    }


    class NotificacionesListVH(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        var view: View = v
        var notificacion: Notificacion? = null

        init {
            v.setOnClickListener {
            }
        }

        override fun onClick(v: View) {
        }

        companion object {
            //5
            private val ORDEN_KEY = "ORDEN"
        }

        fun bind(notificacion: Notificacion) {

            val titulo: TextView = view.findViewById(R.id.titulo)
            val descripcion: TextView = view.findViewById(R.id.descripcion)
            val fecha: TextView = view.findViewById(R.id.fecha)
            val tipo: ImageView = view.findViewById(R.id.icono)

            titulo.text = notificacion.titulo
            descripcion.text = notificacion.mensajeCorto

            if(DateUtils.isToday(notificacion.fecha!!.time)) {
                val formatter = SimpleDateFormat("h:mm aa")
                fecha.text = formatter.format(notificacion.fecha!!.time)
            }
            else {
                val formatter = SimpleDateFormat("dd/MM/yyyy")
                fecha.text = formatter.format(notificacion.fecha!!.time)
            }

            var drawable:Drawable? = null;

            if(notificacion.tipo == "turno") {

                drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_turno_24dp)!!
                val wrappedDrawable = DrawableCompat.wrap(drawable)
                DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(view.context, R.color.colorVerde))
            }
            else if(notificacion.tipo == "domicilio") {

                drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_domicilio_24dp)!!
                val wrappedDrawable = DrawableCompat.wrap(drawable)
                DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(view.context, R.color.colorPrimaryDark))
            }
            else if(notificacion.tipo == "suscripcion") {

                drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_suscripcion_24dp)!!
                val wrappedDrawable = DrawableCompat.wrap(drawable)
            }

            tipo.setImageDrawable(drawable);

            view.setOnClickListener {
                val alerta = NotificationDetailDialog(notificacion.titulo!!, notificacion.mensaje!!,false, {}, {})
                val fm = (view.context as AppCompatActivity).supportFragmentManager
                alerta.show(fm!!, "")
            }
        }

    }
}

