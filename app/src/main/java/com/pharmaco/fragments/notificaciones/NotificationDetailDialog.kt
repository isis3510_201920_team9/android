package com.pharmaco.fragments.notificaciones

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import com.pharmaco.R

class NotificationDetailDialog(val titulo: String, val texto: String, val cancelar: Boolean, val okCallBack: () -> Unit, val cancelCallBack: () -> Unit): AppCompatDialogFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val inflater: LayoutInflater = LayoutInflater.from(this.requireContext())
        var builder: AlertDialog.Builder = AlertDialog.Builder(this.requireContext())
        val view: View = inflater.inflate(R.layout.temp_notificacion_detail, null)

        val descripcion: TextView = view.findViewById<TextView>(R.id.descripcion)
        descripcion.text = texto
        builder.setView(view).setTitle(titulo).setPositiveButton("OK", {dialog, which->
            okCallBack()
        })

        if(cancelar){
            builder.setNegativeButton("Cancelar", {dialog, which->
                cancelCallBack()
            })
        }
        return builder.create()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        cancelCallBack()
    }
}