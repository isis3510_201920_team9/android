package com.pharmaco.fragments.perfil

import android.app.Dialog
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import android.widget.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.pharmaco.R
import com.pharmaco.broadcastReceivers.ConnectivityReceiver
import com.pharmaco.fragments.GenericAlertDialog
import com.pharmaco.viewModels.usuarios.UsuariosEditViewModel

class EditPerfilDialogFragment(val viewModel: UsuariosEditViewModel): DialogFragment(), ConnectivityReceiver.ConnectivityReceiverListener{

    private var mSnackBar: Snackbar? = null
    private var rootlayout: CoordinatorLayout? = null
    private lateinit var con_rec: ConnectivityReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.dialog_fragment_edit_perfil, container, false)

        val cerrar = view.findViewById<ImageView>(R.id.cerrar)
        var nombre = view.findViewById<EditText>(R.id.editarNombre)
        var eps = view.findViewById<EditText>(R.id.editarEPS)
        val enviar = view.findViewById<Button>(R.id.enviar)
        var edad = view.findViewById<EditText>(R.id.edad)

        val generos = arrayOf("Masculino", "Femenino", "Otro")
        val genero = view.findViewById<Spinner>(R.id.spinnerGenero)
        var selectedItem: String? = ""
        if (genero != null) {
            val arrayAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, generos)
            genero.adapter = arrayAdapter
            genero.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    selectedItem = generos[position]
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }


        val usuario = viewModel.usuario.value

        if(usuario != null){
            nombre.setText(usuario.nombre)
            eps.setText(usuario.eps)
            edad.setText(usuario.edad.toString())

            if(usuario.genero == "Masculino"){
                genero.setSelection(0)
            }
            else if(usuario.genero == "Femenino"){
                genero.setSelection(1)
            }
            else if(usuario.genero == "Otro"){
                genero.setSelection(2)
            }


        }

        rootlayout = view.findViewById<CoordinatorLayout>(R.id.coordinatorView) as CoordinatorLayout




        cerrar.setOnClickListener {
            //como accedo al dialogo anterior desde aca para cerrarlo
            dismiss()
        }

        enviar.setOnClickListener{


            if(eps.text.toString() != "" && nombre.text.toString() != "" && edad.text.toString() != "") {

                if (edad.text.toString().toInt() < 18) {
                    val alerta = GenericAlertDialog("Debe ser mayor de edad", false, {}, {})
                    val fm = fragmentManager
                    alerta.show(fm!!, "")

                    return@setOnClickListener
                }

                viewModel.editUsuario(nombre.text.toString(), eps.text.toString(),
                        edad.text.toString().toInt(), selectedItem!!)
            }
            else{
                val alerta = GenericAlertDialog("Por favor complete todos los campos",false, {}, {})
                val fm = fragmentManager
                alerta.show(fm!!, "")

                return@setOnClickListener
            }


            dismiss()
        }


        con_rec = ConnectivityReceiver()

        this.activity!!.registerReceiver(con_rec,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )

        return view
    }

    override fun onStart() {
        super.onStart()

        var dialog:  Dialog = requireDialog()

        if(dialog != null){

            var width = ViewGroup.LayoutParams.MATCH_PARENT
            var height = ViewGroup.LayoutParams.MATCH_PARENT

            dialog.window!!.setLayout(width, height)
            dialog.window!!.setWindowAnimations(R.style.AppTheme_Slide)
        }
    }

    private fun showMessage(isConnected: Boolean) {
        if (!isConnected) {

            mSnackBar = Snackbar.make(rootlayout as View, R.string.no_internet, Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            mSnackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            mSnackBar?.show()
        } else {
            mSnackBar?.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()

        ConnectivityReceiver.connectivityReceiverListener = this
    }

    /**
     * Callback will be called when there is change
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showMessage(isConnected)
    }

    override fun onDestroy() {
        super.onDestroy()
        this.activity!!.unregisterReceiver(con_rec)
    }
}