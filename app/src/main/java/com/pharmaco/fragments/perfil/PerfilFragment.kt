package com.pharmaco.fragments.perfil

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.RuntimeExecutionException
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.pharmaco.R
import com.pharmaco.activities.MainActivity
import com.pharmaco.broadcastReceivers.ConnectivityReceiver
import com.pharmaco.fragments.GenericAlertDialog
import com.pharmaco.models.Usuario
import com.pharmaco.viewModels.usuarios.UsuariosEditViewModel
import android.app.AlarmManager
import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.media.RingtoneManager
import android.util.Log
import androidx.core.app.NotificationCompat
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.pharmaco.broadcastReceivers.NotificationPublisher
import com.pharmaco.repo.usuarios.UsuariosRepo
import com.pharmaco.viewModels.Routes
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.util.*


class PerfilFragment: Fragment(), ConnectivityReceiver.ConnectivityReceiverListener {

    private var mSnackBar: Snackbar? = null
    private lateinit var con_rec: ConnectivityReceiver
    private lateinit var auth: FirebaseAuth

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        val view: View = inflater.inflate(R.layout.fragment_mi_perfil, container, false)

        auth = FirebaseAuth.getInstance()

        var editar = view.findViewById<Button>(R.id.editar)
        var cambiarContraseña = view.findViewById<Button>(R.id.cambiarContraseña)
        var cerrarSesion = view.findViewById<Button>(R.id.cerrarSesion)

        val nombre = view.findViewById<TextView>(R.id.NombreUsuario)
        val eps = view.findViewById<TextView>(R.id.NombreEPS)
        val cedula = view.findViewById<TextView>(R.id.NumeroCedula)
        val genero = view.findViewById<TextView>(R.id.genero)
        val edad = view.findViewById<TextView>(R.id.edad)

        val viewModel = ViewModelProviders.of(this)[UsuariosEditViewModel::class.java]

        viewModel.usuario.observe(this, Observer<Usuario> {usuario ->

            nombre.text = usuario.nombre
            eps.text = usuario.eps
            cedula.text = usuario.numDocumento.toString()
            edad.text = usuario.edad.toString()
            genero.text = usuario.genero

        })

        editar.setOnClickListener {
            val fm = fragmentManager
            val dialog = EditPerfilDialogFragment(viewModel)
            dialog.show(fm!!, "")
        }

        cambiarContraseña.setOnClickListener {
            auth.sendPasswordResetEmail(auth.currentUser!!.email.toString()).addOnCompleteListener(activity as Activity, OnCompleteListener { task ->
                try {
                    val result = task.result
                    val alerta = GenericAlertDialog("Se ha enviado un link a tu correo para que restaures tu cuenta.",false, {}, {})
                    val fm = fragmentManager
                    alerta.show(fm!!, "")
                } catch(e: RuntimeExecutionException){

                    var partes =  e.message!!.split(": ")
                    var mensaje = partes[1]
                    if(partes[1]=="An internal error has occurred. [ 7") {
                        mensaje = "Lo sentimos, debes estar conectado a internet para poder reestablecer tu contraseña."
                    }
                    val alerta = GenericAlertDialog(mensaje,false, {}, {})
                    val fm = fragmentManager
                    alerta.show(fm!!, "")
                }
            })
        }

        cerrarSesion.setOnClickListener {

            /**
            GlobalScope.launch {
                UsuariosRepo.setInactive()
                Routes.clearUserID()
                auth.signOut()
                val intent = Intent(requireContext(), MainActivity::class.java)
                startActivity(intent)
                activity!!.finish()
            }*/

            cerrarSesion.isEnabled = false
            val queue = Volley.newRequestQueue(requireContext())
            val url = "https://us-central1-pharmaco-68bb9.cloudfunctions.net/cerrarSesion?userId="+Routes.USER_ID

            val stringRequest = StringRequest(url,
                Response.Listener { response ->
                    Log.d("INFO", "cerro sesion correctamente")
                    Routes.clearUserID()
                    auth.signOut()
                    val intent = Intent(requireContext(), MainActivity::class.java)
                    startActivity(intent)
                    activity!!.finish()
                },
                Response.ErrorListener {
                    cerrarSesion.isEnabled = true
                    val alerta = GenericAlertDialog("Lo sentimos, hubo un error al cerra sesión. Revisa tu conexion a internet, o inténtalo más tarde.", false, {}, {})
                    val fm = fragmentManager
                    alerta.show(fm!!, "")
                }
            )
            queue.add(stringRequest)
        }

        con_rec = ConnectivityReceiver()
        this.activity!!.registerReceiver(con_rec,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
        return view
    }

    private fun showMessage(isConnected: Boolean) {
        if (!isConnected) {

            mSnackBar = Snackbar.make(this.view!!, R.string.no_internet, Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            mSnackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            mSnackBar?.show()
        } else {
            mSnackBar?.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    /**
     * Callback will be called when there is change
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showMessage(isConnected)
    }

    override fun onDestroy() {
        super.onDestroy()
        this.activity!!.unregisterReceiver(con_rec)
    }
}