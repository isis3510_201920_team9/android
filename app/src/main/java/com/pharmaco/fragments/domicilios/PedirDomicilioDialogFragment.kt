package com.pharmaco.fragments.domicilios

import android.app.*
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import android.content.DialogInterface
import android.content.Intent
import android.widget.*
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.media.RingtoneManager
import android.net.ConnectivityManager
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.pharmaco.R
import com.pharmaco.broadcastReceivers.ConnectivityReceiver
import com.pharmaco.broadcastReceivers.NotificationPublisher
import com.pharmaco.fragments.GenericAlertDialog
import com.pharmaco.fragments.SwipeLeftToDeleteCallback
import com.pharmaco.fragments.MapFragment
import com.pharmaco.fragments.medicamentos.adapters.MedicamentosAPedirListAdapter
import com.pharmaco.fragments.ordenes.OrdenesModalFragment
import com.pharmaco.models.*
import com.pharmaco.viewModels.domicilios.DomicilioCreateViewModel
import java.text.SimpleDateFormat
import java.util.*

@Suppress("DEPRECATION")
class PedirDomicilioDialogFragment(var setAbierto: () -> Unit, var orden: Orden?, var closeParent: () -> Unit): DialogFragment(), ConnectivityReceiver.ConnectivityReceiverListener{
    private  val LOCATION_PERMISSION_REQUEST_CODE = 1
    private var mSnackBar: Snackbar? = null
    private var rootlayout: CoordinatorLayout? = null

    private lateinit var medicamentosList: MutableList<MedicamentoCreate>
    private lateinit var farmaciasList: MutableList<FarmaciaTSD>

    private lateinit var viewModel: DomicilioCreateViewModel
    private var mapOpened: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.dialog_fragment_pedir_domicilio, container, false)

        val titulo = view.findViewById<TextView>(R.id.descripcionPedirTurno)
        val direccion = view.findViewById<EditText>(R.id.direccion)
        val farmacia = view.findViewById<EditText>(R.id.ubicacionPedirTurno)
        val dateText = view.findViewById<EditText>(R.id.fechaPedirTurno)
        val horaText = view.findViewById<EditText>(R.id.horaPedirTurno)
        val cerrar = view.findViewById<ImageView>(R.id.iconoCerrarPedirTurno)
        val agregar = view.findViewById<Button>(R.id.botonAgregarPedirTurno)
        val enviar = view.findViewById<Button>(R.id.enviar)
        rootlayout = view.findViewById<CoordinatorLayout>(R.id.coordinatorView) as CoordinatorLayout

        var fechaEscogida = dateText.text.toString()
        var horaEscogida = horaText.text.toString()

        titulo.text = "Pedir Domicilio"
        farmacia.setText("Escoger Farmacia")

        var listView = view.findViewById<RecyclerView>(R.id.listPedirTurno)


        //val args: Bundle? = getArguments()
        //val orden = args?.getSerializable("orden") as Orden?


        //listView.adapter = MedicamentosAPedirListAdapter(list);

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)

        val dpd = DatePickerDialog(this.requireContext(), DatePickerDialog.OnDateSetListener { view, pYear, monthOfYear, dayOfMonth ->
            dateText.setText(dayOfMonth.toString()+"/"+(monthOfYear+1).toString()+"/"+pYear.toString())
            fechaEscogida = dayOfMonth.toString()+"/"+(monthOfYear+1).toString()+"/"+pYear.toString()

        }, year, month, day)

        dpd.datePicker.minDate = System.currentTimeMillis();

        val tpd = TimePickerDialog(this.requireContext(), TimePickerDialog.OnTimeSetListener { view, hour, minute ->

            val cal = Calendar.getInstance()
            val currentYear = cal.get(Calendar.YEAR)
            val currentMonth = cal.get(Calendar.MONTH)
            val currentDay = cal.get(Calendar.DAY_OF_MONTH)
            val currentHour = cal.get(Calendar.HOUR_OF_DAY)
            val currentMinute = cal.get(Calendar.MINUTE)

            if(fechaEscogida != "Ingresar Fecha"){

                var partes = fechaEscogida.split("/")
                var diaEscogido = partes[0].toInt()
                var mesEscogido = partes[1].toInt()-1
                var añoEscogido = partes[2].toInt()

                if( diaEscogido == currentDay && mesEscogido == currentMonth && añoEscogido == currentYear){

                    if(hour <= currentHour){
                        val alerta = GenericAlertDialog("Lo sentimos, debe agendar su domicilio, como mínimo, una hora en el futuro.", false, {}, {})
                        val fm = fragmentManager
                        alerta.show(fm!!, "")
                        return@OnTimeSetListener
                    }
                }
            }

            var date = Date()
            date.hours = hour
            date.minutes = minute
            var formatter = SimpleDateFormat("h:mm aa")
            horaText.setText(formatter.format(date.time))
            horaEscogida = formatter.format(date.time)


        }, hour, minute, false)


        farmacia.setOnClickListener {
            openMap()
        }

        dateText.setOnClickListener {
            dpd.show()
        }

        horaText.setOnClickListener {
            if(fechaEscogida==""){
                val alerta = GenericAlertDialog("Escoge una fecha primero.", false, {}, {})
                val fm = fragmentManager
                alerta.show(fm!!, "")
            }
            else {
                tpd.show()
            }
        }

        agregar.setOnClickListener {
            openMedicamentosList()
        }

        val swipeLeft = object : SwipeLeftToDeleteCallback(this.requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                viewModel.deleteMedicamentoSeleccionado(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeLeft)
        itemTouchHelper.attachToRecyclerView(listView)

        cerrar.setOnClickListener {
            var controller = findNavController()
            controller.popBackStack(R.id.ordenDetailDialogFragment, true)
            dismiss()
        }

        fun createDomicilio(){


            val horaArr = horaEscogida.split(":")
            val fechaArr = fechaEscogida.split("/")


            val dateAgenda = Date()
            dateAgenda.hours = horaArr[0].toInt()
            dateAgenda.minutes = horaArr[1].split(" ")[0].toInt()

            if(horaArr[1].split(" ")[1] == "PM"){
                dateAgenda.hours = dateAgenda.hours + 12
            }

            dateAgenda.date = fechaArr[0].toInt()
            dateAgenda.month = fechaArr[1].toInt() - 1
            dateAgenda.year = fechaArr[2].toInt() - 1900

            val direccion = direccion.text.toString()
            viewModel.createDomicilio(direccion, dateAgenda)
            var formatter  = SimpleDateFormat("hh:mm aa")
            var mensajeLargo = "Recuerda que hoy tienes un domicilio agendado para " + direccion + " a las " + formatter.format(dateAgenda) + ". Te deben entregar " + viewModel.getMedicamentosSeleccionadosList()!!.size + " medicamentos."
            scheduleNotification(c, mensajeLargo)

            closeParent()
            dismiss()
            createNotificationNow(direccion, "Pediste un domicilio para la " + direccion + " a las " + formatter.format(dateAgenda) + ". Te deben entregar " + viewModel.getMedicamentosSeleccionadosList()!!.size + " medicamentos.")

        }

        enviar.setOnClickListener{
            //ACA PEZ
            //use las varianles ubicacionEscogida, fechaEscogida, horaEscogida y list (lista  de MedicamentoOrden) para saber lo que escogio el usuario

            if(direccion.text.toString() == "" || farmacia.text.toString() == "Escoger Farmacia" || horaEscogida == ""
                || fechaEscogida == "" || viewModel.getMedicamentosSeleccionadosList() == null
                || viewModel.getMedicamentosSeleccionadosList()!!.size == 0  ){
                val alerta = GenericAlertDialog("Debe completar todos los campos para poder agendar su domicilio", false, {}, {})
                val fm = fragmentManager
                alerta.show(fm!!, "")
                return@setOnClickListener
            }
            val list = viewModel.getMedicamentosSeleccionadosList()
            if(list != null) {
                for (medSelect in list) {

                    if(medSelect.unidadesPedidas == 0){
                        val alerta = GenericAlertDialog("Por favor especifique la cantidad de cada medicamento", false, {}, {})
                        alerta.show(fragmentManager!!, "")
                        return@setOnClickListener
                    }

                }
            }

            //Revisar si hay o no internet
            if(!viewModel.verifyAvailableNetwork(this.activity!!)){


                //si no hay internet, generar alerta.
                val alerta = GenericAlertDialog("No hay internet en este momento. " +
                        "Intentaremos sincronizar tus datos cuando recuperemos la conexión. ¿Deseas continuar?"
                    , true, {
                    createDomicilio()
                }, {})
                val fm = fragmentManager
                alerta.show(fm!!, "")
                return@setOnClickListener
            }
            else{
                createDomicilio()
            }

        }

        //ViewModel
        viewModel = ViewModelProviders.of(this)[DomicilioCreateViewModel::class.java]

        if(viewModel.orden == null){

            if(orden != null){

                val auxList = mutableListOf<MedicamentoTSDCreate>()

                for(frec in orden!!.frecuencias){
                    auxList.add(frec.toMedicamentoTSDCreate(orden!!.reference!!))
                }

                viewModel.setMedicamentosSeleccionados(auxList)
                viewModel.orden = orden

            }
        }

        viewModel.getMedicamentos().observe(this, Observer<MutableList<MedicamentoCreate>>{medicamentos ->

            medicamentosList = medicamentos

            if(!agregar.isEnabled && !medicamentosList.isEmpty()){
                agregar.isEnabled = true
            }

        })

        viewModel.getMedicamentosSeleccionados().observe(this, Observer<MutableList<MedicamentoTSDCreate>> { medicamentos ->

            listView.adapter = MedicamentosAPedirListAdapter(viewModel, medicamentos, fragmentManager!!)
            listView.layoutManager = LinearLayoutManager(this.requireContext())

        })

        viewModel.getFarmaciasDisponibles().observe(this, Observer<MutableList<FarmaciaTSD>> {farmacias ->

            farmaciasList = farmacias

            if(!farmacia.isEnabled && !farmaciasList.isEmpty()){
                farmacia.isEnabled = true
            }
        })

        viewModel.selectedFarmacia.observe(this, Observer<FarmaciaTSD>{pfarmacia ->
            farmacia.setText(pfarmacia.nombre)

        })

        this.activity!!.registerReceiver(ConnectivityReceiver(),
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )

        return view
    }

    override fun onStart() {
        super.onStart()

        var dialog:  Dialog = requireDialog()

        if(dialog != null){

            var width = ViewGroup.LayoutParams.MATCH_PARENT
            var height = ViewGroup.LayoutParams.MATCH_PARENT

            dialog.window!!.setLayout(width, height)
            dialog.window!!.setWindowAnimations(R.style.AppTheme_Slide)
        }
    }
    fun openMedicamentosList() {
        //El Dialogo del detalle
        val fm = fragmentManager
        val medicamentosList = OrdenesModalFragment(list = medicamentosList, viewModel = viewModel)
        medicamentosList.show(fm!!, "")
    }
    fun openMap() {

        if(!mapOpened) {

            if (ActivityCompat.checkSelfPermission(
                    this.requireContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    LOCATION_PERMISSION_REQUEST_CODE
                )
                return
            }

            if (::farmaciasList.isInitialized) {
                val fm = fragmentManager
                val medicamentosList = MapFragment(farmaciasList, { farmacia ->
                    viewModel.setSelected(farmacia)
                }, {
                    mapOpened = false
                })
                medicamentosList.show(fm!!, "")
                mapOpened = true
            }
            else {
                val alerta = GenericAlertDialog("Por favor espera un momento mientras obtenemos las farmacias",false, {}, {})
                val fm = fragmentManager
                alerta.show(fm!!, "")
            }
        }
    }

    private fun showMessage(isConnected: Boolean) {
        if (!isConnected) {

            mSnackBar = Snackbar.make(rootlayout as View, R.string.no_internet, Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            mSnackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            mSnackBar?.show()
        } else {
            mSnackBar?.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()

        ConnectivityReceiver.connectivityReceiverListener = this
    }

    /**
     * Callback will be called when there is change
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showMessage(isConnected)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        setAbierto()
    }

    private fun scheduleNotification(fechaEvento: Calendar, mensajeLargo: String) {

        val notificationIntent = Intent(requireContext(), NotificationPublisher::class.java)

        val notification = NotificationCompat.Builder(requireContext(), "ForegroundService Kotlin")
            .setContentTitle("Recordatorio")
            .setContentText("Recuerda que hoy tienes un domicilio agendado")
            .setSmallIcon(R.drawable.ic_logo_blanco)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .build()
        notificationIntent.putExtra("tipo", "domicilio")
        notificationIntent.putExtra("mensajeLargo", mensajeLargo)
        notificationIntent.putExtra("mensaje", "Recuerda que hoy tienes un domicilio agendado")
        notificationIntent.putExtra("notification-id", 1)
        notificationIntent.putExtra("notification", notification)

        val pendingIntent = PendingIntent.getBroadcast(requireContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val alarmManager = context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
        alarmManager!!.set(AlarmManager.RTC_WAKEUP, fechaEvento.timeInMillis-3600000, pendingIntent)
    }

    private fun createNotificationNow(direccion: String, mensajeLargo: String) {

        val notificationIntent = Intent(requireContext(), NotificationPublisher::class.java)

        val notification = NotificationCompat.Builder(requireContext(), "ForegroundService Kotlin")
            .setContentTitle("Domicilio Agendado")
            .setContentText("Pediste un domicilio para la "+direccion)
            .setSmallIcon(R.drawable.ic_logo_blanco)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .build()
        notificationIntent.putExtra("titulo", "Domicilio Agendado")
        notificationIntent.putExtra("tipo", "domicilio")
        notificationIntent.putExtra("mensajeLargo", mensajeLargo)
        notificationIntent.putExtra("mensaje", "Pediste un domicilio para la "+direccion)
        notificationIntent.putExtra("notification-id", 1)
        notificationIntent.putExtra("notification", notification)

        val pendingIntent = PendingIntent.getBroadcast(requireContext(), 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val alarmManager = context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
        alarmManager!!.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+1000, pendingIntent)

    }
}