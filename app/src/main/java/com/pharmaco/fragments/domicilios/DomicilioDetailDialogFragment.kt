package com.pharmaco.fragments.domicilios

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.R
import com.pharmaco.fragments.medicamentos.adapters.MedicamentosPedidosListAdapter
import com.pharmaco.models.Domicilio
import com.pharmaco.viewmodelfactories.domicilio.DomicilioDetailViewModelFactory
import java.text.SimpleDateFormat
import androidx.lifecycle.Observer
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.iid.FirebaseInstanceId
import com.pharmaco.viewModels.domicilios.DomicilioDetailViewModel
import net.glxn.qrgen.android.QRCode


class DomicilioDetailDialogFragment(var setAbierto: (Boolean) -> Unit, var domicilio: Domicilio): AppCompatDialogFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val inflater: LayoutInflater = LayoutInflater.from(this.requireContext())
        var builder: AlertDialog.Builder = AlertDialog.Builder(this.requireContext())
        val view: View = inflater.inflate(R.layout.dialog_fragment_turno_domicilio_detail, null)

        val fecha: TextView = view.findViewById<TextView>(R.id.fecha)
        val farmacia: TextView = view.findViewById<TextView>(R.id.farmacia)
        val recordatorio: TextView = view.findViewById<TextView>(R.id.recordatorio)
        var listView = view.findViewById<RecyclerView>(R.id.listaDetailTurno)
        val QR = view.findViewById<ImageView>(R.id.QR)

        //val args: Bundle? = getArguments()
        //val domicilio = args?.getSerializable("domicilio")!! as Domicilio

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("FIREBASE ERROR", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }
                val token = task.result?.token
                var url = "https://us-central1-pharmaco-68bb9.cloudfunctions.net/qrDomicilio?domicilio="+domicilio.id+"&email="+ FirebaseAuth.getInstance().currentUser!!.email+"&token="+token
                val bitmap = QRCode.from(url).withSize(1000, 1000).bitmap()
                QR.setImageBitmap(bitmap)
            })

        builder.setView(view).setTitle("Domicilio").setNegativeButton("Aceptar", {dialog, which->

        })

        //ViewModel
        val vm = ViewModelProviders.of(this,
            DomicilioDetailViewModelFactory(domicilio)
        )[DomicilioDetailViewModel::class.java]

        vm.getDomicilio().observe(this, Observer<Domicilio>{ domicilio ->

            val formatter  = SimpleDateFormat("h:mm aa - dd/MM/yyyy")
            fecha.text = formatter.format(domicilio.fechaAgendada.time)
            farmacia.text = domicilio.direccion
            recordatorio.text = "No olvides que para que te entreguen tus medicamentos debes presentar el siguiente QR al domiciliario"

            var list = domicilio.medicamentos
            listView.adapter = MedicamentosPedidosListAdapter(list)
            listView.layoutManager = LinearLayoutManager(this.requireContext())


        })

        return builder.create()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        setAbierto(false)
    }

}