package com.pharmaco.fragments.domicilios.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.pharmaco.R
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.pharmaco.fragments.domicilios.DomicilioDetailDialogFragment
import com.pharmaco.models.Domicilio
import android.os.Bundle
import java.text.SimpleDateFormat

class DomiciliosListAdapter (private val items: MutableList<Domicilio>, var setAbierto: (Boolean) -> Unit, val getAbierto: () -> Boolean): RecyclerView.Adapter<DomiciliosListAdapter.DomiciliosListVH>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DomiciliosListVH {

        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.fragment_eventos_list_description_row, parent, false)

        return DomiciliosListVH(inflatedView, parent.context)
    }

    override fun onBindViewHolder(holder: DomiciliosListVH, position: Int) {

        holder.bind(items[position], setAbierto, getAbierto)
    }

    override fun getItemCount(): Int = items.size

    fun addItem(domicilio: Domicilio) {

        items.add(domicilio)
        notifyItemInserted(items.size)
    }

    class DomiciliosListVH(v: View, ctx: Context) : RecyclerView.ViewHolder(v), View.OnClickListener {
        var view: View = v
        var context: Context = ctx
        var domicilio: Domicilio? = null

        init {
            v.setOnClickListener {
            }
        }

        override fun onClick(v: View) {
        }

        companion object {
            //5
            private val ORDEN_KEY = "ORDEN"
        }
        fun bind(domicilio: Domicilio, setAbierto: (Boolean) -> Unit, getAbierto: () -> Boolean) {

            var titulo: TextView = view.findViewById(R.id.titulo)
            var descripcion: TextView = view.findViewById(R.id.descripcion)
            var fecha: TextView = view.findViewById(R.id.fecha)
            var hora: TextView = view.findViewById(R.id.hora)
            var icono: ImageView = view.findViewById(R.id.icono)

            if (domicilio.medicamentos.size == 1) {
                titulo.text = domicilio.medicamentos.get(0).nombre
            }
            else {

                if(!domicilio.medicamentos.isEmpty())
                    titulo.text = domicilio.medicamentos.get(0).nombre+"+"+(domicilio.medicamentos.size-1)
                else{
                    titulo.text = "ERROR"
                }
            }

            descripcion.text = domicilio.direccion
            var formatter  = SimpleDateFormat("dd/MM/yyyy")
            fecha.text = formatter.format(domicilio.fechaAgendada.time)
            formatter  = SimpleDateFormat("hh:mm aa")
            hora.text = formatter.format(domicilio.fechaAgendada.time)

            var drawable:Drawable? = null;
            drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_domicilio_24dp)!!
            val wrappedDrawable = DrawableCompat.wrap(drawable)
            icono.setImageDrawable(drawable);

            view.setOnClickListener {
                if(!getAbierto()) {
                    setAbierto(true)
                    val fm = (view.context as AppCompatActivity).supportFragmentManager
                    val domiciliosDetail = DomicilioDetailDialogFragment(setAbierto, domicilio)
                    domiciliosDetail.show(fm, "")
                }
            }
        }
    }
}

