package com.pharmaco.auxiliary

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import java.io.Serializable
import java.util.*
import kotlin.collections.HashMap

data class CallbackWrapper(@Transient var callBack: ((Boolean) -> Unit)?=null): Serializable {


}