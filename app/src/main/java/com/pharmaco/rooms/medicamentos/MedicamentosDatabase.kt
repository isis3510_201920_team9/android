package com.pharmaco.rooms.medicamentos

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(entities = arrayOf(MedicamentoEntity::class), version = 1)
abstract class MedicamentosDatabase: RoomDatabase(){

    abstract fun medicamentoDao(): MedicamentoDao

    companion object {
        @JvmField
        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
            }
        }
    }

}
