package com.pharmaco.rooms.medicamentos

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "medicamentos")
data class MedicamentoEntity(

        @PrimaryKey(autoGenerate = true) val uid: Int,

        val nombre: String?,

        val laboratorio: String?,

        val principioActivo: String?

)