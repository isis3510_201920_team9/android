package com.pharmaco.rooms.medicamentos

import androidx.room.Dao
import androidx.room.Query

@Dao
interface MedicamentoDao{

    @Query("SELECT * FROM medicamentos WHERE medicamentos.nombre LIKE :nombre GROUP BY medicamentos.nombre")
    fun searchMedicamento(nombre: String): List<MedicamentoEntity>

    @Query("SELECT * FROM medicamentos WHERE medicamentos.principioActivo LIKE :principio GROUP BY medicamentos.nombre")
    fun searchPrincipio(principio: String): List<MedicamentoEntity>

    //Para debugging
    @Query("SELECT * FROM medicamentos LIMIT 30")
    fun getAll(): List<MedicamentoEntity>
}