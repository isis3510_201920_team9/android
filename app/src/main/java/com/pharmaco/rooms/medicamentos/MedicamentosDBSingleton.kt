package com.pharmaco.rooms.medicamentos

import android.content.Context
import androidx.room.Room

object MedicamentosDBSingleton{


    private var db: MedicamentosDatabase? = null

    //Esta función se debe llamar desde el Main Activity cuando la app se abre y la BD se ha migrado exitosamente
    fun setDB(pDb: MedicamentosDatabase){
        db = pDb
    }

    fun getInstance(): MedicamentosDatabase{

        if(db != null){
            return db!!
        }
        else{
            throw Exception("DB NOT INITIALIZED!")
        }

    }
}