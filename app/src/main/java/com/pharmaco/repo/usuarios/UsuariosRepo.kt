package com.pharmaco.repo.usuarios

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Source
import com.pharmaco.models.Usuario
import com.pharmaco.viewModels.Routes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object UsuariosRepo{

    private val TAG = this.javaClass.toString()

    suspend fun updateUsuario(uid: String, nombre: String, eps: String, edad: Int, genero: String) = withContext(Dispatchers.IO){
        updateUsuarioDB(uid, nombre, eps, edad, genero)
    }

    private fun updateUsuarioDB(uid: String, nombre: String, eps: String, edad: Int, genero: String){

        if(uid != null) {
            val db = FirebaseFirestore.getInstance()

            db.collection(Routes.CLIENTES_ROUTE).document(uid)
                    .update("nombre", nombre, "eps", eps, "edad", edad, "genero", genero)
                    .addOnSuccessListener {

                        Log.d(TAG, "Documento actualizado")

                    }
                    .addOnFailureListener { e ->
                        Log.w(TAG, "Error actualizando el documento", e)
                    }
        }
    }

    suspend fun loadUsuario(uid: String, usuarioLD: MutableLiveData<Usuario>) =
            withContext(Dispatchers.IO){
                loadUsuarioDB(uid, usuarioLD, Source.CACHE)
                loadUsuarioDB(uid, usuarioLD, Source.SERVER)
            }

    private fun loadUsuarioDB(uid: String, usuarioLD: MutableLiveData<Usuario>, source: Source){
        val db = FirebaseFirestore.getInstance()

        Log.d(TAG, "STARTING LOAD")
        db.collection(Routes.CLIENTES_ROUTE).document(uid).get(source)
                .addOnSuccessListener { doc ->
                    if(doc.exists()){
                        usuarioLD.value = doc.toObject(Usuario::class.java)
                        Log.d(TAG, usuarioLD.value.toString())
                    }
                    else{
                        Log.d(TAG, "DOC DOES NOT EXIST")
                    }
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error recuperando el documento", e)
                }
    }

    suspend fun setActive() = withContext(Dispatchers.IO){

        val db = FirebaseFirestore.getInstance()

        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .update("sesionActiva", true, "plataforma", "Android").addOnSuccessListener {
                    Log.d(TAG, "Usuario registrado como activo en BD")
                }
                .addOnFailureListener { e->
                    Log.w(TAG, "Error registrando sesión activa", e)
                }
    }

    suspend fun setInactive() = withContext(Dispatchers.IO){

        val db = FirebaseFirestore.getInstance()

        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .update("sesionActiva", false).addOnSuccessListener {
                    Log.d(TAG, "Usuario registrado como inactivo en BD")
                }
                .addOnFailureListener { e->
                    Log.w(TAG, "Error registrando sesión inactiva", e)
                }

    }

    suspend fun createCollection(usuario: Usuario) = withContext(Dispatchers.IO){

        val db = FirebaseFirestore.getInstance()

        db.collection(Routes.CLIENTES_ROUTE).document(usuario.uid!!).set(usuario.toHashMap())
                .addOnSuccessListener {
                    Log.d(TAG, "CREADO USUARIO")
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error creando el documento", e)
                }


    }

}