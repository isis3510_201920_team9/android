package com.pharmaco.repo.farmacias

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Source
import com.pharmaco.models.FarmaciaTSD
import com.pharmaco.viewModels.Routes
import io.grpc.Server
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object FarmaciasRepo{

    val TAG = this.javaClass.simpleName

    suspend fun loadFarmacias(farmaciasDisponibles: MutableLiveData<MutableList<FarmaciaTSD>>) =
            withContext(Dispatchers.IO){
                try {
                    loadFarmaciasDB(farmaciasDisponibles, Source.CACHE)
                }
                catch(e: FirebaseFirestoreException){
                    loadFarmaciasDB(farmaciasDisponibles, Source.SERVER)
                }
            }

    private fun loadFarmaciasDB(farmaciasDisponibles: MutableLiveData<MutableList<FarmaciaTSD>>,
                                source: Source){

        val farmaciasList = mutableListOf<FarmaciaTSD>()
        val db = FirebaseFirestore.getInstance()

        db.collection(Routes.FARMACIAS_ROUTE).get(source)
                .addOnSuccessListener { farmacias ->

                    for(document in farmacias){

                        val farmacia = document.toObject(FarmaciaTSD::class.java)


                        farmaciasList.add(farmacia)
                    }

                    farmaciasDisponibles.value = farmaciasList
                    Log.d("FARMLOADED", farmaciasList.toString())

                }
    }

    suspend fun cacheFarmaciasOnInit() = withContext(Dispatchers.IO)
    {
        val db = FirebaseFirestore.getInstance()

        db.collection(Routes.FARMACIAS_ROUTE).get(Source.SERVER)
                .addOnSuccessListener { farmacias ->
                    Log.d(TAG, "FARMACIAS CACHED")

                }
                .addOnFailureListener { exception ->
                    Log.w(TAG, "FARMACIAS ERROR CACHING", exception)
                }

    }

}