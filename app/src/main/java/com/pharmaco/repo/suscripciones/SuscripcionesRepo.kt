package com.pharmaco.repo.suscripciones

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.Timestamp
import com.google.firebase.firestore.*
import com.pharmaco.models.*
import com.pharmaco.viewModels.Routes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

object SuscripcionesRepo {

    val TAG = this.javaClass.toString()

    suspend fun loadSuscripcionesProximas(suscripcionesProximas: MutableLiveData<MutableList<Suscripcion>>) = withContext(Dispatchers.IO){

        try {
            loadSuscripcionesProximasDB(suscripcionesProximas, Source.CACHE)
        }
        catch(e: FirebaseFirestoreException){
            loadSuscripcionesProximasDB(suscripcionesProximas, Source.SERVER)
        }
    }

    suspend fun deleteSuscripcion(posSuscripcion: Int, suscripcionesProximas: MutableLiveData<MutableList<Suscripcion>>) =
        withContext(Dispatchers.IO){
            deleteSuscripcionDB(posSuscripcion, suscripcionesProximas)
        }

    suspend fun createSuscripcion(fechaInicio: Date , frecuencia: Int, fechaFin: Date,
                                    medicamentosSeleccionados: MutableLiveData<MutableList<MedicamentoTSDCreate>>,
                                    selectedFarmacia: MutableLiveData<FarmaciaTSD>) = withContext(Dispatchers.IO){
        createSuscripcionDB(fechaInicio, frecuencia, fechaFin, medicamentosSeleccionados, selectedFarmacia)
    }

    suspend fun editSuscripcion(fechaModificacion: Date, frecuencia: Int, fechaFin: Date,  suscripcion: Suscripcion) =
        withContext(Dispatchers.IO){
            editSuscripcionDB(fechaModificacion, frecuencia, fechaFin, suscripcion)
        }

    private fun editSuscripcionDB(fechaModificacion: Date, frecuencia: Int, fechaFin: Date,  suscripcion: Suscripcion){

        val db = FirebaseFirestore.getInstance()


        val fechasModificacion = mutableListOf<Date>()

        var nuevaFecha = fechaModificacion

        val hora = nuevaFecha.hours
        val minutes = nuevaFecha.minutes

        nuevaFecha.minutes = 0
        nuevaFecha.hours = 0


        while(nuevaFecha.before(fechaFin)){

            nuevaFecha.hours = hora
            nuevaFecha.minutes = minutes
            Log.d("Nueva Fecha En WHiles", nuevaFecha.toString())
            fechasModificacion.add(nuevaFecha)

            nuevaFecha = addDays(nuevaFecha, 7*frecuencia)

            nuevaFecha.minutes = 0
            nuevaFecha.hours = 0
        }

        //Obtener actual
        val fechasAgendadas = suscripcion!!.fechaAgendada


        val nuevasFechas = mutableListOf<Date>()

        var entsSize = 0
        if (suscripcion!!.fechaEntrega != null){
            entsSize = suscripcion!!.fechaEntrega!!.size
        }

        for(i in 0 until entsSize){

            nuevasFechas.add(fechasAgendadas.get(i))

        }

        nuevasFechas.addAll(fechasModificacion)

        //Ya se tiene la nueva lista de fechas, modificarla a Timestramp
        val fechasTimestamp = mutableListOf<Timestamp>()
        for(nF in nuevasFechas){
            fechasTimestamp.add(Timestamp(nF))
        }

        //Actualizar

        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
            .collection(Routes.SUSCRIPCIONES_ROUTE)
            .document(suscripcion!!.id)
            .update(
                "fechaFin", Timestamp(fechaFin),
                "fechaAgendada", fechasTimestamp
            )
            .addOnSuccessListener {
                Log.d(TAG, "Documento actualizado: ${suscripcion!!.id}")
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error creando el documento", e)
            }

    }

    //frecuencia = número de semanas entre cada cita.
    private fun createSuscripcionDB(fechaInicio: Date , frecuencia: Int, fechaFin: Date,
                                  medicamentosSeleccionados: MutableLiveData<MutableList<MedicamentoTSDCreate>>,
                                  selectedFarmacia: MutableLiveData<FarmaciaTSD>){

        val medicamentos = medicamentosSeleccionados.value


        if(medicamentos != null){
            val ordenes = mutableListOf<DocumentReference>()
            val medicamentosTSD = mutableListOf<MedicamentoTSD>()


            for(med in medicamentos) {
                ordenes.add(med.ordenReference)
                medicamentosTSD.add(med.toMedicamentoTSD())
            }

            //PRIMERO, con String de farmacia, obtener Farmacia!
            val db = FirebaseFirestore.getInstance()

            //ordenes entra con entradas repetidas, se deben eliminar
            val ordenesDistinct = ordenes.distinct().toMutableList()

            val fechasAgendada = mutableListOf<Date>()

            var nuevaFecha = fechaInicio

            Log.d("Nueva Fecha", nuevaFecha.toString())

            val hora = nuevaFecha.hours
            val minutes = nuevaFecha.minutes

            nuevaFecha.minutes = 0
            nuevaFecha.hours = 0


            while(nuevaFecha.before(fechaFin)){

                nuevaFecha.hours = hora
                nuevaFecha.minutes = minutes
                Log.d("Nueva Fecha En WHiles", nuevaFecha.toString())
                fechasAgendada.add(nuevaFecha)

                nuevaFecha = addDays(nuevaFecha, 7*frecuencia)

                nuevaFecha.minutes = 0
                nuevaFecha.hours = 0
            }

            Log.d("Fechas Agendadas: ", fechasAgendada.toString())

            val suscripcion = Suscripcion(fechaInicio = fechaInicio, fechaFin = fechasAgendada.last(),
                fechaAgendada = fechasAgendada, farmacia = selectedFarmacia!!.value!!, medicamentos = medicamentosTSD,
                ordenes = ordenesDistinct)


            db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.SUSCRIPCIONES_ROUTE).add(suscripcion.toHashMap())
                .addOnSuccessListener {suscripcionReference ->
                    Log.d(TAG, "Documento creado: $suscripcion")

                    //Ahora añadir referencia en las ordenes.
                    for (orden in ordenesDistinct) {
                        Log.d(TAG, "ORDEN IN ORDENES DISCTINCT")

                        orden.get().addOnSuccessListener {

                            Log.d(TAG, "ORDEN GET")


                            val ordenObj = it.toObject(Orden::class.java)

                            var suscripciones= ordenObj!!.suscripciones

                            if(suscripciones == null){
                                suscripciones = mutableListOf()
                            }
                            if(!suscripciones!!.contains(orden)){
                                suscripciones.add(suscripcionReference)
                            }

                            orden.update("suscripciones", suscripciones)
                                .addOnSuccessListener {
                                    Log.d(TAG, "ACTUALIZADA REFERENCIA EN ORDENES")
                                }
                                .addOnFailureListener { e ->
                                    Log.w(TAG, "Error creando el documento", e)
                                }
                        }
                    }

                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error creando el documento", e)
                }

        }
    }


    //Esta función fue tomada de https://www.journaldev.com/700/java-date-add-days-subtract-days-calendar
    private fun addDays(date: Date, days: Int): Date {
        val cal = GregorianCalendar()
        cal.time = date
        cal.add(Calendar.DATE, days)

        return cal.time
    }

    //posTurno indica la posición del turno en la lista turnosProximos
    private suspend fun deleteSuscripcionDB(posSuscripcion: Int, suscripcionesProximas: MutableLiveData<MutableList<Suscripcion>>){

        val db = FirebaseFirestore.getInstance()

        //Primero, quitar de MutableLiveData
        val suscripciones = suscripcionesProximas.value

        if(suscripciones != null) {
            val suscripcion = suscripciones.get(posSuscripcion)
            suscripciones.removeAt(posSuscripcion)

            withContext(Dispatchers.Main) {
                suscripcionesProximas.value = suscripciones
            }
            db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.SUSCRIPCIONES_ROUTE).document(suscripcion.id)
                .update("cancelado", true)
                .addOnSuccessListener {
                    Log.d(TAG, "Documento marcado como eliminado")
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error eliminando el documento", e)
                }
        }
        else{
            Log.w(TAG, "Error eliminando el documento, turnosProximos inesperadamente nulo")
        }
    }

    private fun loadSuscripcionesProximasDB(suscripcionesProximas: MutableLiveData<MutableList<Suscripcion>>, source: Source) {

        val db = FirebaseFirestore.getInstance()
        val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID).collection(Routes.SUSCRIPCIONES_ROUTE)
        val list = mutableListOf<Suscripcion>()

        docRef.whereEqualTo("cancelado", false).orderBy("fechaInicio", Query.Direction.DESCENDING)
            .get(source)
            .addOnSuccessListener { result ->

                Log.d(TAG, "EN RESULT DE SUSCRIPS")

                for (document in result) {

                    Log.d(TAG, "${document.id} => ${document.data}")

                    val suscripcion = document.toObject(Suscripcion::class.java)
                    suscripcion.id = document.id
                    suscripcion.hora = loadHora(suscripcion)
                    suscripcion.diaSemana = loadDiaSemana(suscripcion)
                    suscripcion.frecuencia = loadFrecuencia(suscripcion)
                    list.add(suscripcion)
                }
                suscripcionesProximas.value = list
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }

    private fun loadDiaSemana(mSuscripcion: Suscripcion): String {

        //Contar cuántas entregas se han realizado.

        var entregas = mSuscripcion.fechaEntrega?.size

        //La fecha correspondiente será última entregas o entregas-1 si entregas == fechaAgendada.size

        //Si ya se hicieron todas las entregas, el día estará dado por el último día de entrega
        //Si no hay entregas, el último día estará dado por la primera fecha programada
        var fecha: Date? = null
        if (entregas == mSuscripcion?.fechaAgendada?.size) {
            if (entregas != null)
                fecha = mSuscripcion?.fechaEntrega?.get(entregas - 1)
        }else if(entregas == null){
            fecha = mSuscripcion.fechaAgendada.get(0)
        }
        else {
            if (entregas != null)
                fecha = mSuscripcion?.fechaAgendada?.get(entregas)
        }

        val diasSemana = arrayOf("Domingos", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábados")

        val mlp: MutableLiveData<String> = MutableLiveData()
        if (fecha != null) {
            return diasSemana[fecha.day]
        }
        //Nunca debería entrar al else!
        else {
            return "Error Fatal!"
        }
    }

    private fun loadFrecuencia(mSuscripcion: Suscripcion): Int {

        //Tomar las dos últimas agendadas y revisar tiempo entre ellas diviviso 7 para obtener semanas.

        val size = mSuscripcion.fechaAgendada.size
        val lastFecha = mSuscripcion.fechaAgendada[size - 1]
        val beforeLastFecha = mSuscripcion.fechaAgendada[size - 2]

        lastFecha.time
        val timeMilis = lastFecha.time - beforeLastFecha.time

        //milias a días y después dividido 7 para semanas
        var weeks = ((timeMilis.toDouble() / 86400000.00) / 7.00)

        val weeksInt = Math.ceil(weeks).toInt()
        return weeksInt

    }

    private fun loadHora(mSuscripcion: Suscripcion): String{

        //Contar cuántas entregas se han realizado.
        var entregas = mSuscripcion?.fechaEntrega?.size

        //Si no hay entregas realizadas
        if(entregas == null){
            entregas = 0
        }

        //La fecha correspondiente será última entregas o entregas-1 si entregas == fechaAgendada.size

        //Si ya se hicieron todas las entregas, el día estará dado por el último día de entrega
        var fecha: Date? = null
        if(entregas == mSuscripcion?.fechaAgendada?.size){
            if(entregas != null)
                fecha = mSuscripcion?.fechaEntrega?.get(entregas-1)
        }
        else{
            if(entregas != null)
                fecha = mSuscripcion?.fechaAgendada?.get(entregas)
        }

        val formatter  = SimpleDateFormat("hh:mm aa")
        val hora = formatter.format(fecha?.time)

        return hora
    }

    suspend fun cacheSuscripciones() = withContext(Dispatchers.IO){
        val db = FirebaseFirestore.getInstance()

        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.SUSCRIPCIONES_ROUTE).whereEqualTo("cancelado", false)
                .get(Source.SERVER).addOnSuccessListener {
                    Log.d(TAG, "SUSCRIPCIONES CACHED")
                }

    }

}