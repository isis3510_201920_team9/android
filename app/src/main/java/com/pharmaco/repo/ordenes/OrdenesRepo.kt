package com.pharmaco.repo.ordenes

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.Source
import com.pharmaco.models.*
import com.pharmaco.viewModels.Routes
import com.pharmaco.viewModels.ordenes.OrdenesViewModel
import com.squareup.okhttp.Route
import kotlinx.coroutines.*
import java.io.*
import java.util.*

object OrdenesRepo{

    val TAG = this.javaClass.toString()

    var threadRunning: Boolean = false

    /*//TODO, recibir un callback de qué hacer cuando se tienen las ordenes.
    fun loadOrdenes(ordenes: MutableLiveData<MutableList<Orden>>, deletesValidos: OrdenesViewModel.MutableListWrapper<Boolean>){


        //TODO, debe determinar si ir a Cache o ir a Firebase
        //retreiveOrdenesLocal(application, ordenes, deletesValidos)

        if(!threadRunning) {
            //TODO, esto debe ser un Singleton, no se debe crear cada vez.
            //TODO, este intento actual de mantener sólo un thread corriendo, no está sirviendo. Parece ser bloqueante.
            GlobalScope.launch {

                val handlerThread = OrdenesHandlerThread()

                handlerThread.start()
                threadRunning = true

                //Se debe esperar, pues de modo contrario el looper no alcanza a inicializarse y la siguiente linea
                //lanza null pointer.
                while (handlerThread.getHandler() == null) {
                    delay(20L)
                }

                //Primero saca del cache
                handlerThread.getHandler()!!.post(OrdenesGetRunnable(ordenes, deletesValidos, this.javaClass.toString(),
                    Source.CACHE)
                )

                //Después saca del servidor
                handlerThread.getHandler()!!.post(OrdenesGetRunnable(ordenes, deletesValidos, this.javaClass.toString(),
                    Source.SERVER)
                )

                handlerThread.quitSafely()
                handlerThread.join()
                threadRunning = false
            }
        }
    }*/

    suspend fun loadOrdenes(ordenes: MutableLiveData<MutableList<Orden>>,
                      deletesValidos: OrdenesViewModel.MutableListWrapper<Boolean>) = withContext(Dispatchers.IO){

        if(!threadRunning) {
            threadRunning = true
            try {
                fetchOrdenes(ordenes, deletesValidos, Source.CACHE)
            }
            catch (e: FirebaseFirestoreException)
            {
                fetchOrdenes(ordenes, deletesValidos, Source.SERVER)
            }
            threadRunning = false
        }

    }

    suspend fun deleteOrden(posOrden: Int, ordenes: MutableLiveData<MutableList<Orden>>,
                            deletesValidos: OrdenesViewModel.MutableListWrapper<Boolean>) =
        withContext(Dispatchers.IO){
            deleteOrdenDB(posOrden, ordenes, deletesValidos)
        }

    suspend fun addOrdenImagen(imagen: File) = withContext(Dispatchers.IO){

    }

    suspend fun addOrdenTexto(nombreDoctor: String, especialidad: String,
                         frecuencias: MutableLiveData<MutableList<Frecuencia>>) = withContext(Dispatchers.IO){
        addOrdenDB(nombreDoctor, especialidad, frecuencias)
    }

    suspend fun updateOrden(nombreDoctor: String, especialidad: String, ordenId: String,
                            frecuencias: MutableLiveData<MutableList<Frecuencia>>) = withContext(Dispatchers.IO){
        updateOrdenDB(nombreDoctor, especialidad, ordenId, frecuencias)
    }

    private fun updateOrdenDB(nombreDoctor: String, especialidad: String, ordenId: String,
                            frecuencias: MutableLiveData<MutableList<Frecuencia>>) {


        val db = FirebaseFirestore.getInstance()

        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
            .collection(Routes.ORDENES_ROUTE).document(ordenId)
            .update("doctor", nombreDoctor, "especialidad", especialidad)
            .addOnSuccessListener {

                Log.d(TAG, "Document modified: ${ordenId}")

                //Eliminar todas las frecuencias
                db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                    .collection(Routes.ORDENES_ROUTE).document(ordenId).collection("frecuencias")
                    .get()
                    .addOnSuccessListener {freqs ->

                        for(freq in freqs ){

                            db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                                .collection(Routes.ORDENES_ROUTE).document(ordenId).collection("frecuencias")
                                .document(freq.id)
                                .delete()
                        }



                        //Volver a crear las frecuencias
                        val frecuenciasList = frecuencias.value

                        if(frecuenciasList != null) {

                            Log.d(TAG, "En ciclo!")
                            for (frecuencia in frecuenciasList) {

                                db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                                    .collection(Routes.ORDENES_ROUTE).document(ordenId).collection("frecuencias")
                                    .add(frecuencia.toHashMap())
                                    .addOnSuccessListener {

                                        Log.d(TAG, "Frecuencia Persistida ${frecuencia}")

                                    }
                            }
                        }

                    }

            }

    }

    private fun addOrdenDB(nombreDoctor: String, especialidad: String,
                           frecuencias: MutableLiveData<MutableList<Frecuencia>>){

        //TODO, por ahora sólo se reciben entradas de texto, audio e imágenes no está implementado aún
        val orden = Orden(doctor = nombreDoctor, fecha = Date(), tipo = "texto", especialidad = especialidad)


        val db = FirebaseFirestore.getInstance()

        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
            .collection(Routes.ORDENES_ROUTE).add(orden.toHashMap())
            .addOnSuccessListener {dr ->
                val newId = dr.id
                Log.d(TAG, "Documento creado: $orden")


                val freqList = frecuencias.value
                if(freqList != null) {
                    for (freq in freqList) {

                        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                            .collection(Routes.ORDENES_ROUTE).document(newId)
                            .collection("frecuencias")
                            .add(freq.toHashMap())
                            .addOnSuccessListener {dr ->
                                Log.d(TAG, "Documento creado: $freq")
                            }
                            .addOnFailureListener { e ->
                                Log.w(TAG, "Error creando el documento", e)
                            }
                    }
                }
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error creando el documento", e)
            }

    }

    //Retorna falso si no se eliminó, verdadero si sí.
    private suspend fun deleteOrdenDB(posOrden: Int, ordenes: MutableLiveData<MutableList<Orden>>, deletesValidos: OrdenesViewModel.MutableListWrapper<Boolean>) {


        val tordenes = ordenes.value

        if(tordenes != null) {

            val orden = tordenes.get(posOrden)

            tordenes.removeAt(posOrden)

            withContext(Dispatchers.Main) {
                ordenes.value = tordenes
            }

            val db = FirebaseFirestore.getInstance()


            //Marcar documento como eliminado
            db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.ORDENES_ROUTE).document(orden.id)
                .update("borrada", true)
                .addOnSuccessListener {
                    Log.d(TAG, "Documento marcado como eliminado")
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error eliminando el documento", e)
                }
        }
        else{
            Log.w(TAG, "Error eliminando el documento, turnosProximos inesperadamente nulo")
        }

    }

    private fun fetchOrdenes(ordenes: MutableLiveData<MutableList<Orden>>,
                              deletesValidos: OrdenesViewModel.MutableListWrapper<Boolean>, source: Source){

        Log.d(TAG, deletesValidos.toString())

        @Synchronized
        fun validListReplace(validList: MutableList<Boolean>, i: Int, valido: Boolean) {
            validList.add(i, valido)
            validList.removeAt(i + 1)
        }

        val db = FirebaseFirestore.getInstance()

        //Recuperar las ordenes de este usuario de la base de datos

        val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID).collection(Routes.ORDENES_ROUTE)

        val list = mutableListOf<Orden>()

        val validList = mutableListOf<Boolean>()

        docRef.whereEqualTo("borrada", false).orderBy("fecha", Query.Direction.DESCENDING).get(source)
            .addOnSuccessListener { result ->

                var counter = 0

                for (document in result) {

                    Log.d(TAG, "${document.id} => ${document.data}")
                    val orden = document.toObject(Orden::class.java)
                    orden.id = document.id
                    orden.reference = document.reference
                    list.add(orden)

                    // por cada orden, se deben traer sus frecuencias
                    val freqRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                        .collection(Routes.ORDENES_ROUTE).document(document.id).collection(Routes.FRECUENCIAS_ROUTE)

                    val freqList = mutableListOf<Frecuencia>()

                    freqRef.get(source).addOnSuccessListener { frecuencias ->
                        for (document in frecuencias) {
                            Log.d(TAG, "${document.id} => ${document.data}")
                            freqList.add(document.toObject(Frecuencia::class.java))
                        }
                        orden.frecuencias = freqList

                        ordenes.value = list

                    }

                    var valido: Boolean = true

                    validList.add(valido)

                    if (orden.turnos != null) {

                        val references = orden.turnos!!

                        val i = counter

                        for (reference in references) {

                            reference.get(source).addOnSuccessListener { pTurno ->

                                val turno = pTurno.toObject(Turno::class.java)

                                if (turno != null) {

                                    valido = valido!! && !(turno?.exitoso == null)
                                }

                                validListReplace(validList, i, valido)
                                //validList.add(i, valido)
                                //validList.removeAt(i+1)

                                /*Log.d(TAG, "VALID LIST: $validList")

                                deletesValidos.value = validList

                                Log.d(TAG, "VALIDOS ${deletesValidos.toString()}")*/

                            }

                        }
                    }

                    if (orden.domicilios != null) {

                        val references = orden.domicilios!!

                        val i = counter

                        for (reference in references) {

                            reference.get(source).addOnSuccessListener { pDomicilio ->

                                val domicilio = pDomicilio.toObject(Domicilio::class.java)

                                if (valido != null) {

                                    valido = valido!! && !(domicilio?.exitoso == null)
                                }

                                validListReplace(validList, i, valido)
                                //validList.add(i, valido)
                                //validList.removeAt(i+1)

                            }

                        }
                    }

                    if (orden.suscripciones != null) {

                        val references = orden.suscripciones!!

                        val i = counter

                        for (reference in references) {

                            reference.get(source).addOnSuccessListener { pSuscripcion ->

                                val suscripcion = pSuscripcion.toObject(Suscripcion::class.java)

                                if (suscripcion != null) {


                                    Log.d(TAG, "${valido}")
                                    Log.d(TAG, "${suscripcion}")
                                    Log.d(TAG, "${suscripcion?.cancelado!!}")
                                    valido = valido!! && (suscripcion?.cancelado!!)


                                    validListReplace(validList, i, valido)
                                    //validList.add(i, valido)
                                    //validList.removeAt(i+1)

                                    /*deletesValidos.value = validList
                                    Log.d(TAG, "VALIDOS ${deletesValidos.toString()}")*/
                                }
                            }

                        }

                    }

                    deletesValidos.value = validList
                    Log.d(TAG, "VALIDOS ${validList}")
                    counter++
                }
                ordenes.value = list
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
        }

    /*private fun retreiveOrdenesLocal(application: Application, ordenes: MutableLiveData<MutableList<Orden>>, deletesValidos: OrdenesViewModel.MutableListWrapper<Boolean>){

        //TODO, calcular deletes válidos

        val file = File(application.cacheDir, Routes.ORDENES_CACHE_ROUTE)
        val fis = FileInputStream(file)
        val ois = ObjectInputStream(fis)

        GlobalScope.launch {

            val ordenesHashMaps = ois.readObject() as MutableList<HashMap<String, Any?>>

            val ordenesList = mutableListOf<Orden>()
            var i = 0
            while (i < ordenesHashMaps.size) {
                Log.d(TAG, "ordenes hashmaps conversion $i")
                ordenesList.add(Orden.ordenFromHashMap(ordenesHashMaps.get(i)))
                i++
            }

            ordenes.value = ordenesList

            Log.d(TAG, "ORDENES LOCAL RETREIVED $ordenes")
        }


    }*/

    //Guarda las órdenes en Local Storage
    /*fun saveOrdenesLocal(application: Application, ordenes: MutableList<Orden>?){


        if(ordenes != null) {

            val ordenesSerizalizableHashMaps = mutableListOf<HashMap<String, Any?>>()
            var i = 0
            while(i<ordenes.size){
                ordenesSerizalizableHashMaps.add(ordenes.get(i).toSerializableHashMap())
                i++
            }

            Log.d(TAG, "ON SAVE ORDENES LOCAL")

            val file = File(application.cacheDir, Routes.ORDENES_CACHE_ROUTE)
            val fos = FileOutputStream(file)
            val oos = ObjectOutputStream(fos)

            val milisBefore = System.currentTimeMillis()
            oos.writeObject(ordenesSerizalizableHashMaps)
            val milisAfter = System.currentTimeMillis()

            Log.d(TAG, "TIME TO WRITE ${milisAfter - milisBefore}")
            fos.close()
            oos.close()
        }

    }*/

    suspend fun cacheOrdenes() = withContext(Dispatchers.IO){

        //Saca ordenes y frecuencias
        val db = FirebaseFirestore.getInstance()

        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.ORDENES_ROUTE).whereEqualTo("borrada", false)
                .get(Source.SERVER)
                .addOnSuccessListener { docs ->
                    for(doc in docs){

                        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                                .collection(Routes.ORDENES_ROUTE).document(doc.id)
                                .collection(Routes.FRECUENCIAS_ROUTE).get(Source.SERVER)

                    }
                }

    }
}