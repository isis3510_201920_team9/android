package com.pharmaco.repo.domicilios

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.*
import com.pharmaco.models.*
import com.pharmaco.viewModels.Routes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

object DomiciliosRepo {

    val TAG = this.javaClass.toString()

    suspend fun deleteDomicilio(posDomicilio: Int, domiciliosProximos: MutableLiveData<MutableList<Domicilio>>) =
        withContext(Dispatchers.IO){
            deleteDomicilioDB(posDomicilio, domiciliosProximos)
        }

    suspend fun loadDomiciliosProximos(domiciliosProximos: MutableLiveData<MutableList<Domicilio>>) =
        withContext(Dispatchers.IO){

            try {
                loadDomiciliosProximosDB(domiciliosProximos, Source.CACHE)
            }
            catch(e: FirebaseFirestoreException){3
                //atrapar FirebaseFirestoreException y volver a pedir pero del servidor
                loadDomiciliosProximosDB(domiciliosProximos, Source.SERVER)
            }
        }

    suspend fun createDomicilio(direccion: String, fechaAgendada: Date,
                                  selectedFarmacia: MutableLiveData<FarmaciaTSD>,
                                  medicamentosSeleccionados: MutableLiveData<MutableList<MedicamentoTSDCreate>>) =
        withContext(Dispatchers.IO){
            createDomicilioDB(direccion, fechaAgendada, selectedFarmacia, medicamentosSeleccionados)
        }

    private fun createDomicilioDB(direccion: String, fechaAgendada: Date,
                                selectedFarmacia: MutableLiveData<FarmaciaTSD>,
                                medicamentosSeleccionados: MutableLiveData<MutableList<MedicamentoTSDCreate>>) {

        val medicamentos = medicamentosSeleccionados.value


        if (medicamentos != null) {
            val ordenes = mutableListOf<DocumentReference>()
            val medicamentosTSD = mutableListOf<MedicamentoTSD>()


            for (med in medicamentos) {
                ordenes.add(med.ordenReference)
                medicamentosTSD.add(med.toMedicamentoTSD())
            }

            //PRIMERO, con String de farmacia, obtener Farmacia!
            val db = FirebaseFirestore.getInstance()


            //ordenes entra con entradas repetidas, se deben eliminar
            val ordenesDistinct = ordenes.distinct().toMutableList()

            val domicilio = Domicilio( direccion = direccion, fechaAgendada = fechaAgendada,
                farmacia = selectedFarmacia!!.value!!, medicamentos = medicamentosTSD, ordenes = ordenesDistinct)

            db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.DOMICILIOS_ROUTE).add(domicilio.toHashMap())
                .addOnSuccessListener {domicilioReference ->
                    Log.d(TAG, "Documento creado: $domicilio")

                    //Ahora añadir referencia en las ordenes.
                    for (orden in ordenesDistinct) {
                        Log.d(TAG, "ORDEN IN ORDENES DISCTINCT")

                        orden.get().addOnSuccessListener {

                            Log.d(TAG, "ORDEN GET")


                            val ordenObj = it.toObject(Orden::class.java)

                            var domicilios= ordenObj!!.domicilios

                            if(domicilios == null){
                                domicilios = mutableListOf()
                            }
                            if(!domicilios!!.contains(orden)){
                                domicilios.add(domicilioReference)
                            }

                            orden.update("domicilios", domicilios)
                                .addOnSuccessListener {
                                    Log.d(TAG, "ACTUALIZADA REFERENCIA EN ORDENES")
                                }
                                .addOnFailureListener { e ->
                                    Log.w(TAG, "Error creando el documento", e)
                                }
                        }
                    }
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error creando el documento", e)
                }
        }
    }

    private fun loadDomiciliosProximosDB(domiciliosProximos: MutableLiveData<MutableList<Domicilio>>, source: Source){

        val db = FirebaseFirestore.getInstance()

        //Recuperar las ordenes de este usuario de la base de datos

        val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID).collection(Routes.DOMICILIOS_ROUTE)

        val list = mutableListOf<Domicilio>()

        docRef.whereEqualTo("exitoso", null).orderBy("fechaAgendada", Query.Direction.DESCENDING).get(source)
            .addOnSuccessListener { result ->

                for (document in result) {

                    Log.d(TAG, "${document.id} => ${document.data}")

                    val domicilio = document.toObject(Domicilio::class.java)
                    domicilio.id = document.id
                    list.add(domicilio)
                }

                domiciliosProximos.value = list
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }

    //posTurno indica la posición del turno en la lista turnosProximos
    private suspend fun deleteDomicilioDB(posDomicilio: Int, domiciliosProximos: MutableLiveData<MutableList<Domicilio>>){

        val db = FirebaseFirestore.getInstance()

        //Primero, quitar de MutableLiveData
        val domicilios = domiciliosProximos.value

        if(domicilios != null) {
            val domicilio = domicilios.get(posDomicilio)
            domicilios.removeAt(posDomicilio)

            withContext(Dispatchers.Main){
                domiciliosProximos.value = domicilios
            }

            val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.DOMICILIOS_ROUTE).document(domicilio.id)

            docRef.update("exitoso", false, "novedades", "Domicilio cancelado por el usuario")
                .addOnSuccessListener {
                    Log.d(TAG, "Documento marcado como eliminado")

                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error eliminando el documento", e)
                }
        }
        else{
            Log.w(TAG, "Error eliminando el documento, turnosProximos inesperadamente nulo")
        }
    }

    suspend fun cacheDomicilios() = withContext(Dispatchers.IO){

        val db = FirebaseFirestore.getInstance()

        //Recuperar las ordenes de este usuario de la base de datos

        val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID).collection(Routes.DOMICILIOS_ROUTE)

        val list = mutableListOf<Domicilio>()

        docRef.whereEqualTo("exitoso", null).orderBy("fechaAgendada", Query.Direction.DESCENDING).get(Source.SERVER)
                .addOnSuccessListener { farmacias ->
                    Log.d(TAG, "DOMICILIOS CACHED")

                }
                .addOnFailureListener { exception ->
                    Log.w(TAG, "DOMICILIOS ERROR CACHING", exception)
                }


    }


}