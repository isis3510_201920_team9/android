package com.pharmaco.repo.notificaciones

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.Source
import com.pharmaco.models.Notificacion
import com.pharmaco.repo.medicamentos.MedicamentosRepo
import com.pharmaco.viewModels.Routes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object NotificacionesRepo {

    val TAG = this.javaClass.simpleName


    suspend fun getNotificaciones(ml: MutableLiveData<MutableList<Notificacion>>) =
            withContext(Dispatchers.IO){
                getNotificacionesFromDB(ml, Source.CACHE)
                getNotificacionesFromDB(ml, Source.SERVER)
            }

    suspend fun addNotificacion(notificacion: Notificacion) = withContext(Dispatchers.IO){
        addNotificacionDB(notificacion)
    }

    private fun addNotificacionDB(notificacion: Notificacion){

        val db = FirebaseFirestore.getInstance()

        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.NOTIFICACIONES_ROUTE).add(notificacion.toHashMap())
                .addOnSuccessListener {
                    Log.d(MedicamentosRepo.TAG, "Documento Persistido $notificacion")

                }

                .addOnFailureListener{exception ->
                    Log.w(MedicamentosRepo.TAG, "Error getting documents.", exception)
                }

    }

    private fun getNotificacionesFromDB(ml: MutableLiveData<MutableList<Notificacion>>, source: Source){

        val db = FirebaseFirestore.getInstance()

        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.NOTIFICACIONES_ROUTE).orderBy("fecha", Query.Direction.DESCENDING).get(source)
                .addOnSuccessListener { documents ->

                    val notificaciones: MutableList<Notificacion> = mutableListOf()
                    var notificacion: Notificacion? = null
                    for(document in documents){

                        notificacion = document.toObject(Notificacion::class.java)
                        notificacion.id = document.id
                        notificaciones.add(notificacion)
                    }

                    ml.value = notificaciones

                }
                .addOnFailureListener{exception ->
                    Log.w(MedicamentosRepo.TAG, "Error getting documents.", exception)
                }
    }

}