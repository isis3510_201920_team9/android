package com.pharmaco.repo.medicamentos

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.Source
import com.pharmaco.models.Frecuencia
import com.pharmaco.models.MedicamentoCreate
import com.pharmaco.models.Orden
import com.pharmaco.rooms.medicamentos.MedicamentosDBSingleton
import com.pharmaco.viewModels.Routes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object MedicamentosRepo {


    val TAG = this.javaClass.toString()

    suspend fun loadMedicamentos(medicamentos: MutableLiveData<MutableList<MedicamentoCreate>>) =
        withContext(Dispatchers.IO){

            try {
                loadMedicamentosDB(medicamentos, Source.CACHE)
            }
            catch(e: FirebaseFirestoreException) {
                loadMedicamentosDB(medicamentos, Source.SERVER)
            }
        }


    suspend fun confirmarMedicamento(nombre: String, mb: MutableLiveData<Boolean>) =
            withContext(Dispatchers.Default){
                val b = confirmarMedicamentoDB(nombre)
                withContext(Dispatchers.Main){
                    mb.value = b
                }
        }

    suspend fun buscarMedicamento(nombre: String, ml: MutableLiveData<List<String>>) =
            withContext(Dispatchers.Default){

                var result = buscarMedicamento(nombre)
                withContext(Dispatchers.Main) {
                    ml.value = null
                }
                if(result != null){
                    withContext(Dispatchers.Main) {
                        ml.value = result
                    }
                }
                //Ahora buscar por principio activo
                result = buscarPrincipioActivo(nombre)

                if(result != null){
                    withContext(Dispatchers.Main) {

                        val list = ml.value

                        if(list != null) {
                            ml.value = mergeLists(list, result)
                        }
                        else{
                            ml.value = result
                        }
                    }
                }

            }

    private fun confirmarMedicamentoDB(nombre: String): Boolean{

        val db = MedicamentosDBSingleton.getInstance()
        return !db.medicamentoDao().searchMedicamento(nombre).isEmpty()

    }

    private fun mergeLists(a: List<String>, b: List<String>): List<String>{

        val c = mutableListOf<String>()

        for(item in a){
            c.add(item)
        }

        for(item in b){
            c.add(item)
        }

        return c

    }

    private fun buscarPrincipioActivo(nombre: String): List<String>?{

        try{
            val db = MedicamentosDBSingleton.getInstance()
            val medicamentos = db.medicamentoDao().searchPrincipio("%$nombre%")

            if(medicamentos.isEmpty()){
                return null
            }


            val list = mutableListOf<String>()
            var i = 0;
            var name: String? = null
            while(i < medicamentos.size){

                name = medicamentos[i].nombre
                if(name != null) {
                    list.add(name)
                }

                i++
            }

            return list
        }
        catch(e: Exception){
            e.printStackTrace()
        }
        return null
    }

    //Recibe un nombre, revisa si algún medicamento en la BD contiene ese nombre
    //Retorna true si existe un medicamento con ese nombre, false de lo contrario.
    private fun buscarMedicamento(nombre: String): List<String>? {

        try {
            val db = MedicamentosDBSingleton.getInstance()
            val medicamentos = db.medicamentoDao().searchMedicamento("%$nombre%")

            if(medicamentos.isEmpty()){
                return null
            }

            val list = mutableListOf<String>()
            var i = 0;
            var name: String? = null
            while(i < medicamentos.size){

                name = medicamentos[i].nombre
                if(name != null) {
                    list.add(name)
                }

                i++
            }

            return list

        }
        catch(e:Exception){
            e.printStackTrace()
        }
        return null
    }


    //Ir a BD, consultar órdenes
    // por cada órden consultar frecuencias,
    // por cada frecuencia, añadir su medicamento a la lista

    private fun loadMedicamentosDB(medicamentos: MutableLiveData<MutableList<MedicamentoCreate>>, source: Source) {

        var  list = mutableListOf<MedicamentoCreate>()

        @Synchronized fun addMedicamento(med: MedicamentoCreate){
            list!!.add(med)
            medicamentos.value = list
        }

        val db = FirebaseFirestore.getInstance()

        //Recuperar las ordenes de este usuario de la base de datos

        val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID).collection(
            Routes.ORDENES_ROUTE
        )

        docRef.whereEqualTo("borrada", false).orderBy("fecha", Query.Direction.DESCENDING)
            .get(source)
            .addOnSuccessListener { result ->

                for (documentOrden in result) {

                    Log.d(TAG, "${documentOrden.id} => ${documentOrden.data}")

                    val orden = documentOrden.toObject(Orden::class.java)
                    orden.id = documentOrden.id

                    // por cada orden, se deben traer sus frecuencias
                    val freqRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                        .collection(Routes.ORDENES_ROUTE).document(documentOrden.id).collection(
                            Routes.FRECUENCIAS_ROUTE
                        )


                    freqRef.get(source).addOnSuccessListener { frecuencias ->
                        for(document in frecuencias){

                            Log.d(TAG, "${document.id} => ${document.data}")

                            val freq = document.toObject(Frecuencia::class.java)
                            val med = MedicamentoCreate(freq.medicamento, documentOrden.reference)
                            addMedicamento(med)
                        }

                    }

                }
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }


}