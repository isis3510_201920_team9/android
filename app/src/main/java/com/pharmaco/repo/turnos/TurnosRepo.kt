package com.pharmaco.repo.turnos

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.*
import com.pharmaco.models.*
import com.pharmaco.viewModels.Routes
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

object TurnosRepo {


    val TAG = this.javaClass.toString()


    suspend fun loadTurnosProximos(turnosProximos: MutableLiveData<MutableList<Turno>>) = withContext(Dispatchers.IO){

        try {
            fetchTurnosProximosDB(turnosProximos, Source.CACHE)
        }
        catch(e: FirebaseFirestoreException) {
            fetchTurnosProximosDB(turnosProximos, Source.SERVER)
        }

    }

    suspend fun deleteTurno(posTurno: Int, turnosProximos: MutableLiveData<MutableList<Turno>>) = withContext(Dispatchers.IO) {
            deleteTurnoDB(posTurno, turnosProximos)
    }

    suspend fun createTurno(fechaAgendada: Date,
                            medicamentosSeleccionados: MutableLiveData<MutableList<MedicamentoTSDCreate>>,
                            selectedFarmacia: MutableLiveData<FarmaciaTSD>) =
        withContext(Dispatchers.IO){
            createTurnoDB(fechaAgendada, medicamentosSeleccionados, selectedFarmacia)
        }


    private fun createTurnoDB(fechaAgendada: Date,
                              medicamentosSeleccionados: MutableLiveData<MutableList<MedicamentoTSDCreate>>,
                              selectedFarmacia: MutableLiveData<FarmaciaTSD>){

        val medicamentos = medicamentosSeleccionados.value


        if(medicamentos != null){
            val ordenes = mutableListOf<DocumentReference>()
            val medicamentosTSD = mutableListOf<MedicamentoTSD>()


            for(med in medicamentos) {
                ordenes.add(med.ordenReference)
                medicamentosTSD.add(med.toMedicamentoTSD())
            }


            //ordenes entra con entradas repetidas, se deben eliminar
            val ordenesDistinct = ordenes.distinct().toMutableList()

            Log.d(TAG, "ORDENES DISTINCT ${ordenesDistinct}")

            val turno = Turno(fechaAgendada = fechaAgendada, farmacia =  selectedFarmacia!!.value!!, medicamentos = medicamentosTSD ,
                ordenes = ordenesDistinct
            )

            val db = FirebaseFirestore.getInstance()

            db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.TURNOS_ROUTE).add(turno.toHashMap())
                .addOnSuccessListener {turnoReference ->
                    Log.d(TAG, "Documento creado: $turno")

                    //Ahora añadir referencia en las ordenes.
                    for (orden in ordenesDistinct) {
                        Log.d(TAG, "ORDEN IN ORDENES DISCTINCT")

                        orden.get().addOnSuccessListener {

                            Log.d(TAG, "ORDEN GET")


                            val ordenObj = it.toObject(Orden::class.java)

                            var turnos= ordenObj!!.turnos

                            if(turnos == null){
                                turnos = mutableListOf()
                            }
                            if(!turnos!!.contains(orden)){
                                turnos.add(turnoReference)
                            }

                            orden.update("turnos", turnos)
                                .addOnSuccessListener {
                                    Log.d(TAG, "ACTUALIZADA REFERENCIA EN ORDENES")
                                }
                                .addOnFailureListener { e ->
                                    Log.w(TAG, "Error creando el documento", e)
                                }
                        }
                    }
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error creando el documento", e)
                }

        }
    }

    //posTurno indica la posición del turno en la lista turnosProximos
    private suspend fun deleteTurnoDB(posTurno: Int, turnosProximos: MutableLiveData<MutableList<Turno>>){

        val db = FirebaseFirestore.getInstance()

        //Primero, quitar de MutableLiveData
        val turnos = turnosProximos.value

        if(turnos != null) {
            val turno = turnos.get(posTurno)
            turnos.removeAt(posTurno)

            withContext(Dispatchers.Main) {
                turnosProximos.value = turnos
            }

            db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID)
                .collection(Routes.TURNOS_ROUTE).document(turno.id)
                .update("exitoso", false, "novedades", "Turno cancelado por el usuario")
                .addOnSuccessListener {
                    Log.d(TAG, "Documento marcado como eliminado!")
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error eliminando el documento", e)
                }
        }
        else{
            Log.w(TAG, "Error eliminando el documento, turnosProximos inesperadamente nulo")
        }
    }

    private fun fetchTurnosProximosDB(turnosProximos: MutableLiveData<MutableList<Turno>>, source: Source) {

        val db = FirebaseFirestore.getInstance()

        //Recuperar las ordenes de este usuario de la base de datos

        val docRef = db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID).collection(Routes.TURNOS_ROUTE)

        val list = mutableListOf<Turno>()

        docRef.whereEqualTo("exitoso", null).orderBy("fechaAgendada", Query.Direction.DESCENDING)
            .get(source)
            .addOnSuccessListener { result ->

                for (document in result) {

                    Log.d(TAG, "${document.id} => ${document.data}")

                    val turno = document.toObject(Turno::class.java)
                    turno.id = document.id

                    list.add(turno)
                }
                turnosProximos.value = list
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }

    suspend fun cacheTurnos() = withContext(Dispatchers.IO){

        val db = FirebaseFirestore.getInstance()

        db.collection(Routes.CLIENTES_ROUTE).document(Routes.USER_ID).collection(Routes.TURNOS_ROUTE)
                .whereEqualTo("exitoso", null).get(Source.SERVER)
                .addOnSuccessListener {
                    Log.d(TAG, "Turnos Cached")
                }

    }


}